Če bomo kdaj želeli uporabljati še sql2o ter jooq, pri čemer si ta delita isti datasource

```
	String dbURL = "jdbc:postgresql://"+konfiguracija.databaseHost+":"+konfiguracija.databasePort+"/"+konfiguracija.database;
	// pridobimo DataSource, ki lahko tvori nove povezave
	Properties dbProps = new Properties();
	dbProps.setProperty("user", konfiguracija.databaseUser);
	dbProps.setProperty("password", konfiguracija.databasePassword);
	DataSource dbDataSource = (new PGDataSourceFactory()).createDataSource(dbProps);
	
	sql = new Sql2o(dbDataSource);
	createJooq = DSL.using(dbDataSource, SQLDialect.POSTGRES);
```

zaenkrat (?) bomo ostali na dependency injectionu za JDBC driver, ki je specificiran v `settings.xml`.
Potrebno je preveriti, če dela pravilno z AWS strežnikom.
