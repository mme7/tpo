# Amazon ECS docker
_Vse kar bi si lahko sploh želel vedeti o vzpostavitvi, konfiguraciji in uporabi Amazon EC2 Container Service - in še malo več._

---
Še opozorilo za vse, ki to berejo: če še niste nikoli imeli stika z Amazon Web Services, je to *zelo* glomazna stvar. Jaz bom resnično predstavil le recept za uporabo docker-ja, bolj nadobudne duše pa si lahko ogledate še linke, na podlagi katerih sem ta navodila naredil:

1. [Kaj ECS je](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
2. [Kako priti do AWS, vzpostavitev sekundarnih uporabnikov](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/get-set-up-for-amazon-ecs.html)
3. [Kaj je AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-set-up.html)
4. [Kako skonfigurirati AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
5. [Uporaba AWS CLI za namen Dockerja](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_AWSCLI.html)
6. [Kako uporabljati Docker v okviru AWS - brez ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html)
7. [Kako uporabljati Docker v okviru AWS - z ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CLI_tutorial.html)
    * [Sintaksa za docker-compose](https://docs.docker.com/compose/compose-file/compose-file-v2)

Ter če boste šli brati zgornje, bo morda prišel prav [ta slovarček](https://www.expeditedssl.com/aws-in-plain-english).
Če boste pa želeli res iti v detajle, pa priporočam [tale tutorail](http://www.ybrikman.com/writing/2015/11/11/running-docker-aws-ground-up/). Jaz nisem šel tako globoko. Sicer je že iz leta 2015, a vmesniki so enaki.
Ter še en [tutorial za docker](https://prakhar.me/docker-curriculum/), ki na koncu obravnava Amazon ECS.

---
> Člani skupine pri TPO lahko zanemarite vse tako oblikovane odseke, za dane korake sem namreč poskrbel že jaz.
> Je pa zdravo, da jih vseeno preberete, ker večinoma vsebujejo dodatne napotke.

---
## Začetki
> Če imate namen izvajati le eno instanco na mesec, je dovolj že [AWS free tier](https://aws.amazon.com/free/), ki nudi 750 ur delovanja za ECS2 storitev (to so Amazonovi clusterji), kar je dovolj za non-stop delovanje ene t2.micro (1 GB RAM, 8GB SSD, precejšen nabor Linux ter Windows distribucij) instance.

>Pri prijavi morate sicer podati še veljaven e-mail, telefonsko številko ter kreditno/debitno kartico. Če ste paranoični glede podatkov, se lahko dobi Izimobil sim za okoli 5€ ter prazna Smartpaysafe kartica za okoli 3 €, pa si s tem pokriješ 12 mesecev delovanja.
Jaz bi sicer še vedno priporočal, da delate znotraj omejitev free tier računa, ter da si nastavite [billing alarm](https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/free-tier-alarms.html) - tu se je vmesnik nekoliko že spremenil, ekvivalenten pogoj je zdaj:
```
EstimatedCharges > 0 for 6 hours
```
### Vzpostavitev repozitorija
> Da boste lažje upravljali z docker slikami, si lahko na Amazonu tvorite še repozitorij zanje - greste pod _AWS Services_ -> _Compute_ razdelek -> _EC2 Container Service_ -> _Get started_

> V tej točki se vam odpre wizard za deployment repozitorija, ki dobro deluje, če želiš deployati le občasno.
Če želiš priti do dejanskih nastavitev, pa kliknite _Cancel_, nakar pojdite v _Repositories_, tam izberete _Get started_ (če prvič) oz. _Create repository_.
Navodila za pushanje, ki jih vam zgenerira, lahko obiščete še kasneje, sicer pa sledijo standardnemu formatu (glej "2. Pushaj sliko na repozitorij").

> Nekoliko bolj direkten način vzpostavitve preko AWS CLI najdete [tule](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_AWSCLI.html)

### Vzpostavitev uporabnikov
> V primeru, da sodeluje več ljudi in nočeš vsem ravno podati vse pravice (navsezadnje lahko tebe počaka račun), lahko to rešiš tako, da v [AWS IAM](https://console.aws.amazon.com/iam/), greš pod Policies -> Create Policy -> Create Your Own Policy ter prilepiš spodnjo definicijo notri

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetRepositoryPolicy",
                "ecr:DescribeRepositories",
                "ecr:ListImages",
                "ecr:DescribeImages",
                "ecr:BatchGetImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:PutImage",
                "ecs:ListClusters",
                "ecs:ListContainerInstances",
                "ecs:ListTaskDefinitions",
                "ecs:DeregisterTaskDefinition",
                "ecs:ListTasks",
                "ecs:RunTask",
                "ecs:StartTask",
                "ecs:StopTask",
                "ecs:DescribeTask"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```
> (pri tem vedi, da je to zelo grobo opredeljen seznam privilegijev, če želiš še bolj komplicirati, si oglej uradne primere za [ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/IAMPolicyExamples.html#first-run-permissions) ter [AWS repozitorij](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecr_managed_policies.html))

> Nato pa greš pod Groups -> Create New Group, kjer pod "Attach Policy" izbereš kreiran policy.

> Uporabnike kreiraš tako, da greš pod Users -> Add User, pod "Select AWS access type" daj vsaj "Programmatic access", da se lahko uporablja AWS CLI/ECS CLI. V "Set permissions" razdelku izbereš "Add user to group" (mora biti že privzeto izbrano), ter ga dodaš v kreiran group.

> Podatke, ki se zgenerirajo, si shrani (izberi "Download .csv"), ker to je **edina** priložnost, da vidiš "Secret access key" ter "Password" uporabnika.

---
# Priprava orodij
Poleg amazonovih orodij potrebujete še `docker` ter `docker-compose`. V primeru linux okolja se dodajte v _docker_ skupino (za več info [glej to](https://wiki.archlinux.org/index.php/Docker#Installation), zavedajte pa se [še tega](https://docs.docker.com/engine/security/security/#linux-kernel-capabilities)).
#### AWS CLI
Tega je mogoče dobiti v Debianu pod `awscli`, v Archu pod `aws-cli`, instalerje za Windows je mogoče dobiti [tule](https://aws.amazon.com/cli/). Za natančnejša navodila [glej tole](https://docs.aws.amazon.com/cli/latest/userguide/installing.html).
**Vzpostavitev dostopa**
TPO skupini sem ključe že zgeneriral. Glejte slack za _credentials.csv_
>Če tole delate prvič, glejte [tole](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-set-up.html#cli-signup), razdelek "_To get your access key ID and secret access key_", dobite zgoraj omenjeni csv. To si nekam shranite, ker do tega je mogoče dostopati le enkrat. Če ste izgubili ključe, ali bi jih radi spremenili, glejte [tole](https://aws.amazon.com/blogs/security/wheres-my-secret-access-key/).

Ko imate ključ za dostop, ga lahko registrirate v terminalu preko
```
aws configure
```
Za "_ID_" ter "_secret access key_" vnesite dobljene ključe, za "_Default region name_" pa nastavite `eu-central-1` ([tabele regij](https://docs.aws.amazon.com/general/latest/gr/rande.html), če potrebujete kakšen drug datacenter).
#### Docker
Zato, da boste lahko pošiljali slike na Amazonov repozitorij, v terminalu izvedite
```
$(aws ecr get-login --registry-ids AWS_REPO_ID)
```
Kjer je `AWS_REPO_ID` ID uporabnika, ki je tvoril repozitorij.
> V primeru, da si repozitorij lasti uporabnik, ki si ga podal pri aws configure (admin), potem lahko to izpustiš. 
#### ECS CLI
Tega orodja trenutno ni v repozitorijih, tako da ga pridobite na [tem](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CLI_installation.html) linku.

---
##### Za zagrizene uporabnike Windows-a (linuxaši preskočite ta odsek)
Tega orodja sicer še ni za Windows, a se ga da (najbrž) izvajati znotraj docker instance. Shranite _ecs-cli_ [iz tu](https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-linux-amd64-latest), pridobite _ca-certificates.crt_ [iz tu](https://github.com/CenturyLinkLabs/ca-certs-base-image) nato pa znotraj istega direktorija tvorite `Dockerfile`
```
FROM busybox
COPY ./ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY ./ecs-cli /ecs-cli
RUN chmod +x /ecs-cli
RUN /ecs-cli configure --region eu-central-1 --access-key AWS_ID --secret-key AWS_SECRET_KEY --cluster CLUSTER_NAME --compose-project-name-prefix "" --compose-service-name-prefix "" --cfn-stack-name-prefix ""
ENTRYPOINT ["/ecs-cli"]
CMD []
```
Za pomen AWS_ID, AWS_SECRET_KEY, CLUSTER_NAME glej linux del ter ustrezno prilagodi.

Nakar lahko tvorimo docker sliko tako, da v istem direktoriju izvedemo
```
docker build -t ecs-cli .
```
Ukaz lahko s tem uporabljamo preko ukazne vrstice kot
```
docker run ecs-cli ...
```

---
##### Za linux
Znotraj linux okolij, potem ko ecs-cli namestimo, ga še skonfiguriramo z
```
ecs-cli configure --region eu-central-1 --access-key AWS_ID --secret-key AWS_SECRET_KEY --cluster CLUSTER_NAME --compose-project-name-prefix "" --compose-service-name-prefix "" --cfn-stack-name-prefix ""
```
Kjer je `AWS_ID` ID AWS tvojega uporabnika, `AWS_SECRET_KEY` je secret key (oboje znotraj zgoraj omenjenega .csv) `CLUSTER_NAME` je ime gruče instanc, ki jo bodisi tvorimo ( pri `ecs-cli ... up`) ali na katero se nanašamo pri ostalih ukazih.
> Pri tem **pazi**, da ime gruče **ne** vsebuje '-' ali '_', ker čeprav ti AWS to dovoljuje, ti ecs-cli v primeru njune uporabe v določenih točkah vrne napako.

Omenjeno nadomesti s pravimi podatki.

---
> V primeru da boš preko ecs-cli tudi tvoril nove gruče (cluster-je), moraš pridobiti še SSH ključe (ecs-cli ga namreč potrebuje pri `up` ukazu). Podrobnejša navodila najdeš na [tem linku](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/get-set-up-for-amazon-ecs.html#create-a-key-pair). Dobljeno .pem datoteko shrani v `~/.ssh/` ter ji nastavi ustrezne pravice.
> S tem ti ob uporabi `ecs-cli ... up` pri obveznem `--keypair` argumentu ni treba navesti poti, temveč le ime .pem datoteke, brez same končnice.

# Uporaba
Tu bom predstavil le del funkcionalnosti, ki so relavantne za TPO. Za širšo predstavitev glej [uradni tutorial](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CLI_tutorial.html), vsekakor pa tudi [reference manual](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CLI_reference.html).
### Ustvarjanje gruče
> Sprva vedno preglej, da nimaš že kakšne gruče ustvarjene, drugače te lahko na koncu meseca pričaka račun.
> Znotraj zmožnosti free teir-a je mogoče ustvariti gručo na sledeč način:
```
ecs-cli up --capability-iam --instance-type t2.micro --size 1 --keypair KEYPAIR_NAME
```
> Kjer je `KEYPAIR_NAME` ime .pem datoteke.
> Če moraš odpreti še kakšen drugi port poleg 80, moraš to eksplicitno določiti z argumentom `--port`.
> Pri tem **pazi**, da v primeru docker image-ov, ki morajo komunicirati med sabo, **ne** potrebuješ odpirati port-ov. Namreč če vse izvajaš na isti instanci (cluster-ju), ter jih med sabo povežeš z `link` znotraj docker-compose.yml, potem docker poskrbi za varno vzpostavitev omrežja med njimi. Glej prej omenjeni uradni tutorial za boljšo predstavo.
### Lokalna uporaba dockerja
Predpostavimo, da imaš za vse strežnike že ustvarjen delujoč `Dockerfile`, ki shrani celotno svojo (začetno) konfiguracijo, (torej baze ipd.) znotraj image-a, torej že ob `docker build ...`
V tem primeru lahko v imeniku `Dockerfile` datotek tvoriš `docker-compose.yml`
z vsebino, ki je podobna sledeči
```
# docker-compose datoteka za pripravo docker slike za lokalno izvajanje ter push
#
# za vec informacij glej
# https://docs.docker.com/compose/compose-file/compose-file-v2
#
# pri tem se zavedaj, da je docker-compose datoteka za ECS CLI drugacna!
# ta ima namrec podprtih le nekaj (deloma svojih) kljucnih besed, glej:
#   https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CLI_tutorial.html
#   http://blog.valiantys.com/en/expert-tips/deploying-docker-containers-aws
#
version: '2'
services:
    streznik:
        # - parametri izgradnje ------------------------------------------------
        build:
            context: ./
            dockerfile: Dockerfile
        # doloci ime:tag slike
        #   ce se tag ne omeni, se ta privzame, da je "latest"
        image: AWS_REPO_NAME
        # - parametri (lokalnega) izvajanja ------------------------------------
        ports:
            # host port : container port
            - "80:80"
        #volumes:
            # host location (relative to the compose file) : container mountpoint
            #   sledeca konfiguracija je smiselna, ko zelis povoziti vsebino
            #   mountpoint-a, ki jo sicer 'docker-compose create/up' zapece v image
            #
            #   - to ti omogoca, da lahko potem, ko lokalno zazenes image,
            #     spreminjas njegovo vsebino med delovanjem
            #   - za deploy na strezniku pa moras obvezno se enkrat pognati
            #     compose, da se spremembe uveljavijo znotraj same slike!
            #
            #./src/:/var/www/

```
Pri tem je `AWS_REPO_NAME` ime slike (image) in s tem **mora** biti enak imenu repozitorija na AWS (lahko se sicer razlikuje, če to upoštevaš pri `docker tag` a si s tem otežuješ delo) - pride prav v naslednjem koraku.
> Toliko da razlikujemo: v dockerju _image/repository_ definira strežnik v njegovem začetnem stanju, _container_ pa instanco tega strežnika, ki se je lahko že zaključila z izvajanjem. Lokalno lahko pridobimo vse container-je z `docker ps -a`, pri čemer jih lahko posamično brišemo z `docker rm CONTAINER_ID`, vse hkrati pa lahko izbrišemo z `docker container prune`.


Slike strežnikov lahko (znotraj istega direktorija kot docker-compose.yml) s tem ustvariš/posodobiš z
```
docker-compose create
```
ustvariš/posodobiš in poženeš pa z
```
docker-compose up
```
### Uporaba dockerja v sklopu ECS
Na podoben način se da olajšati deployment na ECS, pri čemer uporabljamo prilagojen docker-compose.yml - predpostavimo, da ga shranimo kot `docker-compose-ecs.yml`
```
version: '2'
services:
    streznik:
        # doloci ime:tag slike, ki smo jo tvorili z obicajnim docker-compose
        # ta MORA ze biti pushana na amazonov ECS repozitorij!
        image: AWS_REPO_ID.dkr.ecr.eu-central-1.amazonaws.com/AWS_REPO_NAME:latest
        mem_limit: 805306368
        # - parametri (ECS) izvajanja ------------------------------------
        ports:
            # host port : container port
            - "80:80"
```
Pri tem `AWS_REPO_ID` ter `AWS_REPO_NAME` ustrezata prej omenjenim definicijam.

Postopek za postavitev slike na ECS je s tem:

#### 1. (Za vsak slučaj) ponovno prevedi lokalni image
```
docker-compose create
```
#### 2. Pushaj sliko na repozitorij
Bodisi lahko to narediš preko dockerja
```
docker tag AWS_REPO_NAME:latest AWS_REPO_ID.dkr.ecr.eu-central-1.amazonaws.com/AWS_REPO_NAME:latest
docker push AWS_REPO_ID.dkr.ecr.eu-central-1.amazonaws.com/AWS_REPO_NAME:latest
```
ali pa uporabiš ecs-cli
```
ecs-cli push --registry-id AWS_REPO_ID AWS_REPO_NAME:latest
```
#### 3. Preveri če se že izvajajo prejšnje različice ter jih ustavi
```
ecs-cli compose --file docker-compose-ecs.yml ps
ecs-cli compose --file docker-compose-ecs.yml down
```
> Če bi datoteka bila poimenovana kot `docker-compose.yml`, potem se lahko `--file` izpusti. Glede na to, da se compose datoteka za ecs-cli ne nanaša direktno na datoteke projekta, je lahko tudi v drugem direktoriju.
#### 4. Zahtevaj izvajanje slike
```
ecs-cli compose --file docker-compose-ecs.yml up
```
> ecs-cli preoblikuje docker-compose definicijo v "task definition", ki določa delovanje container-jev v gruči. Več [tule](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definitions.html).

### Pospravljanje za sabo
Če ste uporabnik, ki je bil kreiran s zgoraj omenjenimi privilegiji (glej "Vzpostavitev uporabnikov"), potem sledečega niste zmožni narediti, tako da predlagam, da pocukate svojega admina za rokav, da vam to izvede. Naj poudarim, da sledeče je potrebno narediti le v primeru ko popolnoma prenehate z uporabo Amazonovega dockerja.
> Da ustaviš ter odstraniš task definition-e, ki so bili ustvarjeni tekom docker-compose, izvedeš sledeče:
```
ecs-cli compose --file docker-compose-ecs.yml stop
```
> Zdaj Amazon zna včasih biti pameten, ter ti v primeru več task defintionov te poimenuje kot `ime:[zaporedna st. task definition-a]`  - v tem primeru jih ni mogoče brisati direktno preko ecs-cli. Bodisi greš to početi [preko AWS CLI](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CleaningUp.html) ali pa kar ročno preko spletnega vmesnika.

> Da izbrišeš še samo gručo (ki si jo uporabljal za container-je in je definirana kot privzeta preko ecs-cli configure), izvedeš sledeče:
```
ecs-cli down --force
```
Sedaj lahko mirne volje uporabljaš svoje zastonjske resurse za kakšno drugo virtualko - vsaj dokler ne poteče preostanek free-tier obdobja.

