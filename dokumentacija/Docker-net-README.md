# Razhroščevanje omrežij v docker containerjih
Da se olajša debug zaradi zaprtih portov ipd.

## Odisejada po zgodovini mreženja v Docker-ju
V primeru, ko delaš lokalno na računalniku (brez dockerja), lahko predpostaviš
da je vse dostopno iz `localhost` - ko to preseliš v docker, _neha biti tako_.
Vsak container je namreč znotraj svojega omrežja, komunicirajo preko bridge-a,
na katerem je postavljen firewall (iptables pravila določi Docker ob postavitvi
containerja).

Za več info o tem [glej to](https://docs.docker.com/engine/userguide/networking/default_network/container-communication/#communicating-to-the-outside-world).

Če želiš le ročno postavljati omrežja, potem je dovolj, da se uporabljajo
imenovana omrežja [več tu](https://blog.csainty.com/2016/07/connecting-docker-containers.html).
Uradni reference za zgornje je na [tej strani](https://docs.docker.com/engine/reference/commandline/network_connect/).

## Za AWS (predvidoma) neuporaben način

**Primer za network arhitekturo**
Prijavimo se v containerje v interaktivnem načinu, preko `-it` ter ob tem
povozimo privzeti entrypoint z `--entrypoint sh`.

Ustvari omrežje
```
docker network create --driver=bridge tponet
```
Poženi bazo
```
docker run --rm --net=tponet --name tposqlserv tposql
```

Poženi web strežnik
```
docker run --rm --net=tponet --name=tpojavaserv -P -it --entrypoint sh tpojava
```

V web strežniku preveri ali je port odprt
```
 nc 172.22.0.2 5432
```
Če `nc` ne vrne takoj, to pomeni, da je vzpostavil povezavo. Če vrne takoj in je
`$?` po vrednosti 1, potem je port zaprt.

Težava, ki pa se pojavi, pa je v primeru, ko želiš avtomatizirati postavitev več
containerjev - **kako** narediti service discovery?

Docker je to zgodovinsko reševal preko [link-ov](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks/),
a je to zamenjala docker network arhitektura (zgoraj prikazani ukazi so del tega), pri čemer se naj
bi zdaj service discovery delal preko Docker Swarm - glej [tole](https://docs.docker.com/engine/userguide/networking/#an-overlay-network-with-an-external-key-value-store)
za več info.

Žal pri AWS smo še vedno **omejeni na starejšo links arhitekturo** (glej
[tole](https://aws.amazon.com/about-aws/whats-new/2016/08/amazon-ec2-container-service-now-supports-networking-modes-and-memory-reservation/),
[tole](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#container_definitions) in
[tole](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_CLI_tutorial.html), nikjer ni omembe o novejši arhitekturi).
a tu je sedaj ena **zelo** zabavna regresija - namreč Docker obravnava links arhitekturo za tako zastarelo, da jo je pri docker-compose verzije 2 preprosto
nehal podpirati - omrežje med container-ji se sicer vzpostavi, a environment spremenljivke za service discovery se ne generirajo več.

Če bi želeli ohraniti konfiguracijo približno enako med lokalnim in oblačnim svetom (ter se omejiti na ver 1 linke), potem bi morali
razdeliti docker-compose na posebej datoteki verzije 1, kjer [pa se vzpostavi](https://github.com/docker/compose/issues/3268) environment, ali pa ugotoviti drugačen način povezave.

## Uporaben način
Zgornje težave na prvo pogled naredijo odkrivanje ostalih strežnikov praktično nemogoče - **vendar** so od Docker-ja 1.10 dodali funkcionalnost [embedded DNS strežnika](https://docs.docker.com/engine/userguide/networking/configure-dns/)
za service discovery, ki ne potrebuje exsplicitne navedbe containerjev v links okolju (morda se to razlikuje za AWS, potrebno je preveriti).

S tem se lahko namesto preko IPja sklicuješ kar z imenom servisa, kot je ta omenjen v `services:` bloku v docker-compose.
Pri tem se lahko zanemari ime projekta (privzeto ime direktorija, v katerem je docker-compose) ter id skaliranja (tj. katera instanca je, ce jih je vec istih vzporedno zagnanih - najbrz zato (preveri), ker
se ta hostname preslika v instanco istega skalirnega id-ja).

Ob upostevanju zgornjega je mogoče narediti sledeče:
```
docker attach tpo_streznikjava_1
ping -c 1 strezniksql
ping -c 1 tpo_strezniksql_1
```
In je dostop mogoč.

## Debug omrežja preko docker compose
Vsakemu servisu, ki ga želiš razhroščevati, lahko v njegov odsek znotraj skupnega `docker-compose.yml`
datoteke dodaš sledeče:
```
# za debug
entrypoint: /bin/sh
stdin_open: true
```
nato pa z `docker-compose up` poženeš containerje - **bodi pozoren** da se izvajajo, a je namesto prvotnega programa
zagnan `sh`, ki pa ne izpise svojega prompta.

Nanje se lahko nato povežeš z
```
docker attach tpo_strezniksql_1
```
