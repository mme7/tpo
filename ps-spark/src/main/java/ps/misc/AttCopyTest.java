package ps.misc;

import ps.Util;

/**
 * Da preverimo ce kopiranje atributov med razlicnimi razredi dela.
 */
public class AttCopyTest {
    private static class SourceC1 {
        Integer val1;
        Integer val2;
        String val3;

        @Override public String toString() {
            return String.format("%s: %d %d %s", this.getClass().getSimpleName(),
                    val1, val2, val3);
        }
    }
    // Target1 je izgleda da interni javin razred
    private static class TargetC1 {
        Integer val1;
        Integer val2;
        String val3;

        @Override public String toString() {
            return String.format("%s: %d %d %s", this.getClass().getSimpleName(),
                    val1, val2, val3);
        }
    }
    private static class TargetC2 {
        Integer val1;
        Integer val2;
        String val3;
        String val4;

        @Override public String toString() {
            return String.format("%s: %d %d %s", this.getClass().getSimpleName(),
                    val1, val2, val3);
        }
    }
    
    private static class SourceC3 {
        Integer val1;
        Integer val2;
        Integer val3;
        Integer val4;
    }
    
    private static class TargetC3 {
        Integer aval1;
        Integer aval2;
        Integer val3;
        Integer aval4;
        
        @Override public String toString() {
            return String.format("%s: %d %d %d %d", this.getClass().getSimpleName(),
                    aval1, aval2, val3, aval4);
        }
    }

    public static void main(String[] args) {
        System.out.println("proba1");
        SourceC1 s1 = new SourceC1();
        TargetC1 t1 = new TargetC1();
        
        s1.val1 = 2;
        s1.val2 = 45;
        s1.val3 = "Nekajlepega";
        
        Util.kopirajAtribute(s1, t1, true, true);
        System.out.println(s1.toString());
        System.out.println(t1.toString());
        
        System.out.println("proba2");
        SourceC1 s2 = new SourceC1();
        TargetC2 t2 = new TargetC2();
        
        s2.val1 = 7;
        s2.val2 = 17;
        s2.val3 = "lepo";
        Util.kopirajAtribute(s2, t2, true, true, new String[]{"val4"});
        System.out.println(t2.toString());
        
        System.out.println("proba3");
        SourceC3 s3 = new SourceC3();
        TargetC3 t3 = new TargetC3();
        
        s3.val1 = 100;
        s3.val2 = 101;
        s3.val3 = 102;
        s3.val4 = 103;
        
        // kopiramo kar lahko
        Util.kopirajAtribute(s3, t3, false, false);
        System.out.println(t3.toString());
    }
}