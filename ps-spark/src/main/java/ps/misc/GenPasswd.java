package ps.misc;

import java.util.Scanner;
import org.mindrot.jbcrypt.BCrypt;

/**
 * Generira hash ter sol za poljubno geslo.
 */
public class GenPasswd {
    static Scanner s = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Geslo: ");
        String sol = BCrypt.gensalt();
        String hash = BCrypt.hashpw(s.nextLine(), sol);
        
        System.out.format("sol [%s] hash [%s]%n", sol, hash);
    }
}
