package ps.zgodbe;

import static generated.Tables.*;
import generated.tables.OsebaEntiteta;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import static ps.Server.gson;
import static spark.Spark.post;

import generated.tables.pojos.UporabnikEntiteta;
import org.jooq.Condition;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ps.Avtentikacija;
import ps.JooqContext;
import ps.Server;
import ps.entitete.enumeracije.SifraVloge;


public class Zgodba6IzpisFiltrov {
    private static final Logger log = LoggerFactory.getLogger(Zgodba6IzpisFiltrov.class);

    private class Filtri {
        Timestamp DNObdobjeIzdajeOD;
        Timestamp DNObdobjeIzdajeDO;
    }
    
    private static Condition addCon(Condition wc, Condition c) {
        return wc != null ? wc.and(c) : c;
    }
    
    private static class PodatkiVrstice {
        int TabDelovniNalogId;
        String TabImePacient;
        Timestamp TabCasIzdaje;
        String TabVrstaObiska;
        String TabIzdajatelj;
        String TabZadolzenaPS;
        String TabNadomestnaPS;

        public PodatkiVrstice(int TabDelovniNalogId, String TabImePacient, Timestamp TabCasIzdaje, String TabVrstaObiska, String TabIzdajatelj, String TabZadolzenaPS, String TabNadomestnaPS) {
            this.TabDelovniNalogId = TabDelovniNalogId;
            this.TabImePacient = TabImePacient;
            this.TabCasIzdaje = TabCasIzdaje;
            this.TabVrstaObiska = TabVrstaObiska;
            this.TabIzdajatelj = TabIzdajatelj;
            this.TabZadolzenaPS = TabZadolzenaPS;
            this.TabNadomestnaPS = TabNadomestnaPS;
        }
    }
    
    private static class PodatkiVrsticeBaza {
        int TabDelovniNalogId;
        String TabImePacientIme;
        String TabImePacientPriimek;
        Timestamp TabCasIzdaje;
        String TabVrstaObiska;
        String TabIzdajateljIme;
        String TabIzdajateljPriimek;
        String TabZadolzenaPSIme;
        String TabZadolzenaPSPriimek;
        String TabNadomestnaPSIme;
        String TabNadomestnaPSPriimek;
    }

    public static void rutine() {
        post("/api/filtrirani_delovni_nalog", (req, res) -> {
            // pridobimo sifro prijavljenega
            UporabnikEntiteta uporabnik = Avtentikacija.zahtevaj(req,
                    SifraVloge.Vodja_patronazne_sluzbe.ordinal(),
                    SifraVloge.Medicinska_sestra.ordinal(),
                    SifraVloge.Zdravnik.ordinal()
            );
            
            // pridobimo vrednosti filtrov
            log.info(String.format("Dobil [%s]", req.body()));
            Filtri filtri = Server.gson.fromJson(req.body(), Filtri.class);
            
            OsebaEntiteta pacientOseba = OSEBA.as("pacient_oseba");
            OsebaEntiteta izdajateljOseba = OSEBA.as("izdajatelj_oseba");
            OsebaEntiteta sestraOseba = OSEBA.as("sestra_oseba");
            OsebaEntiteta nadomestnaSestraOseba = OSEBA.as("nadomestnasestra_oseba");

            try (JooqContext jooq = new JooqContext()) {
                // glede na to, kateri query je na voljo, dodamo pogoje za where
                Condition wc = null;
                
                // omejimo glede na izdajatelja, ce gre za zdravnika,
                // medicinsko sestro
                if(uporabnik.getVloga() == SifraVloge.Zdravnik.ordinal())
                    wc = addCon(wc, DELOVNINALOG.IZDAJATELJ.eq(uporabnik.getId()));

                if(uporabnik.getVloga() == SifraVloge.Medicinska_sestra.ordinal())
                    wc = addCon(wc, sestraOseba.ID.eq(uporabnik.getId()).or(nadomestnaSestraOseba.ID.eq(uporabnik.getId())));
                
                // preverjamo le ce je uporabnik podal vsaj kaksnega izmed kriterijev
                if(filtri != null) {
                    
                    if(filtri.DNObdobjeIzdajeOD != null)
                        wc = addCon(wc, DELOVNINALOG.DATUMIZDAJE.greaterOrEqual(filtri.DNObdobjeIzdajeOD));

                    if(filtri.DNObdobjeIzdajeDO != null )
                        wc = addCon(wc, DELOVNINALOG.DATUMIZDAJE.lessOrEqual(filtri.DNObdobjeIzdajeDO));

                }
                // ce ni nobenega filtra, potem nastavimo pogoj tako, da bo
                // vedno veljaven
                if(wc == null)
                    wc = DSL.trueCondition();
                
                // zgradimo query z zgeneriranim objektom pogojev za where
                List<PodatkiVrsticeBaza> rezultatiBaza = jooq.context
                 .selectDistinct(
                         DELOVNINALOG.ID.as("TabDelovniNalogId"),
                         pacientOseba.IME.as("TabImePacientIme"),
                         pacientOseba.PRIIMEK.as("TabImePacientPriimek"),
                         DELOVNINALOG.DATUMIZDAJE.as("TabCasIzdaje"),
                         VRSTAOBISKA.IME.as("TabVrstaObiska"),
                         izdajateljOseba.IME.as("TabIzdajateljIme"),
                         izdajateljOseba.PRIIMEK.as("TabIzdajateljPriimek"),
                         sestraOseba.IME.as("TabZadolzenaPSIme"),
                         sestraOseba.PRIIMEK.as("TabZadolzenaPSPriimek"),
                         nadomestnaSestraOseba.IME.as("TabNadomestnaPSIme"),
                         nadomestnaSestraOseba.PRIIMEK.as("TabNadomestnaPSPriimek")
                 ).from(DELOVNINALOG
                        .join(izdajateljOseba).on(DELOVNINALOG.IZDAJATELJ.eq(izdajateljOseba.ID))
                        .join(PACIENTDN).on(PACIENTDN.VLOGA.eq(1).and(DELOVNINALOG.ID.eq(PACIENTDN.DELOVNINALOG)))
                        .join(PACIENT).on(PACIENTDN.PACIENT.eq(PACIENT.ID))
                        .join(pacientOseba).on(PACIENT.ID.eq(pacientOseba.ID))
                        .join(OBISK).on(DELOVNINALOG.ID.eq(OBISK.DELOVNINALOG))
                        .join(VRSTAOBISKA).on(DELOVNINALOG.VRSTAOBISKA.eq(VRSTAOBISKA.ID))
                        .join(sestraOseba).on(OBISK.SESTRA.eq(sestraOseba.ID))
                        .leftOuterJoin(nadomestnaSestraOseba).on(OBISK.NADOMESTNASESTRA.eq(nadomestnaSestraOseba.ID))
                ).where(wc)
                 .orderBy(DELOVNINALOG.ID)
                 .fetchInto(PodatkiVrsticeBaza.class);
                
                // mapiramo v seznam vrstic podatkov, ki jih frontend pricakuje
                List<PodatkiVrstice> rezultati = rezultatiBaza.stream().map((PodatkiVrsticeBaza pvb) ->
                    new PodatkiVrstice(
                            pvb.TabDelovniNalogId,
                            pvb.TabImePacientIme+" "+pvb.TabImePacientPriimek,
                            pvb.TabCasIzdaje,
                            pvb.TabVrstaObiska,
                            pvb.TabIzdajateljIme+" "+pvb.TabIzdajateljPriimek,
                            pvb.TabZadolzenaPSIme+" "+pvb.TabZadolzenaPSPriimek,
                            (pvb.TabNadomestnaPSIme !=null ? pvb.TabNadomestnaPSIme+" "+pvb.TabNadomestnaPSPriimek : "")
                    )
                ).collect(Collectors.toList()); 
                
                return gson.toJson(rezultati);
            }

        });
        
        // potreben se posebej query za
        // pacientdn
        // obisk (gor se sicer uporabi, a se potem uporabijo le unique vrstice)
        // zdravilodn
        // zdravilo
        // ker teh je lahko vec
        	
        	// pridobi id delovnega naloga in vrni njegove podatke ()
        /*
    IzpisVrstaObiska: string;
    IzpisImeinPriimekPacienta: string;
    IzpisNaslovPacienta: string;
    IzpisDatumPrvegaObiska: Date;
    IzpisDatumZadnjegaObiska: Date;
    IzpisSteviloObiskov: number;
    IzpisPlaniranje: string;
         *  specificniPodatki = [
		        {
		            vrstaPodatka: 'Zdravilo 1',
		            opisPodatka: 'Gravibinom'
		        },
		        {
		            vrstaPodatka: 'Zdravilo 2',
		            opisPodatka: '2 brci v tazadnjo'
		        }
		    ];
         * */
        post("/api/pridobi_delovni_nalog", (req, res) -> {
            return "";
        });
    }

}
