package ps.zgodbe;

import static spark.Spark.post;

import java.util.List;

import static generated.Tables.*;
import static ps.Server.gson;

import generated.tables.pojos.*;
import ps.JooqContext;

public class Zgodba16SeznamObiskovPacient {

	public static void rutine() {
	    post("/api/filtrirani_obiski_pacient", (req, res) -> {
    		
	        try (JooqContext jooq = new JooqContext()) {
	        	List<ObiskEntiteta> obiski = jooq.context.select().from(OBISK).fetchInto(ObiskEntiteta.class);
	        	return gson.toJson(obiski);
	        }
	    });
	}
}
