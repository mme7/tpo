package ps.zgodbe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import java.util.concurrent.TimeUnit;


import generated.tables.pojos.PacientEntiteta;
import generated.tables.pojos.ZaposleniEntiteta;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.util.postgres.PostgresDataType;

import static generated.Tables.*;
import static org.jooq.impl.DSL.*;
import static ps.Server.gson;


import ps.Akter;
import ps.JooqContext;
import static spark.Spark.*;

import generated.tables.records.*;

public class Zgodba5KreiranjeDN {




    public static void rutine() {

        get("/api/uporabnik/:id/sestre", (req,res)-> {


            int id = Integer.parseInt(req.params("id"));


            try (JooqContext jooq = new JooqContext()) {
                PacientEntiteta pacient = jooq.context.select().from(PACIENT).where(PACIENT.ID.eq(id)).fetchOneInto(PacientEntiteta.class);

                Result<Record> se = jooq.context.select().from(ZAPOSLENI)
                        .leftJoin(OSEBA).on(ZAPOSLENI.ID.eq(OSEBA.ID))
                        .leftJoin(UPORABNIK).on(UPORABNIK.ID.eq(ZAPOSLENI.ID))
                        .leftJoin(OKOLIS).on(OKOLIS.ID.eq(ZAPOSLENI.OKOLIS))
                        .where(ZAPOSLENI.OKOLIS.eq(pacient.getOkolis()).and(UPORABNIK.VLOGA.eq(4)))
                        .fetch();

                ArrayList<Sestrazg5> sestre = new ArrayList<Sestrazg5>();
                se.forEach(s -> {
                    Sestrazg5 szg = new Sestrazg5();
                     szg.ime = s.get(OSEBA.IME) + " " + s.get(OSEBA.PRIIMEK);
                     szg.id = s.get(OSEBA.ID);
                     sestre.add(szg);
                });


                return gson.toJson(sestre);
            }

        });

        post("/api/isci_uporabnika", (req,res)->{
            try(JooqContext jooq = new JooqContext()){
                //System.out.println(gson.toJson(jooq.context.select().from(UPORABNIK).join(OSEBA).on(UPORABNIK.ID.eq(OSEBA.ID)).fetchInto(Akter.class)));

                String p = req.body().replaceAll(" ", "");
                String str = "%" + p.toLowerCase() + "%";
                try {
                    int n = Integer.parseUnsignedInt(p.trim());
                    return gson.toJson(jooq.context.select().from(PACIENT).join(OSEBA).on(PACIENT.ID.eq(OSEBA.ID))
                            .where(PACIENT.SIFRAZAVAROVANJA.cast(PostgresDataType.TEXT).like(str))
                            .limit(10).fetchInto(Akter.class));

                } catch (NumberFormatException e){
                    return gson.toJson(jooq.context.select().from(PACIENT).join(OSEBA).on(PACIENT.ID.eq(OSEBA.ID))
                            .where(lower(replace(concat(OSEBA.IME,OSEBA.PRIIMEK), " ", "")).like(str))
                            .limit(10).fetchInto(Akter.class));
                }
            }

        });

        post("/api/kreiraj_dn", (req,res)->{
            try(JooqContext jooq = new JooqContext()){

                Dn dn = gson.fromJson(req.body(), Dn.class);



                //------- Polnjenje delovnega naloga

                DelovninalogRecordEntiteta dnRecord = jooq.context.newRecord(DELOVNINALOG);
                dnRecord.setVrstaobiska(dn.vrstaobiska);
                dnRecord.setIzdajatelj(dn.izdajatelj);
                dnRecord.setDatumizdaje(new Timestamp(System.currentTimeMillis()));
                dnRecord.store();
                int dnID = dnRecord.getId();
                System.out.println("Id delovnega naloge: " + dnID);

                PacientdnRecordEntiteta pdn = jooq.context.newRecord(PACIENTDN);
                pdn.setDelovninalog(dnID);
                pdn.setPacient(dn.pacient);
                pdn.setVloga(1);
                pdn.store();

                 PacientEntiteta pacient = jooq.context.select().from(PACIENT).where(PACIENT.ID.eq(dn.pacient)).fetchOneInto(PacientEntiteta.class);

                switch (dn.vrstaobiska) {
                    case 1: {
                        break;
                    }
                    case 2: {
                        Arrays.stream(dn.otroci).forEach(o-> {
                            PacientdnRecordEntiteta pdno = jooq.context.newRecord(PACIENTDN);
                            pdno.setDelovninalog(dnID);
                            pdno.setPacient(o);
                            pdno.setVloga(2);
                            pdno.store();
                        });
                        break;
                    }
                    case 3: {
                        break;
                    }
                    case 4: {
                        Arrays.stream(dn.zdravila).forEach(z-> {
                            ZdravilodnRecordEntiteta zdn = jooq.context.newRecord(ZDRAVILODN);
                            zdn.setDn(dnID);
                            zdn.setZdravilo(z);
                            zdn.store();
                        });
                        break;
                    }
                    case 5: {
                        Arrays.stream(dn.material).forEach(m-> {
                            MaterialdnRecordEntiteta zdn = jooq.context.newRecord(MATERIALDN);
                            zdn.setDn(dnID);
                            zdn.setKolicina(m.kolicina);
                            zdn.setMaterial(m.material);
                            zdn.store();
                        });
                        break;
                    }
                    case 6: {
                        break;
                    }

                }


                //----------- izracun obiskov
                Calendar c = Calendar.getInstance();
                c.setTime(dn.prviObisk);



                long steviloDelovnihMs = 0;
                long razmik = 0;
                if(dn.nacinPorazdelitve == 1){
                    long diff = -dn.prviObisk.getTime() + dn.zadnjiObisk.getTime();
                    int razlika = (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                    System.out.println("Razlika: " + razlika);
                    Calendar k = Calendar.getInstance();

                    k.setTime(dn.prviObisk);
                    for(;razlika > 0; razlika--){
                        Date kd = k.getTime();
                        steviloDelovnihMs += (long)3600*24*1000;
                       k.add(Calendar.DATE, 1);
                    }
                    razmik = steviloDelovnihMs/(dn.steviloObiskov-1)-1000;
                }



                boolean prvi = true;

                int dan = 0;
                long zadnjiObiskMs = (long)-3600*24*1000*1000;
                int obiski = 0;
                for(;;){
                    boolean dodaj = false;
                    if(obiski == 0){
                        dodaj = true;
                    }

                    Date d = c.getTime();

                    if(dn.nacinPorazdelitve == 0) { // dnevi
                       if(d.getTime() - zadnjiObiskMs >= (long)dn.interval * 3600*24*1000){
                           dodaj = true;
                       }
                    } else { //obdobje
                        if((d.getTime() - dn.prviObisk.getTime() ) / razmik >= obiski){
                            dodaj = true;
                        }
                    }

                    if(dodaj && d.getDay() != 0 && d.getDay() != 6){
                        zadnjiObiskMs = d.getTime();
                        obiski++;
                        ObiskRecordEntiteta or = jooq.context.newRecord(OBISK);
                        or.setPredvideni(new java.sql.Date(d.getTime()));
                        or.setObvezen(obiski==1 && dn.obveznostPrvega);
                        if(obiski==1 && dn.obveznostPrvega){
                            or.setPlanirani(new java.sql.Date(d.getTime()));
                        }
                        or.setSestra(dn.sestra);
                        or.setDelovninalog(dnID);
                        or.setStevilkaobiska(obiski);
                        or.store();
                        if(obiski == dn.steviloObiskov) break;
                        System.out.println(d);
                    }
                    dan++;
                    c.add(Calendar.DATE, 1);

                }
                System.out.println(gson.toJson(dn));


                return "";
            }

        });
    }
}


class Dn {
       int vrstaobiska;
       int pacient;
       int[] otroci;
       int[] zdravila;
        Materialdn[] material;

        Date prviObisk;
        boolean obveznostPrvega;
        int steviloObiskov;
        int nacinPorazdelitve;
        int interval;
        Date zadnjiObisk;
        int izdajatelj;
        int sestra;
};


class Materialdn {
    public int material;
    public int dn;
    public int kolicina;
}


class Sestrazg5 {
    public int id;
    public String  ime;
}
