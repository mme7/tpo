package ps.zgodbe;

import generated.tables.pojos.UporabnikEntiteta;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;

import ps.Avtentikacija;
import ps.JooqContext;
import ps.Server;

import static spark.Spark.post;
import static generated.Tables.*;

/*

#4 Uporabnik lahko spremeni svoje geslo
 */
public class Zgodba4SpremembaGesla {
	private static final Logger log = LoggerFactory.getLogger(Zgodba4SpremembaGesla.class);
    
    private static class SpremeniGeslo {
        String oldPsw;
        String newPsw;
    }

    
    public static void rutine() {
        // nalozi glavno admin stran
        post("/api/configuration", (request,response)->{
        	UporabnikEntiteta u = Avtentikacija.prijavljeniUporabnik(request);
        	
            SpremeniGeslo sg = Server.gson.fromJson(request.body(), SpremeniGeslo.class);
            
            if(BCrypt.checkpw(sg.oldPsw, u.getGeslo())) {
                    u.setSol(BCrypt.gensalt());
                    u.setGeslo(BCrypt.hashpw(sg.newPsw, u.getSol()));
                    try(JooqContext jooq = new JooqContext()){
                        jooq.context.update(UPORABNIK).set(UPORABNIK.GESLO, u.getGeslo()).where(UPORABNIK.ID.eq(u.getId())).execute();
	                }
                    log.info("Spremenil geslo za [%s]",u.getEmail());
                    return true;
            } else {
                log.warn(String.format("Za uporabnika [%s] se staro geslo ne ujema s podanim (staro [%s] != podano [%s])",
                        u.getEmail(),
                        u.getGeslo(),
                        sg.oldPsw));
                response.status(418);
        		return false;
            }
        }); 
    }
}

