package ps.zgodbe;
import generated.tables.OsebaEntiteta;
import org.jooq.Record;
import ps.JooqContext;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;

import static generated.Tables.*;
import static generated.tables.OsebaEntiteta.OSEBA;
import static ps.Server.gson;
import static spark.Spark.*;

public class Zgodba9VnosPopravljanjeObiska {
    public static void rutine() {
        get("/api/obisk/:id", (req,res)-> {
            int oid = Integer.parseInt(req.params("id"));
            DNObiskZg9 DNObisk = new DNObiskZg9();


            try (JooqContext jooq = new JooqContext()) {
                OsebaEntiteta zadolzenaSestraOseba = OSEBA.as("zadolzena_sestra_oseba");
                OsebaEntiteta nadomestnaSestraOseba = OSEBA.as("nadomestna_sestra_oseba");

                Record or = jooq.context.select().from(OBISK)
                        .leftJoin(zadolzenaSestraOseba).on(zadolzenaSestraOseba.ID.eq(OBISK.SESTRA))
                        .leftJoin(nadomestnaSestraOseba).on(nadomestnaSestraOseba.ID.eq(OBISK.NADOMESTNASESTRA))
                        .where(OBISK.ID.eq(oid))
                        .fetchOne();

                int id = or.get(OBISK.DELOVNINALOG);
                DNObisk.dn = Zgodba7IzpisDN.pridoviDN(id);
                DNObisk.podatki = or.get(OBISK.PODATKI);
                DNObisk.sestra = or.get(zadolzenaSestraOseba.IME) + " " +  or.get(zadolzenaSestraOseba.PRIIMEK);

                if(or.get(nadomestnaSestraOseba.IME) != null){
                    DNObisk.nadomestnasestra = or.get(nadomestnaSestraOseba.IME) + " " +  or.get(nadomestnaSestraOseba.PRIIMEK);
                }


                Record orPrvi = jooq.context.select().from(OBISK)
                        .leftJoin(DELOVNINALOG).on(DELOVNINALOG.ID.eq(OBISK.DELOVNINALOG))
                        .where(DELOVNINALOG.ID.eq(id).and(OBISK.STEVILKAOBISKA.eq(1))).fetchOne();
                DNObisk.podatkiPrvi = orPrvi.get(OBISK.PODATKI);
                DNObisk.stevilkaObiska = or.get(OBISK.STEVILKAOBISKA);
                DNObisk.predvideni = or.get(OBISK.PREDVIDENI);
                DNObisk.planirani = or.get(OBISK.PLANIRANI);
                DNObisk.dejanski = or.get(OBISK.DEJANSKI);

            }
            return gson.toJson(DNObisk);
        });

        post("/api/obisk/:id/:datum", (req,res)-> {
            int oid = Integer.parseInt(req.params("id"));
            long datum = Long.parseLong(req.params("datum"));

           Date d = new Date(datum);


            try (JooqContext jooq = new JooqContext()) {
                jooq.context.update(OBISK)
                        .set(OBISK.PODATKI, req.body())
                        .set(OBISK.DEJANSKI, d )
                        .where(OBISK.ID.eq(oid))
                        .execute();
            }
            return gson.toJson("");
        });
    }
}

class DNObiskZg9{
    DnZg7 dn;
    String podatki; //json podatki o obisku
    String podatkiPrvi; //json podatki o obisku
    Date predvideni;
    Date    planirani;
    Date    dejanski;
    int stevilkaObiska;
    String sestra;
    String nadomestnasestra;
}


