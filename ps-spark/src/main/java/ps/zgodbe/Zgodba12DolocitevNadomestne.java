package ps.zgodbe;

import static generated.Tables.OBISK;
import static generated.Tables.ZAPOSLENI;
import generated.tables.OsebaEntiteta;
import static generated.tables.OsebaEntiteta.OSEBA;
import static generated.tables.UporabnikEntiteta.UPORABNIK;
import generated.tables.pojos.UporabnikEntiteta;
import java.sql.Date;
import java.util.List;
import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ps.Avtentikacija;
import ps.JooqContext;
import ps.Server;
import static ps.Server.gson;
import ps.entitete.enumeracije.SifraVloge;
import static spark.Spark.get;
import static spark.Spark.post;

public class Zgodba12DolocitevNadomestne {

    private static final Logger log = LoggerFactory.getLogger(Zgodba12DolocitevNadomestne.class);

    private static class SestraOseba {

        public Integer id;
        public Integer sifraZaposlenega;
        public Integer okolis;
        public String imePriimek;
    }

    private static class NadomestnaSestra {

        Date zacetekCasovnega;
        Date konecCasovnega;
        Integer medicinskaSestra;
        Integer nadomestnaSestra;
    };

    private static class ObiskVrstica {
        Integer id;
        Date predvideni;
        Date planirani;
        Boolean zaseden;
        String nadomestnasestra;
        Boolean obvezen;
    }

    public static void rutine() {
        get("/api/vrni_vse_sestre", (req, res) -> {
            try (JooqContext jooq = new JooqContext()) {
                List<SestraOseba> o
                        = jooq.context.select(
                                OSEBA.ID.as("id"),
                                ZAPOSLENI.SIFRAZAPOSLENEGA.as("sifraZaposlenega"),
                                ZAPOSLENI.OKOLIS.as("okolis"),
                                concat(OSEBA.IME, val(" "), OSEBA.PRIIMEK).as("imePriimek")
                        )
                                .from(OSEBA
                                        .join(UPORABNIK).on(OSEBA.ID.eq(UPORABNIK.ID))
                                        .join(ZAPOSLENI).on(OSEBA.ID.eq(ZAPOSLENI.ID))
                                )
                                .where(UPORABNIK.VLOGA.eq(SifraVloge.Medicinska_sestra.ordinal()))
                                .fetchInto(SestraOseba.class);
                return Server.gson.toJson(o);
            }
        });
        post("/api/vrni_obiske_sestre_obdobja", (req, res) -> {
            NadomestnaSestra ns = gson.fromJson(req.body(), NadomestnaSestra.class);

            OsebaEntiteta zadolzenaSestraOseba = OSEBA.as("zadolzena_sestra_oseba");
            OsebaEntiteta nadomestnaSestraOseba = OSEBA.as("nadomestna_sestra_oseba");

            try (JooqContext jooq = new JooqContext()) {
                List<ObiskVrstica> pv
                        = jooq.context.select(
                                OBISK.ID.as("id"),
                                OBISK.PREDVIDENI.as("predvideni"),
                                OBISK.PLANIRANI.as("planirani"),
                                OBISK.OBVEZEN.as("obvezen")
                        // TabNadomestnaSestra se nastavi posebej, zato ker je potrebna dodatna logika za dolocitev vsebine
                        ).from(
                                OBISK
                                        .join(zadolzenaSestraOseba).on(OBISK.SESTRA.eq(zadolzenaSestraOseba.ID))
                        ).where(
                                OBISK.SESTRA.eq(ns.medicinskaSestra)
                                        .and(OBISK.DEJANSKI.isNull())
                                        .and(OBISK.PREDVIDENI.greaterOrEqual(ns.zacetekCasovnega))
                                        .and(OBISK.PREDVIDENI.lessOrEqual(ns.konecCasovnega))
                        ).orderBy(
                                OBISK.STEVILKAOBISKA.asc()
                        ).fetchInto(ObiskVrstica.class);
                
	                pv.stream().forEach(tPv -> {
	                    generated.tables.pojos.OsebaEntiteta nS = jooq.context.select()
	                                .from(OBISK.join(OSEBA).on(OBISK.NADOMESTNASESTRA.eq(OSEBA.ID)))
	                                .where(OBISK.ID.eq(tPv.id))
	                                .fetchOneInto(generated.tables.pojos.OsebaEntiteta.class);
                        tPv.zaseden = nS!=null;
	                    tPv.nadomestnasestra = (nS!=null) ? nS.getIme()+" "+nS.getPriimek() : "/";
	                });
                
                return gson.toJson(pv);
            }
        });

        post("/api/doloci_nadomescanje", (req, res) -> {
            UporabnikEntiteta uporabnik = Avtentikacija.zahtevaj(req,
                    SifraVloge.Vodja_patronazne_sluzbe.ordinal()
            );

            log.info("Dobil " + req.body());
            NadomestnaSestra ns = Server.gson.fromJson(req.body(), NadomestnaSestra.class);

            try (JooqContext jooq = new JooqContext()) {
                // ne dovolimo, da kaksna sestra ze nadomesca drugo sestro,
                // predno poskusamo tretji prirediti te obiske
                if (jooq.context.selectCount()
                        .from(OBISK)
                        .where(
                                OBISK.SESTRA.eq(ns.medicinskaSestra)
                                        .and(OBISK.DEJANSKI.isNull())
                                        .and(OBISK.PREDVIDENI.greaterOrEqual(ns.zacetekCasovnega))
                                        .and(OBISK.PREDVIDENI.lessOrEqual(ns.konecCasovnega))
                                        .and(OBISK.NADOMESTNASESTRA.isNotNull())
                        )
                        .fetchOne(0, int.class) > 0) {
                    res.status(418);
                    return "Ze druga sestra nadomesca v danem obdobju";
                }

                jooq.context.update(OBISK)
                        .set(OBISK.NADOMESTNASESTRA, ns.nadomestnaSestra)
                        .where(
                                OBISK.SESTRA.eq(ns.medicinskaSestra)
                                        // da se ni opravljen
                                        .and(OBISK.DEJANSKI.isNull())
                                        // da je znotraj obmocja
                                        .and(OBISK.PREDVIDENI.greaterOrEqual(ns.zacetekCasovnega))
                                        .and(OBISK.PREDVIDENI.lessOrEqual(ns.konecCasovnega))
                        ).execute();
            }
            return "";
        });
    }
}
