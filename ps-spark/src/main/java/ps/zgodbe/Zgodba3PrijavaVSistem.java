package ps.zgodbe;

import java.util.HashMap;

import generated.tables.pojos.OsebaEntiteta;
import generated.tables.pojos.UporabnikEntiteta;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ps.Avtentikacija;
import ps.JooqContext;
import ps.Server;

import static generated.Tables.*;
import java.sql.Timestamp;

import static ps.Server.gson;
import static spark.Spark.get;
import static spark.Spark.post;

/*

#3 Avtentikacija v sistem

Uporabnik se lahko prijavi v sistem z uporabniškim imenom in geslom.

Preveri, da prijava pacienta v sistem ni možna, dokler uporabniški račun ni aktiviran.
Preveri s pravilnim uporabniškim imenom in geslom.
Preveri s pravilnim uporabniškim imenom in napačnim geslom.
Preveri z napačnim uporabniškim imenom in pravilnim geslom.
Preveri začasno zaklepanje sistema (IP številke) po treh neveljavnih poskusih.
Preveri, da so stalno vidni podatki o prijavljenem uporabniku in njegovi zadnji prijavi.

 */
public class Zgodba3PrijavaVSistem {
	private static final Logger log = LoggerFactory.getLogger(Zgodba3PrijavaVSistem.class);
    
    // koliko ur traja ban
    public static final int ST_POSKUSOV = 2;
    public static final int BAN_TRAJANJE_MIN = 1;
    
    // blacklist vseh IPjev, ki so presegli stevilo dovoljenih prijav
    private static final HashMap<String, Ban> blacklist = new HashMap<>();
    
    // razred, ki hrani podatke za ban
    private static class Ban {
        public int stPoskusov;
        public long banCas;
        Ban(int stPoskusov, long banCas) {
            this.stPoskusov = stPoskusov;
            this.banCas = banCas;
        }
    }
    // za preverjanje bana
    // true ce je prijava iz tega ipja mogoca, false ce ne
    private static boolean preveriIp(String ip) {
        Ban ban = blacklist.get(ip);
        if (ban!= null && ban.stPoskusov>ST_POSKUSOV) {
            log.warn(String.format("Kontaktiran iz IPja v blacklistu: %s", ip));
            // preverimo ce je ze poteklo ()
            if(((System.currentTimeMillis() - ban.banCas)/(60.0e3)) < BAN_TRAJANJE_MIN) {
                return false;
            }
            // ban ze potekel, odstranimo ga iz hash-a
            blacklist.remove(ip);
        }
        // ban bodisi potekel, ali pa ip sploh ni na listi, tko da ok
        return true;
    }
    
    // kar dobimo od klienta
    private static class Prijava {
        String email;
        String geslo;
    }
    // za posiljanje nazaj
    private static enum PrijavaStatus {
        PRIJAVA_OK,         // uspesna prijava
        
        NAPACNA_PRIJAVA,    // v primeru prvih par napacnih poskusov pred banom
        
        BAN                 // dosegli ban, ni druge kot da pocakamo
    }
    private static class PrijavaOdgovor {
        Integer status;
        
        // v primeru, da je IP bannan, vrnemo se stevilo minut do konca
        Integer banPreostaloMinut;
        
        // v primeru, da login uspe, vrnemo login piskotek
        String loginCookie;

        public PrijavaOdgovor(Integer status, Integer banPreostaloMinut, String loginCookie) {
            this.status = status;
            this.banPreostaloMinut = banPreostaloMinut;
            this.loginCookie = loginCookie;
        }
        

        public static String ban(String ip) {
            return Server.gson.toJson(new PrijavaOdgovor(PrijavaStatus.BAN.ordinal(),
                    (int)Math.ceil(BAN_TRAJANJE_MIN - ((System.currentTimeMillis() - blacklist.get(ip).banCas)/(60.0e3))),
                    null));
        }
        public static String ok(String loginCookie) {
            return Server.gson.toJson(new PrijavaOdgovor(PrijavaStatus.PRIJAVA_OK.ordinal(), null, loginCookie));
        }
        public static String fail() {
            return Server.gson.toJson(new PrijavaOdgovor(PrijavaStatus.NAPACNA_PRIJAVA.ordinal(), null, null));
        }
    }
    
    public static void rutine() {
        
        // api klic, da se ugotovi, ali je sploh mogoca prijava iz tega IPja
        // (zato da uporabnik naleti na disablano formo se predno se poskusa
        // prijaviti)
        get("/api/prijava_mogoca", (req, res)->{
            if(preveriIp(req.ip()))
                return PrijavaOdgovor.ok(null);
            else
                return PrijavaOdgovor.ban(req.ip());
        });
        
        // api klic za samo prijavo
        post("/api/login2", (req,res)->{
            Prijava p = Server.gson.fromJson(req.body(), Prijava.class);

            if(!preveriIp(req.ip())) {
                return PrijavaOdgovor.ban(req.ip());
            } 

            try(JooqContext jooq = new JooqContext()) {
                UporabnikEntiteta up = jooq.context.select().from(UPORABNIK).where(UPORABNIK.EMAIL.eq(p.email)).fetchOneInto(UporabnikEntiteta.class);
                // pri checkpw prvi argument je dejasnko geslo, drugi je pa hash(geslo+sol)
                if(up != null && BCrypt.checkpw(p.geslo, up.getGeslo())) {
                    // trenutnemu uporabniku posljemo se timestamp prejsnje prijave
                    // znotraj baze posodobimo prijavo
                    jooq.context
                        .update(UPORABNIK)
                        .set(UPORABNIK.DATUMZADNJEPRIJAVE, new Timestamp(System.currentTimeMillis()))
                        .where(UPORABNIK.ID.eq(up.getId()))
                        .execute();

                    String login_cookie = BCrypt.gensalt();
                    // ce je uporanbik ze prijavljen v sistem
                    // (obstaja ze avtentikacija), odstranimo prejsnjo sejo
                    if(Avtentikacija.emailCookie.containsKey(up.getEmail())) { //ce za nek email pomnimo cookie
                        Avtentikacija.cookieUporabnik.remove(Avtentikacija.emailCookie.get(up.getEmail())); //zbrisemo uporabnika za ta cookie
                        Avtentikacija.emailCookie.remove(up.getEmail()); //zbrisemo za ta email cookie
                    }

                    Avtentikacija.emailCookie.put(up.getEmail(),login_cookie); //shranimo za email cooke
                    Avtentikacija.cookieUporabnik.put(login_cookie, up); //shranimo za cookie uporabnika

                    res.cookie("/","login_cookie", login_cookie, 3600, false, true);
                    // prijava uspesna, client se naj preusmeri na
                    // dashboard
                    log.info(String.format("Uspesna prijava uporabnika '%s'", up.toString()));

                    // odstranimo morebitne prejsnje neuspesne poskuse iz seznama
                    blacklist.remove(req.ip());

                    return PrijavaOdgovor.ok(login_cookie);
                }
                else
                {
                    Ban ban;
                    // napacna prijava
                    if(blacklist.containsKey(req.ip())) {
                        ban = blacklist.get(req.ip());
                        ban.banCas = System.currentTimeMillis();
                        ban.stPoskusov++;
                    }
                    else
                        blacklist.put(req.ip(), ban = new Ban(1, System.currentTimeMillis()));
                    log.warn(String.format("Neuspesna prijava (%d): %s, IP: %s", ban.stPoskusov, (p.email), req.ip()));
                    return PrijavaOdgovor.fail();
                }
            }
        });


        get("/api/logout", (req,res)->{
            res.cookie("/","login_cookie", "", 3600, false, true);
            res.redirect("/");
            return "";
        });

        get("/api/prijavljeni_uporabnik", (req,res)->{
            UporabnikEntiteta u = Avtentikacija.prijavljeniUporabnik(req);
            if(u != null){
                return Server.gson.toJson(u);
            } else {
                res.status(404);
                return false;
            }
        });

        get("/api/prijavljena_oseba", (req,res)->{
            UporabnikEntiteta u = Avtentikacija.prijavljeniUporabnik(req);
            if(u != null){
                try(JooqContext jooq = new JooqContext()){
                    return gson.toJson(jooq.context.select().from(OSEBA).where(OSEBA.ID.eq(u.getId())).fetchOneInto(OsebaEntiteta.class));
                }
            } else {
                res.status(404);
                return false;
            }
        });
    }
}
