package ps.zgodbe;

/*
#1 Kreiranje uporabniškega računa za zdravstveno osebje

Administrator lahko kreira nov uporabniški račun za zdravnika, vodjo patronažne
službe, patronažno sestro in uslužbenca.

Za kreiranje uporabniškega računa mora ustvariti uporabniški profil in določiti geslo.
Uporabniški profil obsega šifro zdravnika/vodje PS/medicinske sestre/uslužbenca,
priimek in ime, šifro izvajalca zdravstvene dejavnosti (npr. zdravstvenega doma), e-mail
in telefonsko številko. E-mail naslov služi kot uporabniško ime, s katerim se zdravstveno
osebje potem prijavlja v sistem. Posebna aktivacija računa ni potrebna.

Preveri, da so izpolnjeni vsi podatki.
Preveri strukturo email naslova.
Preveri ustreznost gesla (najmanj 8 znakov, vsaj en numeričen)
Preveri ujemanje gesel pri prvem in ponovnem vnosu.
 */


import java.util.Random;

import generated.tables.records.OsebaRecordEntiteta;
import generated.tables.records.UporabnikRecordEntiteta;
import generated.tables.records.ZaposleniRecordEntiteta;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import ps.Avtentikacija;
import static ps.RESTUtil.napaka;

import ps.JooqContext;
import ps.Server;
import static ps.Server.sql;
import static generated.Tables.*;

import ps.entitete.enumeracije.*;


import static spark.Spark.get;
import static spark.Spark.post;

public class Zgodba1KreiranjeRacunaZO {
	private static final Logger log = LoggerFactory.getLogger(Zgodba1KreiranjeRacunaZO.class);
    
    private static class Registracija {
        String  ime,
                priimek,
                geslo,
                email,
                ulica,
                hisnastevilka,
                tel;
        Integer sifraIzvajalec,
                sifraZaposleni,
                vloga,
                posta;
         Integer  okolis;
    }

    public static void rutine(){
        
        Random random = new Random();
        // obravnava generiranje nakljucne (neuporabljene) sifre
        get(("/api/pridobi_sifro"), (req,res)->{
            log.info("Generiram novo sifro");
            int novaSifra;
            try (Connection con = sql.open()) {
                // generiramo, dokler ne dobimo sifre, ki se ni uporabljena
                do
                    novaSifra = random.nextInt(100000);
                while((con.createQuery("SELECT count(id) FROM zaposleni WHERE zaposleni.sifrazaposlenega=:sifrazaposlenega")
                        .addParameter("sifrazaposlenega", novaSifra))
                        .executeScalar(Integer.class)>0);
            }
            return String.format("%05d",novaSifra);
        });
        
        // preverjanje ali email ze obstaja
        post(("/api/email_prost"), (req, res)->{
            log.info("Preverjam ali je '"+req.body()+"' prost.");
            boolean emailOk;
            try (Connection con = sql.open()) {
                emailOk = (con.createQuery("SELECT count(id) FROM uporabnik WHERE uporabnik.email=:email")
                    .addParameter("email", req.body())
                    .executeScalar(Integer.class) == 0);
            }
            return emailOk;
        });

        // obravnava kreiranje uporabnika
        post("/api/kreiraj_racun", (req,res)->{
            // uporaba tega api-ja je dovoljeno samo administratorju
            Avtentikacija.zahtevaj(req, 1);
            if(req.body().length() == 0) {
                return napaka(res, "Prazno telo zahteve");
            }
            try {
                Registracija r = Server.gson.fromJson(req.body(), Registracija.class);

                // preverimo, ce slucajno ze ne obstaja kaksen u z danim
                // geslom, sifro zaposlenega ter okolisa ter da je vloga veljavna
                try(JooqContext jooq = new JooqContext()){
                    if (jooq.context.select().from(UPORABNIK).where(UPORABNIK.EMAIL.eq(r.email)).fetchOne() != null)
                            return napaka(res, "Dan e-poštni naslov je že zaseden");
                    
                    //if (!Kraj.check(con, r.posta)) return napaka(res, "Dana pošta ne obstaja");
                    
                    if (jooq.context.select().from(ZAPOSLENI).where(ZAPOSLENI.SIFRAZAPOSLENEGA.eq(r.sifraZaposleni)).fetchOne() != null)
                            return napaka(res, "Šifra zaposlenega že obstaja");
                    
                    if (r.vloga <= 0 || r.vloga > 5)
                            return napaka(res, "Neveljavna vloga uporabnika");
                    
                    // zaenkrat gledamo na podlagi sifranta baze, potrebno je preveriti kako smiselno ga je hraniti tam, glede na
                    // to da se v backendu zanasamo na interni
                    if (jooq.context.select().from(IZVAJALEC).where(IZVAJALEC.ID.eq(r.sifraIzvajalec)).fetchOne() == null)
                            return napaka(res, "Izvajalec ne obstaja");
                    
                    //if (!Okolis.check(con, r.okolis)) return napaka(res, "Okoliš ne obstaja");
                    
                    // preverili, vhod ok

                    OsebaRecordEntiteta o = jooq.context.newRecord(OSEBA);

                    o.setIme(r.ime);
                    o.setPriimek(r.priimek);
                    o.setUlica(r.ulica);
                    o.setHisnastevilka(r.hisnastevilka);
                    o.setPostna(r.posta);
                    o.setTel(r.tel);

                    o.store();


                    // hashamo dobljeno geslo
                    String sol = BCrypt.gensalt();
                    String soljenoGeslo = BCrypt.hashpw(r.geslo, sol);
                    // vstavimo novega uporabnika, ki se sklicuje na vstavljeno osebo
                    // ker je ta u del zdravstvenega osebja, je ze aktiviran
                    // (stanje=aktivirano, brez aktivacijskega hasha)
                    UporabnikRecordEntiteta u = jooq.context.newRecord(UPORABNIK);
                    u.setId(o.getId());
                    u.setEmail(r.email);
                    u.setGeslo(soljenoGeslo);
                    u.setSol(sol);
                    u.setStanje(StanjeAktivacije.AKTIVIRAN.ordinal());
                    u.setVloga(r.vloga);

                    u.store();

                    
                    // vstavimo informacije za zaposlenega
                    ZaposleniRecordEntiteta z = jooq.context.newRecord(ZAPOSLENI);
                    z.setId(o.getId());
                    z.setIzvajalec(r.sifraIzvajalec);
                    z.setSifrazaposlenega(r.sifraZaposleni);
                    if(r != null){
                        z.setOkolis(r.okolis);
                    }
                    z.store();
                }
            }
            catch(RuntimeException e) {
                e.printStackTrace();
                return napaka(res, "Napaka pri interpretaciji podatkov!");
            }
            // vrnemo JSON z indikacijo napake (ce prazen, potem OK)
            res.status(200);
            return "Uporabnik uspešno registriran.\nPreveri email za aktivacijo.";
        });
    }

}
