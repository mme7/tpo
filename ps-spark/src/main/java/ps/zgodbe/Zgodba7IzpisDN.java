package ps.zgodbe;

import generated.tables.pojos.*;
import org.jooq.Record;
import org.jooq.Result;
import ps.JooqContext;
import ps.Sifranti;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static generated.Tables.*;
import static ps.Server.gson;
import static spark.Spark.get;

public class Zgodba7IzpisDN {
    public static void rutine() {
        get("/api/dn/:id", (req,res)->{
            int id = Integer.parseInt(req.params("id"));
            return gson.toJson(pridoviDN(id));
        });
    }


    public static DnZg7 pridoviDN(int id) throws Exception {
        DnZg7 dn = new DnZg7();

        try (JooqContext jooq = new JooqContext()) {

            DelovninalogEntiteta dne = jooq.context.select().from(DELOVNINALOG).where(DELOVNINALOG.ID.eq(id)).fetchOneInto(DelovninalogEntiteta.class);
            dn.id = dne.getId();
            dn.datumizdaje = dne.getDatumizdaje();


            VrstaobiskaEntiteta voe = jooq.context.select().from(VRSTAOBISKA).where(VRSTAOBISKA.ID.eq(dne.getVrstaobiska())).fetchOneInto(VrstaobiskaEntiteta.class);
            dn.vrstaobiska = voe.getIme() + " (" + (voe.getKurativa() ? "kurativa" : "preventiva") + ")";
            dn.vrstaobiskaid = voe.getId();

            ZaposleniEntiteta ize = jooq.context.select().from(ZAPOSLENI).where(ZAPOSLENI.ID.eq(dne.getIzdajatelj())).fetchOneInto(ZaposleniEntiteta.class);
            OsebaEntiteta ioe = jooq.context.select().from(OSEBA).where(OSEBA.ID.eq(dne.getIzdajatelj())).fetchOneInto(OsebaEntiteta.class);
            UporabnikEntiteta iue = jooq.context.select().from(UPORABNIK).where(UPORABNIK.ID.eq(dne.getIzdajatelj())).fetchOneInto(UporabnikEntiteta.class);
            IzvajalecEntiteta iie = jooq.context.select().from(IZVAJALEC).where(IZVAJALEC.ID.eq(ize.getIzvajalec())).fetchOneInto(IzvajalecEntiteta.class);


            dn.izdajatelj = ioe.getIme() + " " + ioe.getPriimek() + ", " + (iue.getVloga() == 2 ? "zdravnik" : "vodja PS") + " (izvajalec: " + iie.getNaziv() + ")";


            Result<Record> pacienti = jooq.context.select().from(PACIENTDN).join(OSEBA).on(PACIENTDN.PACIENT.eq(OSEBA.ID))
                    .where(PACIENTDN.DELOVNINALOG.eq(id))
                    .orderBy(PACIENTDN.VLOGA)
                    .fetch();

            pacienti.forEach(p -> {
                String pstr = p.get(OSEBA.IME) + " " + p.get(OSEBA.PRIIMEK);
                if(dn.pacienti == null){
                    dn.pacienti = pstr;
                } else {
                    dn.pacienti = dn.pacienti + ", " + pstr;
                }

                if(p.get(PACIENTDN.VLOGA) == 1) {
                    dn.ime =  p.get(OSEBA.IME);
                    dn.priimek =  p.get(OSEBA.PRIIMEK);
                } else {
                    OtrokZg9 o9 = new OtrokZg9();
                    o9.id = p.get(OSEBA.ID);
                    o9.imepriimek = pstr;
                    dn.otroci.add(o9);
                }
            });

            List<ZdraviloEntiteta> zdravila = jooq.context.select().from(ZDRAVILODN)
                    .join(ZDRAVILO).on(ZDRAVILODN.ZDRAVILO.eq(ZDRAVILO.ID))
                    .where(ZDRAVILODN.DN.eq(id)).fetchInto(ZdraviloEntiteta.class);

            zdravila.forEach(z -> {
                String zstr = z.getNaziv();
                if(dn.zdravila == null) {
                    dn.zdravila = zstr;
                } else {
                    dn.zdravila =  dn.zdravila + ", " + zstr;
                }

                ZdraviloZg9 z9 = new ZdraviloZg9();
                z9.dn = id;
                z9.zdravilo = z.getId();
                z9.zdravilostr = zstr;
                dn.zdravilodns.add(z9);

            });

            List<MaterialdnEntiteta> epruvete = jooq.context.select().from(MATERIALDN)
                    .where(MATERIALDN.DN.eq(id)).fetchInto(MaterialdnEntiteta.class);

            epruvete.forEach(e -> {
                String zstr = Sifranti.material.stream()
                        .filter(m->m.getId() == e.getMaterial())
                        .findAny().get().getIme()
                        + " (" + e.getKolicina() +"x)";
                if(dn.epruvete == null) {
                    dn.epruvete = zstr;
                } else {
                    dn.epruvete = dn.epruvete + ", " + zstr;
                }

                MaterialZg9 m9 = new MaterialZg9();
                m9.dn = e.getDn();
                m9.kolicina = e.getKolicina();
                m9.material = e.getMaterial();
                m9.materialstr = zstr;
                dn.materialdns.add(m9);

            });
            
            generated.tables.OsebaEntiteta medicinska = OSEBA.as("medicinska");
            generated.tables.OsebaEntiteta nadomestnaMedicinska = OSEBA.as("nadomestna");

            List<ObiskEntiteta> obiski = jooq.context.select().from(OBISK).where(OBISK.DELOVNINALOG.eq(id)).fetchInto(ObiskEntiteta.class);
            Result<Record> os = jooq.context.select().from(OBISK)
                    .leftJoin(medicinska).on(medicinska.ID.eq(OBISK.SESTRA))
                    .leftJoin(nadomestnaMedicinska).on(nadomestnaMedicinska.ID.eq(OBISK.NADOMESTNASESTRA))
                    .where(OBISK.DELOVNINALOG.eq(id))
                    .orderBy(OBISK.PREDVIDENI)
                    .fetch();


            os.forEach(obisk -> {
                Obisk o = new Obisk();
                o.id = obisk.get(OBISK.ID);
                o.predvideni = obisk.get(OBISK.PREDVIDENI);
                o.planirani = obisk.get(OBISK.PLANIRANI);
                o.dejanski = obisk.get(OBISK.DEJANSKI);


                o.sestra = obisk.get(medicinska.IME)+ " " + obisk.get(medicinska.PRIIMEK);
                if(obisk.get(nadomestnaMedicinska.IME) != null){
                    o.nadomestnasestra = obisk.get(nadomestnaMedicinska.IME)+ " " + obisk.get(nadomestnaMedicinska.PRIIMEK);
                }
                o.obvezen = obisk.get(OBISK.OBVEZEN);

                dn.obiski.add(o);
            });


            return dn;
        }
    }

}

class DnZg7 {
    public Integer   id;
    public String   vrstaobiska;
    public String   izdajatelj;
    public Timestamp datumizdaje;
    public String zdravila;

    public String pacienti;
    public String epruvete;

    List<Obisk> obiski = new ArrayList<Obisk>();


    //pri 9
    public String ime; //"glavni" pacient
    public String priimek;
    List<OtrokZg9> otroci = new ArrayList<OtrokZg9>();
    public int vrstaobiskaid;
    List<MaterialZg9> materialdns = new ArrayList<MaterialZg9>();
    List<ZdraviloZg9> zdravilodns = new ArrayList<ZdraviloZg9>();

}


class OtrokZg9{
    int id;
    String imepriimek;
}

class MaterialZg9{
    Integer material;
    Integer dn;
    Integer kolicina;
    String materialstr;
}

class ZdraviloZg9{
    Integer zdravilo;
    Integer dn;
    String zdravilostr;
}


class Obisk{
    public Integer id;
    public Date    predvideni;
    public Date    planirani;
    public Date    dejanski;
    public String sestra;
    public String nadomestnasestra;
    public Boolean obvezen;
}