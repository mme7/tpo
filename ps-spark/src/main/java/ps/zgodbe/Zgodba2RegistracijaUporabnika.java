package ps.zgodbe;

import static generated.tables.OsebaEntiteta.OSEBA;
import static generated.tables.PacientEntiteta.PACIENT;
import static generated.tables.UporabnikEntiteta.UPORABNIK;
import generated.tables.pojos.OsebaEntiteta;
import generated.tables.pojos.PacientEntiteta;
import generated.tables.pojos.UporabnikEntiteta;
import generated.tables.records.OsebaRecordEntiteta;
import generated.tables.records.PacientRecordEntiteta;
import generated.tables.records.UporabnikRecordEntiteta;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;
import org.jooq.Record;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ps.JooqContext;
import ps.Mail;
import ps.Server;
import ps.entitete.enumeracije.SifraVloge;
import ps.entitete.enumeracije.StanjeAktivacije;
import spark.Response;
import static spark.Spark.post;
import static spark.Spark.get;

public class Zgodba2RegistracijaUporabnika {
	private static final Logger log = LoggerFactory.getLogger(Zgodba2RegistracijaUporabnika.class);
    
    private static class OskrbovaniPacient {
        OsebaEntiteta oseba;
        PacientEntiteta pacient;
    }
    
    private static class Registracija {
        OsebaEntiteta registriranaOseba;
        PacientEntiteta registriranPacient;

        UporabnikEntiteta osebaRegistriraniUporabnik;
        OsebaEntiteta osebaRegistriraniKontaktna;
        
        OskrbovaniPacient[] oskrbovaniPacienti;
    }
    
    // utility, da mi ni treba delati oklepajev
    private static String napaka(Response res, String opisNapake) {
        res.status(418);
        return opisNapake;
    }
    
    private static final SecureRandom random = new SecureRandom();
    
    public static void rutine() {
        post("/api/pacient/register", (req, res) -> {
            // client brez piskota lahko uporabi to stran, brez .zahtevaj
            
            try {
                log.info(req.body());
                Registracija r = Server.gson.fromJson(req.body(), Registracija.class);
                
                try (JooqContext jooq = new JooqContext()) {
                    if (jooq.context.selectCount().from(UPORABNIK)
                            .where(UPORABNIK.EMAIL.eq(r.osebaRegistriraniUporabnik.getEmail()))
                            .fetchOne(0, int.class) > 0)
                        return napaka(res, "Dan e-poštni naslov je že zaseden!");
                    
                    if(jooq.context.selectCount().from(PACIENT)
                            .where(PACIENT.SIFRAZAVAROVANJA.eq(r.registriranPacient.getSifrazavarovanja()))
                            .fetchOne(0, int.class) > 0)
                        return napaka(res, "Dan številka ZZZS že obstaja!");
                    // spol, sorodstveno razmerje sta predvidoma ok zaradi form
                    
                    // ce prisli do tu, smo preverili napake
                    // zapisemo novo osebo
                    OsebaRecordEntiteta osebaRecord = jooq.context.newRecord(OSEBA, r.registriranaOseba);
                    osebaRecord.store();
 
                    // hashamo dobljeno geslo
                    String sol = BCrypt.gensalt();
                    r.osebaRegistriraniUporabnik.setSol(sol);
                    r.osebaRegistriraniUporabnik.setGeslo(BCrypt.hashpw(r.osebaRegistriraniUporabnik.getGeslo(), sol));
                    // vstavimo novega uporabnika, ki se sklicuje na vstavljeno osebo
                    // dodamo se id osebe v uporabnika
                    r.osebaRegistriraniUporabnik.setId(osebaRecord.getId());
                    r.osebaRegistriraniUporabnik.setDatumregistracije(new Timestamp(new Date().getTime()));
                    r.osebaRegistriraniUporabnik.setStanje(StanjeAktivacije.NEAKTIVIRAN.ordinal());
                    // generiramo se aktivacijski niz
                    r.osebaRegistriraniUporabnik.setAktivacija(new BigInteger(130, random).toString(32));
                    r.osebaRegistriraniUporabnik.setVloga(SifraVloge.Pacient.ordinal());
                    
                    
                    UporabnikRecordEntiteta uporabnikRecord = jooq.context.newRecord(UPORABNIK, r.osebaRegistriraniUporabnik);
                    uporabnikRecord.store();
                    
                    // ce kontaktna oseba ni izpolnjena, je ta predvidoma null
                    OsebaRecordEntiteta kontaktnaRecord = null;
                    if(r.osebaRegistriraniKontaktna != null) {
                        kontaktnaRecord = jooq.context.newRecord(OSEBA, r.osebaRegistriraniKontaktna);
                        kontaktnaRecord.store();
                    }
                    // dodamo pacienta
                    PacientRecordEntiteta pacientRecord = jooq.context.newRecord(PACIENT, r.registriranPacient);
                    pacientRecord.setId(osebaRecord.getId());
                    if(kontaktnaRecord != null) {
                        pacientRecord.setKontaktnaoseba(kontaktnaRecord.getId());
                        // razmerje je ze doloceno
                    }
                    pacientRecord.store();
                    log.info(String.format("Nov pacient: \n[oseba]\n%s\n[uporabnik]\n%s\n[kontaktna]\n%s\n[pacient]\n%s",
                            osebaRecord, uporabnikRecord, kontaktnaRecord, pacientRecord));
                    
                    // potrebno se dodati vstavljanje za paciente, za katere
                    // uporabnik skrbi
                    for(OskrbovaniPacient tOskrbovaniPacient : r.oskrbovaniPacienti) {
                        OsebaRecordEntiteta tOskrbovanaOsebaRecord = jooq.context.newRecord(OSEBA, tOskrbovaniPacient.oseba);
                        tOskrbovanaOsebaRecord.store();
                        tOskrbovaniPacient.pacient.setId(tOskrbovanaOsebaRecord.getId());
                        PacientRecordEntiteta tOskrbovaniPacientRecord = jooq.context.newRecord(PACIENT, tOskrbovaniPacient.pacient);
                        tOskrbovaniPacientRecord.setSkrbnik(osebaRecord.getId());
                        log.info(String.format("Nov oskrbovani pacient: \n[oseba]\n%s\n[pacient]\n%s",
                                tOskrbovanaOsebaRecord, tOskrbovaniPacientRecord));
                        tOskrbovaniPacientRecord.store();
                    }
                    // tvorimo niz za mail
                    String mail
                            = "<!DOCTYPE html>\n"
                            + "<html>\n"
                            + "<head>\n"
                            + "    <meta charset=\"UTF-8\">\n"
                            + "</head>\n"
                            + "<body>\n"
                            + "    <p>Pozdravljeni "+osebaRecord.getIme()+",</p>\n"
                            + "    <p>za aktivacijo vašega računa na spletnem servisu patronažne službe\n"
                            + "    obiščite <a href=\"http://52.59.97.77:8080/api/aktivacija?id="+uporabnikRecord.getAktivacija()+"\">ta url</a></p>\n"
                            + "    <p>Lep pozdrav,</p>\n"
                            + "    <p>Ekipa 16, Tehnologija Programske Opreme, FRI UNI LJ</p>\n"
                            + "</body>\n"
                            + "</html>";
                    // posljemo aktivacijski mail
                    Mail.posljiEmail(uporabnikRecord.getEmail(), "Aktivacija računa za patronažno službo", mail);
                }
            } catch(RuntimeException e) {
                log.error(String.format("registracija uporabnika - prislo do napake: %s", e.toString()));
                e.printStackTrace();
                return napaka(res, "Napaka pri interpretaciji podatkov!");
            }
            // vrnemo JSON z indikacijo napake (ce prazen, potem OK)
            return true;
        });
        
        get("/api/aktivacija", "application/json", (req, res)-> {
            String id = req.queryParams("id");
            try (JooqContext jooq = new JooqContext()) {
                Optional<Record> uporabnikOptional = jooq.context.select().from(UPORABNIK).where(UPORABNIK.AKTIVACIJA.eq(id)).fetchOptional();
                if(uporabnikOptional.isPresent()) {
                    UporabnikRecordEntiteta upRec = (UporabnikRecordEntiteta)uporabnikOptional.get();
                    upRec.setStanje(StanjeAktivacije.AKTIVIRAN.ordinal());
                    return "Ativacija uspešna!";
                }
            }
            res.status(418);
            return "Aktivacija neuspešna";
        });
    }
}
