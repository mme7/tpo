package ps.zgodbe;


import generated.tables.pojos.*;
import org.jooq.Record;
import org.jooq.Result;
import ps.JooqContext;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static generated.Tables.*;
import static generated.Tables.OBISK;
import org.jooq.Condition;
import ps.Avtentikacija;
import ps.Server;
import static ps.Server.gson;
import ps.entitete.enumeracije.SifraVloge;
import spark.Request;

import javax.swing.*;

import static spark.Spark.get;

public class Zgodba8PlaniranjeObiskov {
    // metoda, ki pridobi vse obiske sestre, ter jih omeji glede na podan
    // pogoj
    private static List<ObiskZg8> pridobiObiske(Request req, Condition c) {
        UporabnikEntiteta uporabnik = Avtentikacija.zahtevaj(req,
                SifraVloge.Medicinska_sestra.ordinal()
        );

        try (JooqContext jooq = new JooqContext()) {
            List<ObiskZg8> os = new ArrayList<>();

            generated.tables.OsebaEntiteta medicinska = OSEBA.as("medicinska");
            generated.tables.OsebaEntiteta nadomestnaMedicinska = OSEBA.as("nadomestna");

            Result<Record> oes = jooq.context.select().from(OBISK)
                    .leftJoin(medicinska).on(medicinska.ID.eq(OBISK.SESTRA))
                    .leftJoin(nadomestnaMedicinska).on(nadomestnaMedicinska.ID.eq(OBISK.NADOMESTNASESTRA))
                    .leftJoin(DELOVNINALOG).on(OBISK.DELOVNINALOG.eq(DELOVNINALOG.ID))
                    .leftJoin(VRSTAOBISKA).on(DELOVNINALOG.VRSTAOBISKA.eq(VRSTAOBISKA.ID))
                    .leftJoin(PACIENTDN).on(PACIENTDN.DELOVNINALOG.eq(DELOVNINALOG.ID))
                    .leftJoin(OSEBA).on(PACIENTDN.PACIENT.eq(OSEBA.ID))
                    .where(
                            (OBISK.SESTRA.eq(uporabnik.getId()).or(OBISK.NADOMESTNASESTRA.eq(uporabnik.getId())))
                                    // 1 pomeni, da je pacient stars oz. obicajna oseba, 2 pa da je otrok starsa
                                    // da ne podvajamo referenc na isti delovni nalog, gledamo le za
                                    // starsa -- to velja le, ce pri vseh obiskih otrok dejansko obiscemo starsa!
                                    .and(PACIENTDN.VLOGA.eq(1))
                                    // dodatni pogoji
                                    .and(c)
                    )
                    .orderBy(OBISK.PREDVIDENI, OBISK.DELOVNINALOG)
                    .fetch();

            oes.forEach(oe->{
                ObiskZg8 o = new ObiskZg8();
                o.id = oe.get(OBISK.ID);
                o.predvideni = oe.get(OBISK.PREDVIDENI);
                o.pacienti = oe.get(OSEBA.IME) + " " + oe.get(OSEBA.PRIIMEK);
                o.planirani = oe.get(OBISK.PLANIRANI);
                o.dejanski = oe.get(OBISK.DEJANSKI);
                o.sestra = oe.get(medicinska.IME)+ " " + oe.get(medicinska.PRIIMEK);
                if(oe.get(nadomestnaMedicinska.IME) != null){
                    o.nadomestnasestra = oe.get(nadomestnaMedicinska.IME)+ " " + oe.get(nadomestnaMedicinska.PRIIMEK);
                }
                o.obvezen = oe.get(OBISK.OBVEZEN);
                o.did = oe.get(OBISK.DELOVNINALOG);
                o.vrstaobiska = oe.get(VRSTAOBISKA.IME);
                os.add(o);
            });

            return os;
        }
        catch (Exception e) {
            throw new RuntimeException();
        }
    }


    public static void rutine() {
        // tu se vrnejo obiski le za sestro, ki je zadolzena za obisk, bodisi
        // redno ali kot nadomestna sestra

        // doda (okviren) obisk v plan
        get("/api/obisk/dodajvplan/:dan/:obisk", (req,res)-> {
            String danstr = req.params("dan");
            Date dan = Date.valueOf(danstr);
            int obisk = Integer.parseInt(req.params("obisk"));
            try (JooqContext jooq = new JooqContext()) {
                jooq.context.update(OBISK)
                        .set(OBISK.PLANIRANI, dan)
                        .where(OBISK.ID.eq(obisk))
                        .execute();
            }
            return "";
        });
        // odstrani (okviren) obisk iz plana

        // logika za client: obisk ki je ze bil opravljen (cetudi se gre za
        // danasnji datum), se ne sme vec odstraniti iz seznama
        get("/api/obisk/odstraniizplana/:obisk", (req,res)-> {
            int obisk = Integer.parseInt(req.params("obisk"));
            try (JooqContext jooq = new JooqContext()) {
                jooq.context.update(OBISK)
                        .set(OBISK.PLANIRANI, (Date)null)
                        .where(OBISK.ID.eq(obisk))
                        .execute();
            }
            return "";
        });

        // vrne seznam (okvirnih) obiskov, ki jih je mogoce dodati v plan
        // vrnejo se obiski pred in po danasnjem datumu (torej vsi, ki so
        // namenjeni za sestro)
        get("/api/obisk/neuvrsceni_obiski", (req,res)->{
            return Server.gson.toJson(pridobiObiske(req,
                    OBISK.PLANIRANI.isNull()
            ));
        });

        // vrne seznam vseh planiranih obiskov
        // - to se uporabi za pregled vseh obiskov
        get("/api/obisk/planirani_obiski", (req,res)-> {
            Date danes = new Date(Calendar.getInstance().getTimeInMillis());
            Date vceraj = new Date(Calendar.getInstance().getTimeInMillis()-3600l*24*1000);
            return Server.gson.toJson(pridobiObiske(req,
                    OBISK.PLANIRANI.isNotNull().and(OBISK.PLANIRANI.eq(danes).or(OBISK.PLANIRANI.eq(vceraj)))
            ));
        });

        // vrne seznam obiskov, ki so za podani dan ze planirani in se niso
        // opravljeni
        get("/api/obisk/planirani_obiski/:dan", (req,res)->{
            String danstr = req.params("dan");
            Date dan =  Date.valueOf(danstr);
            return gson.toJson(pridobiObiske(req,
                    OBISK.PLANIRANI.eq(dan)
                            /*.and(OBISK.DEJANSKI.isNull())*/
            ));
        });
    }
}

class ObiskZg8{
    public Integer id;  // id obiska
    public Integer did; // id delovnega naloga
    public Date predvideni;
    public Date    planirani;
    public Date    dejanski;
    public String sestra;
    public String nadomestnasestra;
    public Boolean obvezen;
    public String pacienti;
    public String vrstaobiska;
}