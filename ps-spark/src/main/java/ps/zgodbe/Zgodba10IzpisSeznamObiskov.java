package ps.zgodbe;

import static generated.Tables.DELOVNINALOG;
import static generated.Tables.OBISK;
import static generated.Tables.PACIENT;
import static generated.Tables.PACIENTDN;
import static generated.Tables.VRSTAOBISKA;
import generated.tables.OsebaEntiteta;
import static generated.tables.OsebaEntiteta.OSEBA;
import generated.tables.PacientEntiteta;
import generated.tables.pojos.UporabnikEntiteta;
import static spark.Spark.post;

import java.sql.Date;

import java.util.List;
import org.jooq.Condition;
import org.jooq.impl.DSL;
import static org.jooq.impl.DSL.choose;
import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ps.Avtentikacija;
import ps.JooqContext;
import ps.Server;
import static ps.Server.gson;
import ps.entitete.enumeracije.SifraVloge;

public class Zgodba10IzpisSeznamObiskov {

    private static final Logger log = LoggerFactory.getLogger(Zgodba10IzpisSeznamObiskov.class);

    private class Filtri {

        Date ObiskPredvideniOD;
        Date ObiskPredvideniDO;

        Date ObiskDejanskiOD;
        Date ObiskDejanskiDO;
    }

    private static Condition addCon(Condition wc, Condition c) {
        return wc != null ? wc.and(c) : c;
    }

    private static class PodatkiVrstice {

        Integer TabIdObiska;
        String TabVrstaObiska;
        Date TabPredvideniDatum;
        Date TabDejanskiDatum;
        String TabIzdajatelj;
        String TabZadolzenaSestra;
        String TabNadomestnaSestra;
        String TabKategorija;
        String TabPacient;

        public PodatkiVrstice() {
        }

        public PodatkiVrstice(int TabIdObiska, String TabVrstaObiska, Date TabPredvideniDatum, Date TabDejanskiDatum, String TabIzdajatelj, String TabZadolzenaSestra, String TabNadomestnaSestra, String TabKategorija, String TabPacient) {
            this.TabIdObiska = TabIdObiska;
            this.TabVrstaObiska = TabVrstaObiska;
            this.TabPredvideniDatum = TabPredvideniDatum;
            this.TabDejanskiDatum = TabDejanskiDatum;
            this.TabIzdajatelj = TabIzdajatelj;
            this.TabZadolzenaSestra = TabZadolzenaSestra;
            this.TabNadomestnaSestra = TabNadomestnaSestra;
            this.TabKategorija = TabKategorija;
            this.TabPacient = TabPacient;
        }

    }

    public static void rutine() {
        post("/api/filtrirani_obiski", (req, res) -> {
            UporabnikEntiteta uporabnik = Avtentikacija.zahtevaj(req,
                    SifraVloge.Administrator.ordinal(),
                    SifraVloge.Vodja_patronazne_sluzbe.ordinal(),
                    SifraVloge.Medicinska_sestra.ordinal(),
                    SifraVloge.Zdravnik.ordinal(),
                    SifraVloge.Pacient.ordinal()
            );

            // omejimo iskalne filtre glede na prijavljeno osebo
            Condition wc = null;

            OsebaEntiteta izdajateljOseba = OSEBA.as("izdajatelj_oseba");
            OsebaEntiteta zadolzenaSestraOseba = OSEBA.as("zadolzena_sestra_oseba");
            OsebaEntiteta nadomestnaSestraOseba = OSEBA.as("nadomestna_sestra_oseba");
            OsebaEntiteta pacientOseba = OSEBA.as("pacient_oseba");
            PacientEntiteta pacientPacient = PACIENT.as("pacient_pacient");

            // pridobimo vrednosti filtrov
            log.info(String.format("Dobil [%s]", req.body()));
            Filtri filtri = Server.gson.fromJson(req.body(), Zgodba10IzpisSeznamObiskov.Filtri.class);

            if (uporabnik.getVloga() == SifraVloge.Zdravnik.ordinal()) {
                wc = addCon(wc, DELOVNINALOG.IZDAJATELJ.eq(uporabnik.getId()));
            }

            if (uporabnik.getVloga() == SifraVloge.Medicinska_sestra.ordinal()) {
                wc = addCon(wc, zadolzenaSestraOseba.ID.eq(uporabnik.getId()).or(nadomestnaSestraOseba.ID.eq(uporabnik.getId())));
            }

            // filtriranje za pacienta (16. zgodba)
            if (uporabnik.getVloga() == SifraVloge.Pacient.ordinal()) {
                // pregledamo lahko tudi za paciente, katerim smo njihov skrbnik
                wc = addCon(wc, pacientOseba.ID.eq(uporabnik.getId()).or(pacientPacient.SKRBNIK.eq(uporabnik.getId())));
                wc = addCon(wc, OBISK.DEJANSKI.isNotNull());
            }

            // vodja patronazne sluzbe vidi vse obiske (ni pogojev za omejitev)
            // preverjamo le ce je uporabnik podal vsaj kaksnega izmed kriterijev
            if (filtri != null) {
                if (filtri.ObiskDejanskiOD != null) {
                    wc = addCon(wc, OBISK.DEJANSKI.greaterOrEqual(filtri.ObiskDejanskiOD));
                }
                if (filtri.ObiskDejanskiDO != null) {
                    wc = addCon(wc, OBISK.DEJANSKI.lessOrEqual(filtri.ObiskDejanskiDO));
                }

                if (filtri.ObiskPredvideniOD != null) {
                    wc = addCon(wc, OBISK.PREDVIDENI.greaterOrEqual(filtri.ObiskPredvideniOD));
                }
                if (filtri.ObiskPredvideniDO != null) {
                    wc = addCon(wc, OBISK.PREDVIDENI.lessOrEqual(filtri.ObiskPredvideniDO));
                }
            }

            // ce ni nobenega filtra, potem nastavimo pogoj tako, da bo
            // vedno veljaven
            if (wc == null) {
                wc = DSL.trueCondition();
            }

            try (JooqContext jooq = new JooqContext()) {

                List<PodatkiVrstice> pv
                        = jooq.context.select(
                                OBISK.ID.as("TabIdObiska"),
                                VRSTAOBISKA.IME.as("TabVrstaObiska"),
                                OBISK.PREDVIDENI.as("TabPredvideniDatum"),
                                OBISK.DEJANSKI.as("TabDejanskiDatum"),
                                concat(izdajateljOseba.IME, val(" "), izdajateljOseba.PRIIMEK).as("TabIzdajatelj"),
                                concat(zadolzenaSestraOseba.IME, val(" "), zadolzenaSestraOseba.PRIIMEK).as("TabZadolzenaSestra"),
                                // TabNadomestnaSestra se nastavi posebej, zato ker je potrebna dodatna logika za dolocitev vsebine
                                (choose(VRSTAOBISKA.KURATIVA)
                                        .when(true, "kurativa")
                                        .when(false, "preventiva")).as("TabKategorija"),
                                concat(pacientOseba.IME, val(" "), pacientOseba.PRIIMEK).as("TabPacient")
                        ).from(
                                DELOVNINALOG
                                        .join(VRSTAOBISKA).on(DELOVNINALOG.VRSTAOBISKA.eq(VRSTAOBISKA.ID))
                                        .join(OBISK).on(OBISK.DELOVNINALOG.eq(DELOVNINALOG.ID))
                                        .join(izdajateljOseba).on(DELOVNINALOG.IZDAJATELJ.eq(izdajateljOseba.ID))
                                        .join(zadolzenaSestraOseba).on(OBISK.SESTRA.eq(zadolzenaSestraOseba.ID))
                                        .join(PACIENTDN).on(PACIENTDN.VLOGA.eq(1).and(PACIENTDN.DELOVNINALOG.eq(DELOVNINALOG.ID)))
                                        .join(pacientOseba).on(PACIENTDN.PACIENT.eq(pacientOseba.ID))
                                        .join(pacientPacient).on(pacientPacient.ID.eq(pacientOseba.ID))
                                        .leftJoin(nadomestnaSestraOseba).on(OBISK.NADOMESTNASESTRA.eq(nadomestnaSestraOseba.ID))
                        ).where(
                                wc
                        ).orderBy(
                                DELOVNINALOG.ID.asc(),
                                OBISK.STEVILKAOBISKA.asc()
                        ).fetchInto(PodatkiVrstice.class);
                
                // dodamo se podatke o nadomestni patronazni sestri, ce je ta zabelezena
                pv.stream().forEach(tPv -> {
                    generated.tables.pojos.OsebaEntiteta nS = jooq.context.select()
                            .from(OBISK.join(OSEBA).on(OBISK.NADOMESTNASESTRA.eq(OSEBA.ID)))
                            .where(OBISK.ID.eq(tPv.TabIdObiska))
                            .fetchOneInto(generated.tables.pojos.OsebaEntiteta.class);
                    tPv.TabNadomestnaSestra = (nS != null) ? nS.getIme() + " " + nS.getPriimek() : "/";
                });
                return gson.toJson(pv);
            }
        });
    }
}
