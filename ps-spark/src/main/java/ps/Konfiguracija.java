package ps;

/**
 * Pomozni razred za hranjenje konfiguracije.
 *
 * Se uporablja znotraj inicializacije(), inicializira se direktno iz
 * datotecnega sistema (JSON formatirane datoteke).
 * @author tpecar
 */
public class Konfiguracija {
    public final String databaseHost;
    public final String databasePort;
    public final String database;
    public final String databaseUser;
    public final String databasePassword;
    public final String mail;
    public final String mailPassword;
    public final int webServerPort;
    public final String debugBrowser;
    public final String debugURL;
    public final int debugOid;
    public final String angularDist;
    public boolean debug;


    public Konfiguracija(Konfiguracija konfiguracija) {
        this.databaseHost = konfiguracija.databaseHost;
        this.databasePort = konfiguracija.databasePort;
        this.database = konfiguracija.database;
        this.databaseUser = konfiguracija.databaseUser;
        this.databasePassword = konfiguracija.databasePassword;
        this.mail = konfiguracija.mail;
        this.mailPassword = konfiguracija.mailPassword;
        this.webServerPort = konfiguracija.webServerPort;
        this.debugBrowser = konfiguracija.debugBrowser;
        this.debugURL = konfiguracija.debugURL;
        this.debugOid = konfiguracija.debugOid;
        this.angularDist = konfiguracija.angularDist;
    }
}