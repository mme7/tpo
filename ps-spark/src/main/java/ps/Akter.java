package ps;


import generated.tables.pojos.*;

import java.sql.Date;
import java.sql.Timestamp;


public class Akter {
    public OsebaEntiteta oseba;
    private UporabnikEntiteta uporabnik;
    public PacientEntiteta pacient;
    private ZaposleniEntiteta zaposleni;


    //--------  oseba
    public Integer getId() {
        return oseba.getId();
    }

    public void setId(Integer id) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }
        oseba.setId(id);
    }

    public String getIme() {
        return oseba.getIme();
    }

    public void setIme(String ime) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }
        oseba.setIme(ime);
    }

    public String getPriimek() {
        return oseba.getPriimek();
    }

    public void setPriimek(String priimek) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }
        oseba.setPriimek(priimek);
    }

    public String getTel() {
        return oseba.getTel();
    }

    public void setTel(String tel) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }
        oseba.setTel(tel);
    }

    public String getUlica() {
        return oseba.getUlica();
    }

    public void setUlica(String ulica) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }

        oseba.setUlica(ulica);
    }

    public String getHisnastevilka() {
        return oseba.getHisnastevilka();
    }

    public void setHisnastevilka(String hisnastevilka) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }

        oseba.setHisnastevilka(hisnastevilka);
    }

    public Integer getPostna() {
        return oseba.getPostna();
    }

    public void setPostna(Integer postna) {
        if (oseba == null) {
            oseba = new OsebaEntiteta();
        }

        oseba.setPostna(postna);
    }


    //--------  uporabnik

    public Integer getVloga() {
        return uporabnik.getVloga();
    }

    public void setVloga(Integer vloga) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setVloga(vloga);
    }



    public String getEmail() {
        return uporabnik.getEmail();
    }

    public void setEmail(String email) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setEmail(email);
    }

    public String getGeslo() {
        return  uporabnik.getGeslo();
    }

    public void setGeslo(String geslo) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setGeslo(geslo);
    }

    public String getSol() {
        return uporabnik.getSol();
    }

    public void setSol(String sol) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setSol(sol);
    }

    public Integer getStanje() {
        return uporabnik.getStanje();
    }

    public void setStanje(Integer stanje) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setStanje(stanje);
    }

    public String getAktivacija() {
        return uporabnik.getAktivacija();
    }

    public void setAktivacija(String aktivacija) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setAktivacija(aktivacija);
    }

    public Timestamp getDatumregistracije() {
        return uporabnik.getDatumregistracije();
    }

    public void setDatumregistracije(Timestamp datumregistracije) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setDatumregistracije(datumregistracije);
    }

    public Timestamp getDatumzadnjeprijave() {
        return uporabnik.getDatumzadnjeprijave();
    }

    public void setDatumzadnjeprijave(Timestamp datumzadnjeprijave) {
        if (uporabnik == null) {
            uporabnik = new UporabnikEntiteta();
        }
        uporabnik.setDatumzadnjeprijave(datumzadnjeprijave);
    }



    //--------  pacient

    public Integer getSifrazavarovanja() {
        return pacient.getSifrazavarovanja();
    }

    public void setSifrazavarovanja(Integer sifrazavarovanja) {

        if (pacient == null) {
            pacient = new PacientEntiteta();
        }

        pacient.setSifrazavarovanja(sifrazavarovanja);
    }

    public Date getDatumrojstva() {
        return pacient.getDatumrojstva();
    }

    public void setDatumrojstva(Date datumrojstva) {
        if (pacient == null) {
            pacient = new PacientEntiteta();
        }
        pacient.setDatumrojstva(datumrojstva);
    }

    public Short getSpol() {
        return pacient.getSpol();
    }

    public void setSpol(Short spol) {
        if (pacient == null) {
            pacient = new PacientEntiteta();
        }
        pacient.setSpol(spol);
    }

    public Integer getKontaktnaoseba() {
        return pacient.getKontaktnaoseba();
    }

    public void setKontaktnaoseba(Integer kontaktnaoseba) {
        if (pacient == null) {
            pacient = new PacientEntiteta();
        }
        pacient.setKontaktnaoseba(kontaktnaoseba);
    }

    public Short getKontaktnaosebarazmerje() {
        return pacient.getKontaktnaosebarazmerje();
    }

    public void setKontaktnaosebarazmerje(Short kontaktnarazmerje) {
        if (pacient == null) {
            pacient = new PacientEntiteta();
        }
        pacient.setKontaktnaosebarazmerje(kontaktnarazmerje);
    }

    public Integer getSkrbnik() {
        return pacient.getSkrbnik();
    }

    public void setSkrbnik(Integer skrbnik) {
        if (pacient == null) {
            pacient = new PacientEntiteta();
        }
        pacient.setSkrbnik(skrbnik);
    }


    //--------  pacient


    public Integer getSifrazaposlenega() {
        return zaposleni.getSifrazaposlenega();
    }

    public void setSifrazaposlenega(Integer sifrazaposlenega) {
        if (zaposleni == null) {
            zaposleni = new ZaposleniEntiteta();
        }
        zaposleni.setSifrazaposlenega(sifrazaposlenega);
    }

    public Integer getIzvajalec() {
        return zaposleni.getIzvajalec();
    }

    public void setIzvajalec(Integer izvajalec) {
        if (zaposleni == null) {
            zaposleni = new ZaposleniEntiteta();
        }
        zaposleni.setIzvajalec(izvajalec);
    }




    //--------------- eh


    public Integer getOkolis() {
        return pacient != null ? pacient.getOkolis() : zaposleni.getOkolis();
    }

    public void setOkolis(Integer okolis) {
        if (pacient == null) {
            pacient = new PacientEntiteta();
        }
        pacient.setOkolis(okolis);

        if (zaposleni == null) {
            zaposleni = new ZaposleniEntiteta();
        }
        zaposleni.setOkolis(okolis);
    }


}
