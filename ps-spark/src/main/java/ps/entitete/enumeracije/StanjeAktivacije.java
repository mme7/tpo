package ps.entitete.enumeracije;

/**
 * Hrani ali je racun aktiviran/brisan.
 * Zato da nimamo enum tabel v bazi.
 * 
 * @author tpecar
 */
public enum StanjeAktivacije {
    NEAKTIVIRAN,
    IZBRISAN,
    AKTIVIRAN;
}
