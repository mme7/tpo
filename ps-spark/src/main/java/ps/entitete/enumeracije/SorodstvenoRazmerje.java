package ps.entitete.enumeracije;

import com.google.gson.annotations.SerializedName;

public enum SorodstvenoRazmerje {
    
    @SerializedName("0")
    MOZ_ZENA("Mož/Žena"),
    @SerializedName("1")
    BRAT_SESTRA("Brat/Sestra"),
    @SerializedName("2")
    HCER_SIN("Hčer/Sin"),
    @SerializedName("3")
    DEDEK_BABICA("Dedek/Babica"),
    @SerializedName("4")
    PARTNER("Partner");
    
    private final String opis;
    
    SorodstvenoRazmerje(String opis) {
        this.opis = opis;
    }
    @Override
    public String toString() {
        return opis;
    }
}
