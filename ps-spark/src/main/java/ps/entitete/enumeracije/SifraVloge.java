package ps.entitete.enumeracije;

import com.google.gson.annotations.SerializedName;

public enum SifraVloge {
    @SerializedName("0")
    Pacient,

    @SerializedName("1")
    Administrator,

    @SerializedName("2")
    Zdravnik,

    @SerializedName("3")
    Vodja_patronazne_sluzbe,

    @SerializedName("4")
    Medicinska_sestra,

    @SerializedName("5")
    Usluzbenec,
}
