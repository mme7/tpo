package ps;

import java.lang.reflect.Field;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Razred za utility funkcije.
 */
public class Util {
	private static final Logger log = LoggerFactory.getLogger(Util.class);
    
    /**
     * Kopira (shallow copy) vse istoimenske atribute iz enega objekta v drugega.
     * Pri tem se NE upostevajo atributi, ki jih razred podeduje.
     * 
     * Pri tem lahko dolocimo, ce zahtevamo ali
     *  - mora biti source obvezno podmnozica targeta (vsaj to kar je v sourcu mora biti tudi v targetu)
     *  - mora biti target obvezno podmnozica sourcea (vsaj to kar je v targetu mora biti tudi v sourcu)
     * Ob tem lahko podamo mnozico atributov v sourcu, ki se naj ignorirajo.
     * @param source objekt iz katerega kopiramo
     * @param target objekt v katerega kopiramo
     * @param targetPodmnozicaSource vsi atributi v target morajo biti v source
     * @param sourcePodmnozicaTarget vsi atributi v source morajo biti v target
     * @param ignorirano atributi (bodisi znotraj source ali target), ki se ne kopirajo in posledicno
     *                   ne preverjajo za prisotnost
     */
    public static void kopirajAtribute(
            Object source, Object target,
            boolean sourcePodmnozicaTarget, boolean targetPodmnozicaSource,
            String[] ignorirano) {
        Field[] sourceFields = source.getClass().getDeclaredFields();
        Boolean[] sourceUsed = new Boolean[sourceFields.length];
        Arrays.fill(sourceUsed, false);
        
        Field[] targetFields = target.getClass().getDeclaredFields();
        // pove, ali je bilo dano polje (na isti poziciji) uporabljeno
        Boolean[] targetUsed = new Boolean[targetFields.length];
        Arrays.fill(targetUsed, false);
        
        for(int curSourceIdx = 0; curSourceIdx < sourceFields.length; curSourceIdx++) {
            Field curSourceField = sourceFields[curSourceIdx];
            if(Arrays.stream(ignorirano).anyMatch(cif -> cif.equals(curSourceField.getName()))) {
                if(sourcePodmnozicaTarget)
                    sourceUsed[curSourceIdx] = true;
                continue;
            }

            for(int curTargetIdx = 0; curTargetIdx < targetFields.length; curTargetIdx++) {
                Field curTargetField = targetFields[curTargetIdx];
                if(Arrays.stream(ignorirano).anyMatch(cif -> cif.equals(curTargetField.getName()))) {
                    if(targetPodmnozicaSource)
                        targetUsed[curTargetIdx] = true;
                    continue;
                }
                
                // matchamo po imenu
                if (curSourceField.getName().equals(curTargetField.getName())) {
                    // kopiramo le ce je tip enak, drugace exception
                    if (curSourceField.getType().equals(curTargetField.getType())) {
                        sourceUsed[curSourceIdx] = true;
                        targetUsed[curTargetIdx] = true;
                        
                        curSourceField.setAccessible(true);
                        curTargetField.setAccessible(true);
                        try {
                            curTargetField.set(target, curSourceField.get(source));
                        } catch(IllegalAccessException e) {
                            throw new RuntimeException(String.format("Ni bilo mogoce kopirati %s, dobil %s",
                                    curTargetField.getName(),
                                    e.toString()));
                        }
                    }
                    else
                        throw new RuntimeException(String.format(
                                "Tipa istoimenskih tipov nista enaka (source %s : %s , target %s : %s)",
                                curSourceField.getName(), curSourceField.getType().toGenericString(),
                                curTargetField.getName(), curTargetField.getType().toGenericString()));
                }
            }
        }
        if(targetPodmnozicaSource)
            if(!Arrays.stream(targetUsed).allMatch(curTargetField -> curTargetField))
                throw new RuntimeException("Target ni podmnozica source");
        if(sourcePodmnozicaTarget)
            if(!Arrays.stream(sourceUsed).allMatch(curSourceField -> curSourceField))
                throw new RuntimeException("Source ni podmnozica target");
    }
    /*
     * Enako prvotni razlicici, a brez ignoriranja.
     */
    public static void kopirajAtribute(
            Object source, Object target,
            boolean sourcePodmnozicaTarget, boolean targetPodmnozicaSource) {
        kopirajAtribute(source, target, sourcePodmnozicaTarget, targetPodmnozicaSource, new String[]{});
    }
}
