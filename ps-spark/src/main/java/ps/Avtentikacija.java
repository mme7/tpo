package ps;

import generated.tables.pojos.UporabnikEntiteta;
import generated.tables.pojos.VlogaEntiteta;
import generated.tables.pojos.VrstaobiskaEntiteta;
import spark.Request;
import java.util.Arrays;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static ps.Server.gson;
import static spark.Spark.halt;
import static spark.Spark.get;
import static generated.Tables.*;

public class Avtentikacija {
private static final Logger log = LoggerFactory.getLogger(Avtentikacija.class);

    public static HashMap<String, UporabnikEntiteta> cookieUporabnik = new HashMap<>(); //iz piskota dobimo uporabnika
    public static HashMap<String, String> emailCookie = new HashMap<>(); //pri vpisu izbrisemo star piskot(ce obstaja)
    public static UporabnikEntiteta prijavljeniUporabnik(Request req){
        String cookie = req.cookie("login_cookie");
        log.info(String.format("Klient posredoval piskot [%s]", cookie));
        
        if(cookie == null && Server.konfiguracija.debug) {
            log.info(String.format("Uporabljen debug uporabnik %s", Server.debugUporabnik.toString()));
            return Server.debugUporabnik;
        }
        if(cookie != null && cookieUporabnik.containsKey(cookie)){
            return cookieUporabnik.get(cookie);
        } else
            return null;
    }

    public static UporabnikEntiteta zahtevaj(Request req, int... vloge){
        UporabnikEntiteta u = prijavljeniUporabnik(req);
        if(u == null || (vloge.length != 0 && !Arrays.stream(vloge).anyMatch(s-> s == u.getVloga()))){
            halt(401, "Nedovoljen dostop");
            return null;
        } else {
            return u;
        }
    }
    
    // za potrebe avtentikacije s strani frontenda
    public static void rutine() {
        get("/api/seznam-vlog", (req,res)->{
            try(JooqContext jooq = new JooqContext()){
                return gson.toJson(
                        jooq.context.select().from(VLOGA).fetchInto(VlogaEntiteta.class)
                );
            }
        });

        get("/api/seznam-izvajalcev", (req,res)->{
            try(JooqContext jooq = new JooqContext()){
                return gson.toJson(
                        jooq.context.select().from(IZVAJALEC).join(KRAJ).on(IZVAJALEC.POSTNA.eq(KRAJ.POSTNA)).fetchInto(Izvajalec.class)
                );
            }
        });


        get("/api/seznam-vrstobiska", (req,res)->{
            try(JooqContext jooq = new JooqContext()){
                return gson.toJson(
                        jooq.context.select().from(VRSTAOBISKA).fetchInto(VrstaobiskaEntiteta.class)
                );
            }
        });
    }

}

class Izvajalec {
    public Integer id;
    public String naziv;
    public String ulica;
    public String hisna;
    public Integer postna;
    public String imekraja;
}