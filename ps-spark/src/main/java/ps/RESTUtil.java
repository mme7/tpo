package ps;

import spark.Response;

public class RESTUtil {
    /**
     * Da nekoliko okrajsamo ter standardiziramo vracanje napak.
     * 
     * @param res response objekt kateremu pripnemo status
     * @param opisNapake sam opis napake
     * @return preoblikovan response, ki ga vracamo iz rest handlerja
     */
    public static String napaka(Response res, String opisNapake) {
        res.status(418);
        return opisNapake;
    }
}
