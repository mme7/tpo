package ps;

import static generated.Tables.*;
import generated.tables.PacientEntiteta;

import java.util.Date;
import java.util.List;
import static spark.Spark.*;

import static ps.Server.*;
import static ps.Server.gson;
import ps.zgodbe.*;

public class Main {
    // razred za API klic uporabniki
    private static class UporabnikAPI {
        public int id;

        public String email;
        public String tel;
        public Date prijava;
        public Date datumregistracije;
        public Date datumzadnjeprijave;
        public Integer vloga;
    }


    public static void main(String[] args) {
        inicializacije();

        get("/api/admin/uporabniki", (req, res) -> {
            //Avtentikacija.zahtevaj(req, Administrator);
            try (JooqContext jooq = new JooqContext()) {
                //return gson.toJson(jooq.context.select().from(UPORABNIK).fetchInto(UporabnikEntiteta.class));
                return gson.toJson(jooq.context.select().from(UPORABNIK).join(OSEBA).on(UPORABNIK.ID.eq(OSEBA.ID)).fetchInto(Akter.class));
            }
        });

        get("/api/uporabnik/:starsid/otroci", (req,res)->{
            try(JooqContext jooq = new JooqContext()){
                int starsid = Integer.parseInt(req.params("starsid"));

                PacientEntiteta otrok = PACIENT.as("otrok");
                PacientEntiteta stars = PACIENT.as("stars");

                List<Akter> otroci = jooq.context.select().from(stars)
                                        .join(otrok).on(otrok.SKRBNIK.eq(stars.ID))
                                        .join(OSEBA).on(otrok.ID.eq(OSEBA.ID))
                                        .where(stars.ID.eq(starsid))
                                        .fetchInto(Akter.class);

                return gson.toJson(otroci);
            }
        });

        Avtentikacija.rutine();
        
        Zgodba1KreiranjeRacunaZO.rutine();
        
        Zgodba2RegistracijaUporabnika.rutine();

        Zgodba3PrijavaVSistem.rutine();
        
        Zgodba4SpremembaGesla.rutine();

        Zgodba5KreiranjeDN.rutine();
        
        Zgodba6IzpisFiltrov.rutine();

        Zgodba7IzpisDN.rutine();

        Zgodba8PlaniranjeObiskov.rutine();

        Zgodba9VnosPopravljanjeObiska.rutine();

        Zgodba10IzpisSeznamObiskov.rutine();

        Zgodba11IzpisPodatkoObiska.rutine();

        Zgodba12DolocitevNadomestne.rutine();
        
        Zgodba16SeznamObiskovPacient.rutine();
    }


}
