package ps;

// pazi, za samo fetchanje uporabi POJO razred, ne .tables obliko!
import static generated.Tables.*;

import generated.tables.pojos.*;
import org.sql2o.Connection;

import java.util.List;

import static ps.Server.gson;

import static ps.Server.sql;
import static spark.Spark.get;



public class Sifranti {
    // sifranti, ki se predvidoma ne bodo veliko spreminjali, bodo prebrani le
    // ob zagonu streznika
    /*private static class Razmerje {
        Integer id;
        String opis;
        Razmerje(Integer idx, String opis) {
            this.id = idx;
            this.opis = opis;
        }
    }*/

    public static List<VlogaEntiteta> vloge;
    public static List<MaterialEntiteta> material;
    public static List<ZdraviloEntiteta> zdravila;
    public static List<VrstaobiskaEntiteta> vrsteobiska;
    public static List<SorodstvenorazmerjeEntiteta> sorodstvenazamerja;
    public static List<OkolisEntiteta> okolisi;
    public static List<KrajEntiteta> kraji;

    static {
        try (JooqContext jooq = new JooqContext()) {
            vloge = jooq.context.select().from(VLOGA).fetchInto(VlogaEntiteta.class);
            material = jooq.context.select().from(MATERIAL).fetchInto(MaterialEntiteta.class);
            zdravila = jooq.context.select().from(ZDRAVILO).fetchInto(ZdraviloEntiteta.class);
            vrsteobiska = jooq.context.select().from(VRSTAOBISKA).fetchInto(VrstaobiskaEntiteta.class);
            sorodstvenazamerja = jooq.context.select().from(SORODSTVENORAZMERJE).fetchInto(SorodstvenorazmerjeEntiteta.class);
            okolisi = jooq.context.select().from(OKOLIS).fetchInto(OkolisEntiteta.class);
            kraji = jooq.context.select().from(KRAJ).fetchInto(KrajEntiteta.class);
        } catch (Exception e) {
            throw new RuntimeException("Sifranti init fail.");
        }
    }

    public static void vzpostavitev() {

        get("/api/seznam-vlog", (req, res) -> {
            return gson.toJson(vloge);
        });

        get("/api/seznam-materiala", (req, res) -> {
            return gson.toJson(material);
        });

        get("/api/seznam-zdravil", (req, res) -> {
            return gson.toJson(zdravila);
        });

        get("/api/vrstaobiska", (req, res) -> {
            return gson.toJson(vrsteobiska);
        });

        get("/api/sorodstvenorazmerje", (req, res) -> {
            return gson.toJson(sorodstvenazamerja);
        });

        get("/api/okolis", (req, res) -> {
            return gson.toJson(okolisi);
        });

        get("/api/kraj", (req, res) -> {
            return gson.toJson(kraji);
        });
    }
}
