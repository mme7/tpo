package ps;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.sql2o.Connection;

public class JooqContext implements AutoCloseable {
    public DSLContext context;
    private Connection con;
    public JooqContext(){
        con = Server.sql.open();
        //connection preberem iz sql2o-jevega connectiona. Lahko bi brez, lahko bi imeli tud connection pool.. naslednji sprint mrbit..
        context = DSL.using(con.getJdbcConnection(), SQLDialect.POSTGRES_9_5);
    }
    @Override
    public void close() throws Exception {
        con.close();
    }
}