package ps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import static generated.tables.UporabnikEntiteta.UPORABNIK;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import spark.route.RouteOverview;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static spark.Spark.*;


/**
 * Razred za vso globalno konfiguracijo streznika.
 */
public class Server {
	/** Logger tega razreda */
	private static final Logger log = LoggerFactory.getLogger(Server.class);
	
	/** Lokacija konfiguracijske datoteke znotraj .jar.
	 *	V projektu je shranjena v /src/main/resources */
	private static final String CONFIG_PATH = "ps-spark.json";
	
    /* debug uporabnik */
	public static generated.tables.pojos.UporabnikEntiteta debugUporabnik;
    
    public static Sql2o sql;
    public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
    public static final Konfiguracija konfiguracija;

	static {
		/* pridobitev globalne konfiguracije */
		Konfiguracija pridobljenaKonfiguracija;
		try {
			pridobljenaKonfiguracija = gson.fromJson(new String(Files.readAllBytes(Paths.get(CONFIG_PATH)), StandardCharsets.UTF_8), Konfiguracija.class);
		} catch (JsonSyntaxException | IOException e) {
			pridobljenaKonfiguracija = null;
			log.error(String.format("Ni bilo mogoce pridobiti konfiguracije: %s, lokacija iskanja: %s",
					e.toString(),
					Paths.get(CONFIG_PATH).toAbsolutePath()
					));
            System.exit(1);
		}
		if(pridobljenaKonfiguracija == null) {
			log.error(String.format("Pridobljena konfiguracija je neveljavna!"));
			System.exit(1);
		}
		konfiguracija = pridobljenaKonfiguracija;
		/* inicializacija ter uporaba rendererja za HTML */
		
	}
    
	/**
	 * Inicializira streznik (povezave do baze, branje globalnih parametrov ipd.).
	 */

	static String indexhtml = "index.html not found";
    private final static String DEBUG_USER_COOKIE = "1234";

    public static void inicializacije() {
    	// definiramo port, na katerem streznik poslusa
		port(konfiguracija.webServerPort);

		// staticne datoteke

		if(konfiguracija.angularDist != null){
            log.info("Konfiguriran za angular...");
			staticFiles.externalLocation(konfiguracija.angularDist);
			staticFiles.expireTime(1);

			try {
				indexhtml =  Files.readAllLines(Paths.get(konfiguracija.angularDist, "index.html")).stream().reduce("", (a,b)->a + "\n" + b);
			} catch (IOException e) {
				e.printStackTrace();
			}

			notFound((req, res) -> {
				res.status(200);
				return indexhtml;
			});
		} else {
			log.warn("Angular projek se ne poganja is Spark streznika. Dodaj pot do dist direktorija v konfiguracijsko polje angularDist, ce to vseeno zelis.");
		}

        //dostop do routov na http://localhost:webServerPort/debug/routeoverview/
        RouteOverview.enableRouteOverview();


		after((request, response) -> {
			//prepreci shranjevanje vsebin (cache) v brskalniku
			response.header("Cache-Control", "no-cache");
			response.header("Pragma", "no-cache");
			response.header("Expires", "0");
		});


        try {
            // nalozi driver za postgres - namrec ko se zahteva, da se pridobi
						// razred za podani niz, se hkrati inicializira njegov staticni del
            Class.forName("org.postgresql.Driver");

            sql = new Sql2o("jdbc:postgresql://"+konfiguracija.databaseHost+":"+konfiguracija.databasePort+"/"+konfiguracija.database, konfiguracija.databaseUser, konfiguracija.databasePassword);
            boolean cannotConnect = true;
            while(cannotConnect) {
                try {
                    Connection con = sql.open();
                    con.close();
                    cannotConnect = false;
                }
                catch(RuntimeException e) {
                    log.warn(String.format("Dobil [%s] - poskusam ponovno...", e.toString()));
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException sleepE) {
                        log.error(String.format("Napaka v cakanju: %s", sleepE.toString()));
                        System.exit(1);
                    }
                }
            }

        } catch (ClassNotFoundException e) {
            log.error(String.format("Ni bilo mogoce pridobiti JDBC driverja za bazo: %s", e.toString()));
            System.exit(1);
        }
        log.info("Uspesno pridobil povezavo do SQL baze, nadaljujem...");

        
		if(konfiguracija.debug) {
            
			log.info("Inicializiram streznik v debug nacinu!");
			try (JooqContext jooq = new JooqContext()) {
                debugUporabnik = jooq.context.selectFrom(UPORABNIK)
                        .where(UPORABNIK.ID.eq(konfiguracija.debugOid))
                        .fetchOneInto(generated.tables.pojos.UporabnikEntiteta.class);
                log.info(String.format("Debug uporabnik:%s", debugUporabnik.toString()));
            } catch (Exception e) {
                throw new RuntimeException();
            }
            
			// zahtevaj zapiranje starega procesa
			try {
				URL url = new URL("http://127.0.0.1:"+konfiguracija.webServerPort+"/close");
				URLConnection con = url.openConnection();
				HttpURLConnection http = (HttpURLConnection)con;
				http.setRequestMethod("GET");
				http.setDoOutput(true);
				http.connect();
				http.getResponseCode();
				http.disconnect();
			} catch (Exception e){
				log.warn(String.format("Ni bilo mogoce zapreti prejsnje instance streznika: %s", e.toString()));
			}
			// za debuganje - registriramo handler, ki zaustavi to instanco, ce
			// zahteva za /close iz localhosta
			get("close",(req,res)->{
				if(req.ip().equals("127.0.0.1")){
					System.exit(0);
				}
				return null;
			});
			//zazeni firefox ce je URL zahtevka na drugem parametru
			if(konfiguracija.debugBrowser != null && konfiguracija.debugURL != null) {
				new Thread(() -> {
					try {
						Thread.sleep(250);
					} catch (InterruptedException ignored) {}

					try {
						String fullURL = "localhost:"+konfiguracija.webServerPort+konfiguracija.debugURL;
						if(konfiguracija.debugBrowser.equals("firefox")){
							Process process = new ProcessBuilder("/usr/bin/firefox", fullURL).start();
						} else if(konfiguracija.debugBrowser.equals("chromium")) {
							Process process = new ProcessBuilder("/usr/bin/chromium-browser", fullURL).start();
						}
					} catch (IOException e) {
						log.error(String.format("Ni bilo mogoce zagnati brskalnika: %s", e.toString()));
					}
				}).run();
			}
		}
        
		Sifranti.vzpostavitev();
    }
}


