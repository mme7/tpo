package ps;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Staticna instanca mailerja. Glede na:
 * https://crunchify.com/java-mailapi-example-send-an-email-via-gmail-smtp/
 * https://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
 *
 * Za posiljanje aktivacije/pozabljenega gesla.
 */
public class Mail {

    /**
     * Sender mail address, ki ga fejkamo.
     */
    public static final String FROM_ADDRESS = "info@ps.si";

    private static final Logger log = LoggerFactory.getLogger(Mail.class);

    public static void posljiEmail(String naslovnik, String naslov, String vsebina) {
        Properties mailServerProperties;
        Session getMailSession;
        MimeMessage generateMailMessage;
        try {
            log.info("setup Mail Server Properties..");
            mailServerProperties = System.getProperties();
            mailServerProperties.put("mail.smtp.debug", "true");
            mailServerProperties.put("mail.smtp.port", "587");
            mailServerProperties.put("mail.smtp.auth", "true");
            mailServerProperties.put("mail.smtp.starttls.enable", "true");
            System.out.println("Mail Server Properties have been setup successfully..");

            log.info("get Mail Session..");
            getMailSession = Session.getDefaultInstance(mailServerProperties, null);
            generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.setFrom(new InternetAddress(FROM_ADDRESS));
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(naslovnik));
            generateMailMessage.setSubject(naslov, "UTF-8");
            generateMailMessage.setContent(vsebina, "text/html; charset=UTF-8");
            
            log.info("Mail Session has been created successfully..");

            log.info("Get Session and Send mail");
            Transport transport = getMailSession.getTransport("smtp");

            // Enter your correct gmail UserID and Password
            // if you have 2FA enabled then provide App Specific Password
            transport.connect("smtp.gmail.com", Server.konfiguracija.mail, Server.konfiguracija.mailPassword);
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e.toString());
        }
    }
}
