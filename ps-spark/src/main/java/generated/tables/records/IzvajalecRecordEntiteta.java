/*
 * This file is generated by jOOQ.
*/
package generated.tables.records;


import generated.tables.IzvajalecEntiteta;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class IzvajalecRecordEntiteta extends UpdatableRecordImpl<IzvajalecRecordEntiteta> implements Record5<Integer, String, String, String, Integer> {

    private static final long serialVersionUID = -126976339;

    /**
     * Setter for <code>public.izvajalec.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.izvajalec.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.izvajalec.naziv</code>.
     */
    public void setNaziv(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.izvajalec.naziv</code>.
     */
    public String getNaziv() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.izvajalec.ulica</code>.
     */
    public void setUlica(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.izvajalec.ulica</code>.
     */
    public String getUlica() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.izvajalec.hisna</code>.
     */
    public void setHisna(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.izvajalec.hisna</code>.
     */
    public String getHisna() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.izvajalec.postna</code>.
     */
    public void setPostna(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>public.izvajalec.postna</code>.
     */
    public Integer getPostna() {
        return (Integer) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, String, Integer> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, String, Integer> valuesRow() {
        return (Row5) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return IzvajalecEntiteta.IZVAJALEC.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return IzvajalecEntiteta.IZVAJALEC.NAZIV;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return IzvajalecEntiteta.IZVAJALEC.ULICA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return IzvajalecEntiteta.IZVAJALEC.HISNA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field5() {
        return IzvajalecEntiteta.IZVAJALEC.POSTNA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getNaziv();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getUlica();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getHisna();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value5() {
        return getPostna();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IzvajalecRecordEntiteta value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IzvajalecRecordEntiteta value2(String value) {
        setNaziv(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IzvajalecRecordEntiteta value3(String value) {
        setUlica(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IzvajalecRecordEntiteta value4(String value) {
        setHisna(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IzvajalecRecordEntiteta value5(Integer value) {
        setPostna(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IzvajalecRecordEntiteta values(Integer value1, String value2, String value3, String value4, Integer value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached IzvajalecRecordEntiteta
     */
    public IzvajalecRecordEntiteta() {
        super(IzvajalecEntiteta.IZVAJALEC);
    }

    /**
     * Create a detached, initialised IzvajalecRecordEntiteta
     */
    public IzvajalecRecordEntiteta(Integer id, String naziv, String ulica, String hisna, Integer postna) {
        super(IzvajalecEntiteta.IZVAJALEC);

        set(0, id);
        set(1, naziv);
        set(2, ulica);
        set(3, hisna);
        set(4, postna);
    }
}
