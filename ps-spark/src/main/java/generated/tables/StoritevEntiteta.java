/*
 * This file is generated by jOOQ.
*/
package generated.tables;


import generated.Keys;
import generated.PublicEntiteta;
import generated.tables.records.StoritevRecordEntiteta;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class StoritevEntiteta extends TableImpl<StoritevRecordEntiteta> {

    private static final long serialVersionUID = 1539322581;

    /**
     * The reference instance of <code>public.storitev</code>
     */
    public static final StoritevEntiteta STORITEV = new StoritevEntiteta();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<StoritevRecordEntiteta> getRecordType() {
        return StoritevRecordEntiteta.class;
    }

    /**
     * The column <code>public.storitev.id</code>.
     */
    public final TableField<StoritevRecordEntiteta, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('storitev_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.storitev.tipstoritve</code>.
     */
    public final TableField<StoritevRecordEntiteta, Integer> TIPSTORITVE = createField("tipstoritve", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>public.storitev.opis</code>.
     */
    public final TableField<StoritevRecordEntiteta, String> OPIS = createField("opis", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * Create a <code>public.storitev</code> table reference
     */
    public StoritevEntiteta() {
        this("storitev", null);
    }

    /**
     * Create an aliased <code>public.storitev</code> table reference
     */
    public StoritevEntiteta(String alias) {
        this(alias, STORITEV);
    }

    private StoritevEntiteta(String alias, Table<StoritevRecordEntiteta> aliased) {
        this(alias, aliased, null);
    }

    private StoritevEntiteta(String alias, Table<StoritevRecordEntiteta> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return PublicEntiteta.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<StoritevRecordEntiteta, Integer> getIdentity() {
        return Keys.IDENTITY_STORITEV;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<StoritevRecordEntiteta> getPrimaryKey() {
        return Keys.STORITEV_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<StoritevRecordEntiteta>> getKeys() {
        return Arrays.<UniqueKey<StoritevRecordEntiteta>>asList(Keys.STORITEV_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoritevEntiteta as(String alias) {
        return new StoritevEntiteta(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public StoritevEntiteta rename(String name) {
        return new StoritevEntiteta(name, null);
    }
}
