/*
 * This file is generated by jOOQ.
*/
package generated.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class IzvajalecEntiteta implements Serializable {

    private static final long serialVersionUID = 468317838;

    private Integer id;
    private String  naziv;
    private String  ulica;
    private String  hisna;
    private Integer postna;

    public IzvajalecEntiteta() {}

    public IzvajalecEntiteta(IzvajalecEntiteta value) {
        this.id = value.id;
        this.naziv = value.naziv;
        this.ulica = value.ulica;
        this.hisna = value.hisna;
        this.postna = value.postna;
    }

    public IzvajalecEntiteta(
        Integer id,
        String  naziv,
        String  ulica,
        String  hisna,
        Integer postna
    ) {
        this.id = id;
        this.naziv = naziv;
        this.ulica = ulica;
        this.hisna = hisna;
        this.postna = postna;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return this.naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getUlica() {
        return this.ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getHisna() {
        return this.hisna;
    }

    public void setHisna(String hisna) {
        this.hisna = hisna;
    }

    public Integer getPostna() {
        return this.postna;
    }

    public void setPostna(Integer postna) {
        this.postna = postna;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("IzvajalecEntiteta (");

        sb.append(id);
        sb.append(", ").append(naziv);
        sb.append(", ").append(ulica);
        sb.append(", ").append(hisna);
        sb.append(", ").append(postna);

        sb.append(")");
        return sb.toString();
    }
}
