/*
 * This file is generated by jOOQ.
*/
package generated.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class KrajEntiteta implements Serializable {

    private static final long serialVersionUID = -489563714;

    private Integer postna;
    private String  imekraja;

    public KrajEntiteta() {}

    public KrajEntiteta(KrajEntiteta value) {
        this.postna = value.postna;
        this.imekraja = value.imekraja;
    }

    public KrajEntiteta(
        Integer postna,
        String  imekraja
    ) {
        this.postna = postna;
        this.imekraja = imekraja;
    }

    public Integer getPostna() {
        return this.postna;
    }

    public void setPostna(Integer postna) {
        this.postna = postna;
    }

    public String getImekraja() {
        return this.imekraja;
    }

    public void setImekraja(String imekraja) {
        this.imekraja = imekraja;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("KrajEntiteta (");

        sb.append(postna);
        sb.append(", ").append(imekraja);

        sb.append(")");
        return sb.toString();
    }
}
