/*
 * This file is generated by jOOQ.
*/
package generated.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class OsebaEntiteta implements Serializable {

    private static final long serialVersionUID = 107297664;

    private Integer id;
    private String  ime;
    private String  priimek;
    private String  tel;
    private String  ulica;
    private String  hisnastevilka;
    private Integer postna;

    public OsebaEntiteta() {}

    public OsebaEntiteta(OsebaEntiteta value) {
        this.id = value.id;
        this.ime = value.ime;
        this.priimek = value.priimek;
        this.tel = value.tel;
        this.ulica = value.ulica;
        this.hisnastevilka = value.hisnastevilka;
        this.postna = value.postna;
    }

    public OsebaEntiteta(
        Integer id,
        String  ime,
        String  priimek,
        String  tel,
        String  ulica,
        String  hisnastevilka,
        Integer postna
    ) {
        this.id = id;
        this.ime = ime;
        this.priimek = priimek;
        this.tel = tel;
        this.ulica = ulica;
        this.hisnastevilka = hisnastevilka;
        this.postna = postna;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return this.ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return this.priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUlica() {
        return this.ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getHisnastevilka() {
        return this.hisnastevilka;
    }

    public void setHisnastevilka(String hisnastevilka) {
        this.hisnastevilka = hisnastevilka;
    }

    public Integer getPostna() {
        return this.postna;
    }

    public void setPostna(Integer postna) {
        this.postna = postna;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("OsebaEntiteta (");

        sb.append(id);
        sb.append(", ").append(ime);
        sb.append(", ").append(priimek);
        sb.append(", ").append(tel);
        sb.append(", ").append(ulica);
        sb.append(", ").append(hisnastevilka);
        sb.append(", ").append(postna);

        sb.append(")");
        return sb.toString();
    }
}
