SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
DROP DATABASE IF EXISTS tpodb;
CREATE DATABASE tpodb WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
ALTER DATABASE tpodb OWNER TO postgres;
\connect tpodb
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;
CREATE TABLE sorodstvenorazmerje (
    id integer NOT NULL,
    opis text
);
ALTER TABLE sorodstvenorazmerje OWNER TO postgres;
CREATE SEQUENCE "SorodstvenoRazmerje _id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "SorodstvenoRazmerje _id_seq" OWNER TO postgres;
ALTER SEQUENCE "SorodstvenoRazmerje _id_seq" OWNED BY sorodstvenorazmerje.id;
CREATE TABLE delovninalog (
    id integer NOT NULL,
    vrstaobiska integer NOT NULL,
    izdajatelj integer NOT NULL,
    datumizdaje timestamp without time zone NOT NULL
);
ALTER TABLE delovninalog OWNER TO postgres;
CREATE SEQUENCE delovninalog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE delovninalog_id_seq OWNER TO postgres;
ALTER SEQUENCE delovninalog_id_seq OWNED BY delovninalog.id;
CREATE TABLE izvajalec (
    id integer NOT NULL,
    naziv text NOT NULL,
    ulica text NOT NULL,
    hisna text,
    postna integer NOT NULL
);
ALTER TABLE izvajalec OWNER TO postgres;
CREATE SEQUENCE izvajalec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE izvajalec_id_seq OWNER TO postgres;
ALTER SEQUENCE izvajalec_id_seq OWNED BY izvajalec.id;
CREATE TABLE kraj (
    postna integer NOT NULL,
    imekraja text NOT NULL
);
ALTER TABLE kraj OWNER TO postgres;
CREATE TABLE material (
    id integer NOT NULL,
    ime text NOT NULL
);
ALTER TABLE material OWNER TO postgres;
CREATE SEQUENCE material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE material_id_seq OWNER TO postgres;
ALTER SEQUENCE material_id_seq OWNED BY material.id;
CREATE TABLE materialdn (
    material integer NOT NULL,
    dn integer NOT NULL,
    kolicina integer NOT NULL
);
ALTER TABLE materialdn OWNER TO postgres;
CREATE TABLE obisk (
    id integer NOT NULL,
    predvideni date NOT NULL,
    planirani date,
    dejanski date,
    sestra integer NOT NULL,
    nadomestnasestra integer,
    obvezen boolean NOT NULL,
    delovninalog integer NOT NULL,
    stevilkaobiska integer NOT NULL,
    podatki text
);
ALTER TABLE obisk OWNER TO postgres;
CREATE SEQUENCE obisk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE obisk_id_seq OWNER TO postgres;
ALTER SEQUENCE obisk_id_seq OWNED BY obisk.id;
CREATE TABLE okolis (
    id integer NOT NULL,
    ime text NOT NULL
);
ALTER TABLE okolis OWNER TO postgres;
CREATE SEQUENCE okolis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE okolis_id_seq OWNER TO postgres;
ALTER SEQUENCE okolis_id_seq OWNED BY okolis.id;
CREATE TABLE oseba (
    id integer NOT NULL,
    ime text NOT NULL,
    priimek text NOT NULL,
    tel text,
    ulica text,
    hisnastevilka text,
    postna integer
);
ALTER TABLE oseba OWNER TO postgres;
CREATE SEQUENCE oseba_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE oseba_id_seq OWNER TO postgres;
ALTER SEQUENCE oseba_id_seq OWNED BY oseba.id;
CREATE TABLE pacient (
    id integer NOT NULL,
    sifrazavarovanja integer NOT NULL,
    okolis integer NOT NULL,
    datumrojstva date NOT NULL,
    spol smallint NOT NULL,
    kontaktnaoseba integer,
    kontaktnaosebarazmerje smallint,
    skrbnik integer,
    skrbnikrazmerje integer
);
ALTER TABLE pacient OWNER TO postgres;
CREATE TABLE pacientdn (
    delovninalog integer NOT NULL,
    pacient integer NOT NULL,
    vloga integer NOT NULL
);
ALTER TABLE pacientdn OWNER TO postgres;
CREATE TABLE razmerje (
    id integer NOT NULL,
    ime text NOT NULL
);
ALTER TABLE razmerje OWNER TO postgres;
CREATE SEQUENCE razmerje_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE razmerje_id_seq OWNER TO postgres;
ALTER SEQUENCE razmerje_id_seq OWNED BY razmerje.id;
CREATE TABLE vloga (
    id integer NOT NULL,
    ime text NOT NULL
);
ALTER TABLE vloga OWNER TO postgres;
CREATE SEQUENCE seznamvlog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE seznamvlog_id_seq OWNER TO postgres;
ALTER SEQUENCE seznamvlog_id_seq OWNED BY vloga.id;
CREATE TABLE uporabnik (
    id integer NOT NULL,
    email text NOT NULL,
    geslo text NOT NULL,
    sol text NOT NULL,
    stanje integer NOT NULL,
    aktivacija text,
    datumregistracije timestamp without time zone,
    datumzadnjeprijave timestamp without time zone,
    vloga integer NOT NULL
);
ALTER TABLE uporabnik OWNER TO postgres;
CREATE TABLE vrstaobiska (
    id integer NOT NULL,
    ime text NOT NULL,
    kurativa boolean NOT NULL
);
ALTER TABLE vrstaobiska OWNER TO postgres;
CREATE SEQUENCE vrstaobiska_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE vrstaobiska_id_seq OWNER TO postgres;
ALTER SEQUENCE vrstaobiska_id_seq OWNED BY vrstaobiska.id;
CREATE TABLE zaposleni (
    id integer NOT NULL,
    sifrazaposlenega integer NOT NULL,
    izvajalec integer NOT NULL,
    okolis integer
);
ALTER TABLE zaposleni OWNER TO postgres;
CREATE TABLE zdravilo (
    id integer NOT NULL,
    naziv text NOT NULL
);
ALTER TABLE zdravilo OWNER TO postgres;
CREATE SEQUENCE zdravilo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE zdravilo_id_seq OWNER TO postgres;
ALTER SEQUENCE zdravilo_id_seq OWNED BY zdravilo.id;
CREATE TABLE zdravilodn (
    zdravilo integer NOT NULL,
    dn integer NOT NULL,
    kolicina integer
);
ALTER TABLE zdravilodn OWNER TO postgres;
ALTER TABLE ONLY delovninalog ALTER COLUMN id SET DEFAULT nextval('delovninalog_id_seq'::regclass);
ALTER TABLE ONLY izvajalec ALTER COLUMN id SET DEFAULT nextval('izvajalec_id_seq'::regclass);
ALTER TABLE ONLY material ALTER COLUMN id SET DEFAULT nextval('material_id_seq'::regclass);
ALTER TABLE ONLY obisk ALTER COLUMN id SET DEFAULT nextval('obisk_id_seq'::regclass);
ALTER TABLE ONLY okolis ALTER COLUMN id SET DEFAULT nextval('okolis_id_seq'::regclass);
ALTER TABLE ONLY oseba ALTER COLUMN id SET DEFAULT nextval('oseba_id_seq'::regclass);
ALTER TABLE ONLY razmerje ALTER COLUMN id SET DEFAULT nextval('razmerje_id_seq'::regclass);
ALTER TABLE ONLY sorodstvenorazmerje ALTER COLUMN id SET DEFAULT nextval('"SorodstvenoRazmerje _id_seq"'::regclass);
ALTER TABLE ONLY vloga ALTER COLUMN id SET DEFAULT nextval('seznamvlog_id_seq'::regclass);
ALTER TABLE ONLY vrstaobiska ALTER COLUMN id SET DEFAULT nextval('vrstaobiska_id_seq'::regclass);
ALTER TABLE ONLY zdravilo ALTER COLUMN id SET DEFAULT nextval('zdravilo_id_seq'::regclass);
SELECT pg_catalog.setval('"SorodstvenoRazmerje _id_seq"', 1, false);
SELECT pg_catalog.setval('delovninalog_id_seq', 1, true);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (5470, 'ZD LJUBLJANA - ŠIŠKA', 'DERČEVA ULICA', '5', 1000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (5150, 'ZD LJBLJANA - CENTER', 'METELKOVA ULICA', '9', 1000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (5600, 'ZD LJUBLJANA - VIČ - RUDNIK', 'ŠESTOVA ULICA', '10', 1000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (5300, 'ZD LJUBLJANA - MOSTE - POLJE', 'PRVOMAJSKA ULICA', '5', 1000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (5030, 'ZD LJUBLJANA - BEŽIGRAD', 'KRŽIČEVA ULICA', '10', 1000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (3401, 'ZDRAVSTVENI DOM KOPER', 'Dellavallejeva ulica', '3', 6000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (7883, 'ZD MARIBOR', 'Ulica talcev', '9', 2000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (2131, 'ZD CELJE', 'Gregorčičeva ulica', '5', 3000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (372, 'ZDRAVSTVENI DOM NOVO MESTO', 'KANDIJSKA CESTA', '4', 8000);
INSERT INTO izvajalec (id, naziv, ulica, hisna, postna) VALUES (353, 'ZD MURSKA SOBOTA', 'GRAJSKA ULICA', '24', 9000);
SELECT pg_catalog.setval('izvajalec_id_seq', 10, true);
INSERT INTO kraj (postna, imekraja) VALUES (1000, 'Ljubljana');
INSERT INTO kraj (postna, imekraja) VALUES (2000, 'Maribor');
INSERT INTO kraj (postna, imekraja) VALUES (3000, 'Celje');
INSERT INTO kraj (postna, imekraja) VALUES (6000, 'Koper');
INSERT INTO kraj (postna, imekraja) VALUES (8270, 'KRŠKO');
INSERT INTO kraj (postna, imekraja) VALUES (8216, 'MIRNA PEČ');
INSERT INTO kraj (postna, imekraja) VALUES (3252, 'ROGATEC');
INSERT INTO kraj (postna, imekraja) VALUES (4281, 'MOJSTRANA');
INSERT INTO kraj (postna, imekraja) VALUES (3272, 'RIMSKE TOPLICE');
INSERT INTO kraj (postna, imekraja) VALUES (8101, 'NOVO MESTO');
INSERT INTO kraj (postna, imekraja) VALUES (8263, 'CERKLJE OB KRKI');
INSERT INTO kraj (postna, imekraja) VALUES (8220, 'ŠMARJEŠKE TOPLICE');
INSERT INTO kraj (postna, imekraja) VALUES (3212, 'VOJNIK');
INSERT INTO kraj (postna, imekraja) VALUES (2270, 'ORMOŽ');
INSERT INTO kraj (postna, imekraja) VALUES (5211, 'KOJSKO');
INSERT INTO kraj (postna, imekraja) VALUES (2256, 'JURŠINCI');
INSERT INTO kraj (postna, imekraja) VALUES (4207, 'CERKLJE NA GORENJSKEM');
INSERT INTO kraj (postna, imekraja) VALUES (1215, 'MEDVODE');
INSERT INTO kraj (postna, imekraja) VALUES (4204, 'GOLNIK');
INSERT INTO kraj (postna, imekraja) VALUES (3331, 'NAZARJE');
INSERT INTO kraj (postna, imekraja) VALUES (2230, 'LENART V SLOVENSKIH GORIC');
INSERT INTO kraj (postna, imekraja) VALUES (2366, 'MUTA');
INSERT INTO kraj (postna, imekraja) VALUES (4267, 'SREDNJA VAS V BOHINJU');
INSERT INTO kraj (postna, imekraja) VALUES (1211, 'LJUBLJANA - ŠMARTNO');
INSERT INTO kraj (postna, imekraja) VALUES (5216, 'MOST NA SOČI');
INSERT INTO kraj (postna, imekraja) VALUES (3273, 'JURKLOŠTER');
INSERT INTO kraj (postna, imekraja) VALUES (1411, 'IZLAKE');
INSERT INTO kraj (postna, imekraja) VALUES (6230, 'POSTOJNA');
INSERT INTO kraj (postna, imekraja) VALUES (2315, 'ŠMARTNO NA POHORJU');
INSERT INTO kraj (postna, imekraja) VALUES (2282, 'CIRKULANE');
INSERT INTO kraj (postna, imekraja) VALUES (9220, 'LENDAVA');
INSERT INTO kraj (postna, imekraja) VALUES (6271, 'DEKANI');
INSERT INTO kraj (postna, imekraja) VALUES (2272, 'GORIŠNICA');
INSERT INTO kraj (postna, imekraja) VALUES (1410, 'ZAGORJE OB SAVI');
INSERT INTO kraj (postna, imekraja) VALUES (9262, 'ROGAŠOVCI');
INSERT INTO kraj (postna, imekraja) VALUES (1290, 'GROSUPLJE');
INSERT INTO kraj (postna, imekraja) VALUES (1296, 'ŠENTVID PRI STIČNI');
INSERT INTO kraj (postna, imekraja) VALUES (4263, 'BOHINJSKA BELA');
INSERT INTO kraj (postna, imekraja) VALUES (1294, 'VIŠNJA GORA');
INSERT INTO kraj (postna, imekraja) VALUES (2319, 'POLJČANE');
INSERT INTO kraj (postna, imekraja) VALUES (1241, 'KAMNIK');
INSERT INTO kraj (postna, imekraja) VALUES (2394, 'KOTLJE');
INSERT INTO kraj (postna, imekraja) VALUES (2222, 'JAKOBSKI DOL');
INSERT INTO kraj (postna, imekraja) VALUES (4290, 'TRŽIČ');
INSERT INTO kraj (postna, imekraja) VALUES (1291, 'ŠKOFLJICA');
INSERT INTO kraj (postna, imekraja) VALUES (3312, 'PREBOLD');
INSERT INTO kraj (postna, imekraja) VALUES (2354, 'BRESTERNICA');
INSERT INTO kraj (postna, imekraja) VALUES (9207, 'PROSENJAKOVCI - PARTOSFAL');
INSERT INTO kraj (postna, imekraja) VALUES (1430, 'HRASTNIK');
INSERT INTO kraj (postna, imekraja) VALUES (1210, 'LJUBLJANA - ŠENTVID');
INSERT INTO kraj (postna, imekraja) VALUES (6274, 'ŠMARJE');
INSERT INTO kraj (postna, imekraja) VALUES (4274, 'ŽIROVNICA');
INSERT INTO kraj (postna, imekraja) VALUES (5274, 'ČRNI VRH NAD IDRIJO');
INSERT INTO kraj (postna, imekraja) VALUES (6281, 'ŠKOFIJE');
INSERT INTO kraj (postna, imekraja) VALUES (3210, 'SLOVENSKE KONJICE');
INSERT INTO kraj (postna, imekraja) VALUES (2342, 'RUŠE');
INSERT INTO kraj (postna, imekraja) VALUES (2274, 'VELIKA NEDELJA');
INSERT INTO kraj (postna, imekraja) VALUES (2318, 'LAPORJE');
INSERT INTO kraj (postna, imekraja) VALUES (4205, 'PREDDVOR');
INSERT INTO kraj (postna, imekraja) VALUES (6215, 'DIVAČA');
INSERT INTO kraj (postna, imekraja) VALUES (3231, 'GROBELNO');
INSERT INTO kraj (postna, imekraja) VALUES (1101, 'LJUBLJANA');
INSERT INTO kraj (postna, imekraja) VALUES (4260, 'BLED');
INSERT INTO kraj (postna, imekraja) VALUES (1217, 'VODICE');
INSERT INTO kraj (postna, imekraja) VALUES (6276, 'POBEGI');
INSERT INTO kraj (postna, imekraja) VALUES (2212, 'ŠENTILJ V SLOVENSKIH GORI');
INSERT INTO kraj (postna, imekraja) VALUES (9253, 'APAČE');
INSERT INTO kraj (postna, imekraja) VALUES (3326, 'TOPOLŠICA');
INSERT INTO kraj (postna, imekraja) VALUES (2287, 'ŽETALE');
INSERT INTO kraj (postna, imekraja) VALUES (2250, 'PTUJ');
INSERT INTO kraj (postna, imekraja) VALUES (3333, 'LJUBNO OB SAVINJI');
INSERT INTO kraj (postna, imekraja) VALUES (9101, 'MURSKA SOBOTA');
INSERT INTO kraj (postna, imekraja) VALUES (2393, 'ČRNA NA KOROŠKEM');
INSERT INTO kraj (postna, imekraja) VALUES (4280, 'KRANJSKA GORA');
INSERT INTO kraj (postna, imekraja) VALUES (8257, 'DOBOVA');
INSERT INTO kraj (postna, imekraja) VALUES (4248, 'LESCE');
INSERT INTO kraj (postna, imekraja) VALUES (3320, 'VELENJE');
INSERT INTO kraj (postna, imekraja) VALUES (1235, 'RADOMLJE');
INSERT INTO kraj (postna, imekraja) VALUES (8280, 'BRESTANICA');
INSERT INTO kraj (postna, imekraja) VALUES (1337, 'OSILNICA');
INSERT INTO kraj (postna, imekraja) VALUES (1354, 'HORJUL');
INSERT INTO kraj (postna, imekraja) VALUES (8275, 'ŠKOCJAN');
INSERT INTO kraj (postna, imekraja) VALUES (9225, 'VELIKA POLANA');
INSERT INTO kraj (postna, imekraja) VALUES (1351, 'BREZOVICA PRI LJUBLJANI');
INSERT INTO kraj (postna, imekraja) VALUES (3342, 'GORNJI GRAD');
INSERT INTO kraj (postna, imekraja) VALUES (3334, 'LUČE');
INSERT INTO kraj (postna, imekraja) VALUES (1240, 'KAMNIK');
INSERT INTO kraj (postna, imekraja) VALUES (8293, 'STUDENEC');
INSERT INTO kraj (postna, imekraja) VALUES (2351, 'KAMNICA');
INSERT INTO kraj (postna, imekraja) VALUES (6002, 'KOPER');
INSERT INTO kraj (postna, imekraja) VALUES (2370, 'DRAVOGRAD');
INSERT INTO kraj (postna, imekraja) VALUES (9251, 'TIŠINA');
INSERT INTO kraj (postna, imekraja) VALUES (8295, 'TRŽIŠČE');
INSERT INTO kraj (postna, imekraja) VALUES (1225, 'LUKOVICA');
INSERT INTO kraj (postna, imekraja) VALUES (4264, 'BOHINJSKA BISTRICA');
INSERT INTO kraj (postna, imekraja) VALUES (2364, 'RIBNICA NA POHORJU');
INSERT INTO kraj (postna, imekraja) VALUES (2285, 'ZGORNJI LESKOVEC');
INSERT INTO kraj (postna, imekraja) VALUES (2214, 'SLADKI VRH');
INSERT INTO kraj (postna, imekraja) VALUES (8360, 'ŽUŽEMBERK');
INSERT INTO kraj (postna, imekraja) VALUES (6240, 'KOZINA');
INSERT INTO kraj (postna, imekraja) VALUES (1370, 'LOGATEC');
INSERT INTO kraj (postna, imekraja) VALUES (9250, 'GORNJA RADGONA');
INSERT INTO kraj (postna, imekraja) VALUES (6333, 'SEČOVLJE - SICCIOLE');
INSERT INTO kraj (postna, imekraja) VALUES (8255, 'PIŠECE');
INSERT INTO kraj (postna, imekraja) VALUES (1231, 'LJUBLJANA - ČRNUČE');
INSERT INTO kraj (postna, imekraja) VALUES (2325, 'KIDRIČEVO');
INSERT INTO kraj (postna, imekraja) VALUES (8290, 'SEVNICA');
INSERT INTO kraj (postna, imekraja) VALUES (2221, 'JARENINA');
INSERT INTO kraj (postna, imekraja) VALUES (8274, 'RAKA');
INSERT INTO kraj (postna, imekraja) VALUES (2201, 'ZGORNJA KUNGOTA');
INSERT INTO kraj (postna, imekraja) VALUES (1001, 'LJUBLJANA');
INSERT INTO kraj (postna, imekraja) VALUES (5220, 'TOLMIN');
INSERT INTO kraj (postna, imekraja) VALUES (1380, 'CERKNICA');
INSERT INTO kraj (postna, imekraja) VALUES (1303, 'ZAGRADEC');
INSERT INTO kraj (postna, imekraja) VALUES (3215, 'LOČE');
INSERT INTO kraj (postna, imekraja) VALUES (2382, 'MISLINJA');
INSERT INTO kraj (postna, imekraja) VALUES (3230, 'ŠENTJUR');
INSERT INTO kraj (postna, imekraja) VALUES (2253, 'DESTRNIK');
INSERT INTO kraj (postna, imekraja) VALUES (2231, 'PERNICA');
INSERT INTO kraj (postna, imekraja) VALUES (5290, 'ŠEMPETER PRI GORICI');
INSERT INTO kraj (postna, imekraja) VALUES (2321, 'MAKOLE');
INSERT INTO kraj (postna, imekraja) VALUES (4209, 'ŽABNICA');
INSERT INTO kraj (postna, imekraja) VALUES (8340, 'ČRNOMELJ');
INSERT INTO kraj (postna, imekraja) VALUES (3327, 'ŠMARTNO OB PAKI');
INSERT INTO kraj (postna, imekraja) VALUES (5212, 'DOBROVO V BRDIH');
INSERT INTO kraj (postna, imekraja) VALUES (5210, 'DESKLE');
INSERT INTO kraj (postna, imekraja) VALUES (5292, 'RENČE');
INSERT INTO kraj (postna, imekraja) VALUES (5000, 'NOVA GORICA');
INSERT INTO kraj (postna, imekraja) VALUES (1431, 'DOL PRI HRASTNIKU');
INSERT INTO kraj (postna, imekraja) VALUES (1315, 'VELIKE LAŠČE');
INSERT INTO kraj (postna, imekraja) VALUES (8294, 'BOŠTANJ');
INSERT INTO kraj (postna, imekraja) VALUES (3211, 'ŠKOFJA VAS');
INSERT INTO kraj (postna, imekraja) VALUES (5281, 'SPODNJA IDRIJA');
INSERT INTO kraj (postna, imekraja) VALUES (3240, 'ŠMARJE PRI JELŠAH');
INSERT INTO kraj (postna, imekraja) VALUES (8311, 'KOSTANJEVICA NA KRKI');
INSERT INTO kraj (postna, imekraja) VALUES (1216, 'SMLEDNIK');
INSERT INTO kraj (postna, imekraja) VALUES (1270, 'LITIJA');
INSERT INTO kraj (postna, imekraja) VALUES (1385, 'NOVA VAS');
INSERT INTO kraj (postna, imekraja) VALUES (1420, 'TRBOVLJE');
INSERT INTO kraj (postna, imekraja) VALUES (9103, 'MURSKA SOBOTA RAKIČAN');
INSERT INTO kraj (postna, imekraja) VALUES (3214, 'ZREČE');
INSERT INTO kraj (postna, imekraja) VALUES (4208, 'ŠENČUR');
INSERT INTO kraj (postna, imekraja) VALUES (9264, 'GRAD');
INSERT INTO kraj (postna, imekraja) VALUES (2317, 'OPLOTNICA');
INSERT INTO kraj (postna, imekraja) VALUES (6310, 'IZOLA - ISOLA');
INSERT INTO kraj (postna, imekraja) VALUES (4201, 'ZGORNJA BESNICA');
INSERT INTO kraj (postna, imekraja) VALUES (6223, 'KOMEN');
INSERT INTO kraj (postna, imekraja) VALUES (8259, 'BIZELJSKO');
INSERT INTO kraj (postna, imekraja) VALUES (2361, 'OŽBALT');
INSERT INTO kraj (postna, imekraja) VALUES (1358, 'LOG PRI BREZOVICI');
INSERT INTO kraj (postna, imekraja) VALUES (5291, 'MIREN');
INSERT INTO kraj (postna, imekraja) VALUES (6280, 'ANKARAN');
INSERT INTO kraj (postna, imekraja) VALUES (3205, 'VITANJE');
INSERT INTO kraj (postna, imekraja) VALUES (1312, 'VIDEM - DOBREPOLJE');
INSERT INTO kraj (postna, imekraja) VALUES (1352, 'PRESERJE');
INSERT INTO kraj (postna, imekraja) VALUES (5250, 'SOLKAN');
INSERT INTO kraj (postna, imekraja) VALUES (1261, 'LJUBLJANA - DOBRUNJE');
INSERT INTO kraj (postna, imekraja) VALUES (2313, 'FRAM');
INSERT INTO kraj (postna, imekraja) VALUES (4206, 'ZGORNJE JEZERSKO');
INSERT INTO kraj (postna, imekraja) VALUES (9201, 'PUCONCI');
INSERT INTO kraj (postna, imekraja) VALUES (1355, 'POLHOV GRADEC');
INSERT INTO kraj (postna, imekraja) VALUES (3254, 'PODČETRTEK');
INSERT INTO kraj (postna, imekraja) VALUES (8310, 'ŠENTJERNEJ');
INSERT INTO kraj (postna, imekraja) VALUES (3325, 'ŠOŠTANJ');
INSERT INTO kraj (postna, imekraja) VALUES (2284, 'VIDEM PRI PTUJU');
INSERT INTO kraj (postna, imekraja) VALUES (9244, 'SVETI JURIJ OB ŠČAVNICI');
INSERT INTO kraj (postna, imekraja) VALUES (2103, 'MARIBOR');
INSERT INTO kraj (postna, imekraja) VALUES (3250, 'ROGAŠKA SLATINA');
INSERT INTO kraj (postna, imekraja) VALUES (9246, 'RAZKRIŽJE');
INSERT INTO kraj (postna, imekraja) VALUES (9221, 'MARTJANCI');
INSERT INTO kraj (postna, imekraja) VALUES (2311, 'HOČE');
INSERT INTO kraj (postna, imekraja) VALUES (8230, 'MOKRONOG');
INSERT INTO kraj (postna, imekraja) VALUES (4220, 'ŠKOFJA LOKA');
INSERT INTO kraj (postna, imekraja) VALUES (3310, 'ŽALEC');
INSERT INTO kraj (postna, imekraja) VALUES (5253, 'ČEPOVAN');
INSERT INTO kraj (postna, imekraja) VALUES (3270, 'LAŠKO');
INSERT INTO kraj (postna, imekraja) VALUES (2312, 'OREHOVA VAS');
INSERT INTO kraj (postna, imekraja) VALUES (2390, 'RAVNE NA KOROŠKEM');
INSERT INTO kraj (postna, imekraja) VALUES (5294, 'DORNBERK');
INSERT INTO kraj (postna, imekraja) VALUES (2363, 'PODVELKA');
INSERT INTO kraj (postna, imekraja) VALUES (8333, 'SEMIČ');
INSERT INTO kraj (postna, imekraja) VALUES (2252, 'DORNAVA');
INSERT INTO kraj (postna, imekraja) VALUES (4224, 'GORENJA VAS');
INSERT INTO kraj (postna, imekraja) VALUES (3314, 'BRASLOVČE');
INSERT INTO kraj (postna, imekraja) VALUES (9263, 'KUZMA');
INSERT INTO kraj (postna, imekraja) VALUES (8281, 'SENOVO');
INSERT INTO kraj (postna, imekraja) VALUES (3301, 'PETROVČE');
INSERT INTO kraj (postna, imekraja) VALUES (1412, 'KISOVEC');
INSERT INTO kraj (postna, imekraja) VALUES (9243, 'MALA NEDELJA');
INSERT INTO kraj (postna, imekraja) VALUES (3241, 'PODPLAT');
INSERT INTO kraj (postna, imekraja) VALUES (2286, 'PODLEHNIK');
INSERT INTO kraj (postna, imekraja) VALUES (1293, 'ŠMARJE - SAP');
INSERT INTO kraj (postna, imekraja) VALUES (2206, 'MARJETA NA DRAV.POLJU');
INSERT INTO kraj (postna, imekraja) VALUES (2314, 'ZGORNJA POLSKAVA');
INSERT INTO kraj (postna, imekraja) VALUES (9000, 'MURSKA SOBOTA');
INSERT INTO kraj (postna, imekraja) VALUES (9231, 'BELTINCI');
INSERT INTO kraj (postna, imekraja) VALUES (2344, 'LOVRENC NA POHORJU');
INSERT INTO kraj (postna, imekraja) VALUES (4270, 'JESENICE');
INSERT INTO kraj (postna, imekraja) VALUES (1262, 'DOL PRI LJUBLJANI');
INSERT INTO kraj (postna, imekraja) VALUES (5271, 'VIPAVA');
INSERT INTO kraj (postna, imekraja) VALUES (5222, 'KOBARID');
INSERT INTO kraj (postna, imekraja) VALUES (2352, 'SELNICA OB DRAVI');
INSERT INTO kraj (postna, imekraja) VALUES (2380, 'SLOVENJ GRADEC');
INSERT INTO kraj (postna, imekraja) VALUES (8342, 'STARI TRG OB KOLPI');
INSERT INTO kraj (postna, imekraja) VALUES (9261, 'CANKOVA');
INSERT INTO kraj (postna, imekraja) VALUES (2310, 'SLOVENSKA BISTRICA');
INSERT INTO kraj (postna, imekraja) VALUES (5296, 'KOSTANJEVICA NA KRASU');
INSERT INTO kraj (postna, imekraja) VALUES (8210, 'TREBNJE');
INSERT INTO kraj (postna, imekraja) VALUES (1356, 'DOBROVA');
INSERT INTO kraj (postna, imekraja) VALUES (2241, 'SPODNJI DUPLEK');
INSERT INTO kraj (postna, imekraja) VALUES (6330, 'PIRAN - PIRANO');
INSERT INTO kraj (postna, imekraja) VALUES (2255, 'VITOMARCI');
INSERT INTO kraj (postna, imekraja) VALUES (9240, 'LJUTOMER');
INSERT INTO kraj (postna, imekraja) VALUES (5270, 'AJDOVŠČINA');
INSERT INTO kraj (postna, imekraja) VALUES (6253, 'KNEŽAK');
INSERT INTO kraj (postna, imekraja) VALUES (6258, 'PRESTRANEK');
INSERT INTO kraj (postna, imekraja) VALUES (9232, 'ČRENŠOVCI');
INSERT INTO kraj (postna, imekraja) VALUES (8253, 'ARTIČE');
INSERT INTO kraj (postna, imekraja) VALUES (6244, 'PODGRAD');
INSERT INTO kraj (postna, imekraja) VALUES (6221, 'DUTOVLJE');
INSERT INTO kraj (postna, imekraja) VALUES (6257, 'PIVKA');
INSERT INTO kraj (postna, imekraja) VALUES (2367, 'VUZENICA');
INSERT INTO kraj (postna, imekraja) VALUES (8272, 'ZDOLE');
INSERT INTO kraj (postna, imekraja) VALUES (2324, 'LOVRENC NA DRAVSKEM POLJU');
INSERT INTO kraj (postna, imekraja) VALUES (1317, 'SODRAŽICA');
INSERT INTO kraj (postna, imekraja) VALUES (2288, 'HAJDINA');
INSERT INTO kraj (postna, imekraja) VALUES (4000, 'KRANJ');
INSERT INTO kraj (postna, imekraja) VALUES (1292, 'IG');
INSERT INTO kraj (postna, imekraja) VALUES (1318, 'LOŠKI POTOK');
INSERT INTO kraj (postna, imekraja) VALUES (2204, 'MIKLAVŽ NA DRAV.POLJU');
INSERT INTO kraj (postna, imekraja) VALUES (2236, 'CERKVENJAK');
INSERT INTO kraj (postna, imekraja) VALUES (5230, 'BOVEC');
INSERT INTO kraj (postna, imekraja) VALUES (4246, 'KAMNA GORICA');
INSERT INTO kraj (postna, imekraja) VALUES (1234, 'MENGEŠ');
INSERT INTO kraj (postna, imekraja) VALUES (8250, 'BREŽICE');
INSERT INTO kraj (postna, imekraja) VALUES (2360, 'RADLJE OB DRAVI');
INSERT INTO kraj (postna, imekraja) VALUES (8222, 'OTOČEC');
INSERT INTO kraj (postna, imekraja) VALUES (8232, 'ŠENTRUPERT');
INSERT INTO kraj (postna, imekraja) VALUES (1230, 'DOMŽALE');
INSERT INTO kraj (postna, imekraja) VALUES (8251, 'ČATEŽ OB SAVI');
INSERT INTO kraj (postna, imekraja) VALUES (1432, 'ZIDANI MOST');
INSERT INTO kraj (postna, imekraja) VALUES (1251, 'MORAVČE');
INSERT INTO kraj (postna, imekraja) VALUES (1330, 'KOČEVJE');
INSERT INTO kraj (postna, imekraja) VALUES (4226, 'ŽIRI');
INSERT INTO kraj (postna, imekraja) VALUES (4275, 'BEGUNJE NA GORENJSKEM');
INSERT INTO kraj (postna, imekraja) VALUES (8233, 'MIRNA');
INSERT INTO kraj (postna, imekraja) VALUES (5280, 'IDRIJA');
INSERT INTO kraj (postna, imekraja) VALUES (2392, 'MEŽICA');
INSERT INTO kraj (postna, imekraja) VALUES (1386, 'STARI TRG PRI LOŽU');
INSERT INTO kraj (postna, imekraja) VALUES (8362, 'HINJE');
INSERT INTO kraj (postna, imekraja) VALUES (1219, 'LAZE V TUHINJU');
INSERT INTO kraj (postna, imekraja) VALUES (3330, 'MOZIRJE');
INSERT INTO kraj (postna, imekraja) VALUES (3220, 'ŠTORE');
INSERT INTO kraj (postna, imekraja) VALUES (9203, 'PETROVCI');
INSERT INTO kraj (postna, imekraja) VALUES (9242, 'KRIŽEVCI PRI LJUTOMERU');
INSERT INTO kraj (postna, imekraja) VALUES (4245, 'KROPA');
INSERT INTO kraj (postna, imekraja) VALUES (9223, 'DOBROVNIK - DOBRONAK');
INSERT INTO kraj (postna, imekraja) VALUES (8330, 'METLIKA');
INSERT INTO kraj (postna, imekraja) VALUES (3204, 'DOBRNA');
INSERT INTO kraj (postna, imekraja) VALUES (4240, 'RADOVLJICA');
INSERT INTO kraj (postna, imekraja) VALUES (8344, 'VINICA');
INSERT INTO kraj (postna, imekraja) VALUES (2229, 'MALEČNIK');
INSERT INTO kraj (postna, imekraja) VALUES (2277, 'SREDIŠČE OB DRAVI');
INSERT INTO kraj (postna, imekraja) VALUES (1360, 'VRHNIKA');
INSERT INTO kraj (postna, imekraja) VALUES (3225, 'PLANINA PRI SEVNICI');
INSERT INTO kraj (postna, imekraja) VALUES (9224, 'TURNIŠČE');
INSERT INTO kraj (postna, imekraja) VALUES (2281, 'MARKOVCI');
INSERT INTO kraj (postna, imekraja) VALUES (8296, 'KRMELJ');
INSERT INTO kraj (postna, imekraja) VALUES (6320, 'PORTOROŽ - PORTOROSE');
INSERT INTO kraj (postna, imekraja) VALUES (3260, 'KOZJE');
INSERT INTO kraj (postna, imekraja) VALUES (2331, 'PRAGERSKO');
INSERT INTO kraj (postna, imekraja) VALUES (9241, 'VERŽEJ');
INSERT INTO kraj (postna, imekraja) VALUES (2326, 'CIRKOVCE');
INSERT INTO kraj (postna, imekraja) VALUES (5213, 'KANAL');
INSERT INTO kraj (postna, imekraja) VALUES (1281, 'KRESNICE');
INSERT INTO kraj (postna, imekraja) VALUES (3311, 'ŠEMPETER V SAVINJSKI DOLI');
INSERT INTO kraj (postna, imekraja) VALUES (8000, 'NOVO MESTO');
INSERT INTO kraj (postna, imekraja) VALUES (4228, 'ŽELEZNIKI');
INSERT INTO kraj (postna, imekraja) VALUES (8351, 'STRAŽA PRI NOVEM MESTU');
INSERT INTO kraj (postna, imekraja) VALUES (6250, 'ILIRSKA BISTRICA');
INSERT INTO kraj (postna, imekraja) VALUES (3256, 'BISTRICA OB SOTLI');
INSERT INTO kraj (postna, imekraja) VALUES (2341, 'LIMBUŠ');
INSERT INTO kraj (postna, imekraja) VALUES (3203, 'NOVA CERKEV');
INSERT INTO kraj (postna, imekraja) VALUES (1353, 'BOROVNICA');
INSERT INTO kraj (postna, imekraja) VALUES (1275, 'ŠMARTNO PRI LITIJI');
INSERT INTO kraj (postna, imekraja) VALUES (5243, 'PODBRDO');
INSERT INTO kraj (postna, imekraja) VALUES (2254, 'TRNOVSKA VAS');
INSERT INTO kraj (postna, imekraja) VALUES (2205, 'STARŠE');
INSERT INTO kraj (postna, imekraja) VALUES (2373, 'ŠENTJANŽ PRI DRAVOGRADU');
INSERT INTO kraj (postna, imekraja) VALUES (9226, 'MORAVSKE TOPLICE');
INSERT INTO kraj (postna, imekraja) VALUES (2235, 'SV.TROJICA V SL.GORICAH');
INSERT INTO kraj (postna, imekraja) VALUES (1357, 'NOTRANJE GORICE');
INSERT INTO kraj (postna, imekraja) VALUES (1236, 'TRZIN');
INSERT INTO kraj (postna, imekraja) VALUES (1218, 'KOMENDA');
INSERT INTO kraj (postna, imekraja) VALUES (2211, 'PESNICA PRI MARIBORU');
INSERT INTO kraj (postna, imekraja) VALUES (8273, 'LESKOVEC PRI KRŠKEM');
INSERT INTO kraj (postna, imekraja) VALUES (5295, 'BRANIK');
INSERT INTO kraj (postna, imekraja) VALUES (2242, 'ZGORNJA KORENA');
INSERT INTO kraj (postna, imekraja) VALUES (8350, 'DOLENJSKE TOPLICE');
INSERT INTO kraj (postna, imekraja) VALUES (6224, 'SENOŽEČE');
INSERT INTO kraj (postna, imekraja) VALUES (6210, 'SEŽANA');
INSERT INTO kraj (postna, imekraja) VALUES (1310, 'RIBNICA');
INSERT INTO kraj (postna, imekraja) VALUES (2391, 'PREVALJE');
INSERT INTO kraj (postna, imekraja) VALUES (5282, 'CERKNO');
INSERT INTO kraj (postna, imekraja) VALUES (4202, 'NAKLO');
INSERT INTO kraj (postna, imekraja) VALUES (2283, 'ZAVRČ');
INSERT INTO kraj (postna, imekraja) VALUES (3305, 'VRANSKO');
INSERT INTO kraj (postna, imekraja) VALUES (1260, 'LJUBLJANA - POLJE');
INSERT INTO kraj (postna, imekraja) VALUES (3313, 'POLZELA');
INSERT INTO kraj (postna, imekraja) VALUES (2322, 'MAJŠPERK');
INSERT INTO kraj (postna, imekraja) VALUES (2327, 'RAČE');
INSERT INTO kraj (postna, imekraja) VALUES (1434, 'LOKA PRI ZIDANEM MOSTU');
INSERT INTO kraj (postna, imekraja) VALUES (1433, 'RADEČE');
INSERT INTO kraj (postna, imekraja) VALUES (1295, 'IVANČNA GORICA');
INSERT INTO kraj (postna, imekraja) VALUES (9252, 'RADENCI');
INSERT INTO kraj (postna, imekraja) VALUES (4203, 'DUPLJE');
INSERT INTO kraj (postna, imekraja) VALUES (1338, 'KOČEVSKA REKA');
INSERT INTO material (id, ime) VALUES (1, 'Rdeča epruveta');
INSERT INTO material (id, ime) VALUES (2, 'Modra epruveta');
INSERT INTO material (id, ime) VALUES (3, 'Rumena epruveta');
INSERT INTO material (id, ime) VALUES (4, 'Zelena epruveta');
SELECT pg_catalog.setval('material_id_seq', 1, true);
SELECT pg_catalog.setval('obisk_id_seq', 1, true);
INSERT INTO okolis (id, ime) VALUES (2001, 'Maribor Vzhod');
INSERT INTO okolis (id, ime) VALUES (3001, 'Celje Zahod');
INSERT INTO okolis (id, ime) VALUES (6001, 'Koper Jug');
INSERT INTO okolis (id, ime) VALUES (1001, 'Ljubljana Šiška');
INSERT INTO okolis (id, ime) VALUES (1002, 'Ljubljana Center Sever');
INSERT INTO okolis (id, ime) VALUES (1003, 'Ljubljana Center Jug');
SELECT pg_catalog.setval('okolis_id_seq', 6, true);
INSERT INTO oseba (id, ime, priimek, tel, ulica, hisnastevilka, postna) VALUES (1, 'Administator', '', '111 111 111', 'DevOp', '1', 1000);
SELECT pg_catalog.setval('oseba_id_seq', 1, true);
INSERT INTO razmerje (id, ime) VALUES (4, 'drugo nesorodstveno');
INSERT INTO razmerje (id, ime) VALUES (3, 'drugo sorodstveno');
INSERT INTO razmerje (id, ime) VALUES (2, 'sin/hčer');
INSERT INTO razmerje (id, ime) VALUES (1, 'mati/oče');
SELECT pg_catalog.setval('razmerje_id_seq', 4, true);
SELECT pg_catalog.setval('seznamvlog_id_seq', 5, true);
INSERT INTO sorodstvenorazmerje (id, opis) VALUES (0, 'Mož/Žena');
INSERT INTO sorodstvenorazmerje (id, opis) VALUES (1, 'Brat/Sestra"');
INSERT INTO sorodstvenorazmerje (id, opis) VALUES (2, 'Hčer/Sin');
INSERT INTO sorodstvenorazmerje (id, opis) VALUES (3, 'Dedek/Babica');
INSERT INTO sorodstvenorazmerje (id, opis) VALUES (4, 'Partner');
INSERT INTO uporabnik (id, email, geslo, sol, stanje, aktivacija, datumregistracije, datumzadnjeprijave, vloga) VALUES (1, 'admin@email.com', '$2a$10$S8vx2.t58cVCkLhXeZoC6OLLiQGvLI1hE143fy/qhqAe/gUcbSP6i', '$2a$10$S8vx2.t58cVCkLhXeZoC6O', 0, '1', '2017-03-31 13:44:23.996', '2017-03-31 13:44:26.626', 1);
INSERT INTO vloga (id, ime) VALUES (0, 'Pacient');
INSERT INTO vloga (id, ime) VALUES (1, 'Administator');
INSERT INTO vloga (id, ime) VALUES (2, 'Zdravnik');
INSERT INTO vloga (id, ime) VALUES (3, 'Vodja patronažne službe');
INSERT INTO vloga (id, ime) VALUES (4, 'Medicinska sestra');
INSERT INTO vloga (id, ime) VALUES (5, 'Uslužbenec ZD');
INSERT INTO vrstaobiska (id, ime, kurativa) VALUES (1, 'Obisk nosečnice', false);
INSERT INTO vrstaobiska (id, ime, kurativa) VALUES (2, 'Obisk otročnice in novorojenčka', false);
INSERT INTO vrstaobiska (id, ime, kurativa) VALUES (3, 'Preventiva starostnika', false);
INSERT INTO vrstaobiska (id, ime, kurativa) VALUES (4, 'Aplikacija injekcij', true);
INSERT INTO vrstaobiska (id, ime, kurativa) VALUES (5, 'Odvzem krvi', true);
INSERT INTO vrstaobiska (id, ime, kurativa) VALUES (6, 'Kontrola zdravstvenega stanja', true);
SELECT pg_catalog.setval('vrstaobiska_id_seq', 5, true);
INSERT INTO zdravilo (id, naziv) VALUES (144760, 'ALKOHOL-KONZENTRAT 95% BRAUN 20 ML');
INSERT INTO zdravilo (id, naziv) VALUES (24163, 'ENDOBULIN');
INSERT INTO zdravilo (id, naziv) VALUES (88773, 'GRAVIBINON');
INSERT INTO zdravilo (id, naziv) VALUES (65269, 'HUMANI ANTIRABIČNI IMUNOGLOBULIN');
INSERT INTO zdravilo (id, naziv) VALUES (63207, 'CEPIVO PROTI TETANUSU');
SELECT pg_catalog.setval('zdravilo_id_seq', 5, true);
ALTER TABLE ONLY sorodstvenorazmerje
    ADD CONSTRAINT "SorodstvenoRazmerje _pkey" PRIMARY KEY (id);
ALTER TABLE ONLY delovninalog
    ADD CONSTRAINT delovninalog_pkey PRIMARY KEY (id);
ALTER TABLE ONLY izvajalec
    ADD CONSTRAINT izvajalec_pkey PRIMARY KEY (id);
ALTER TABLE ONLY kraj
    ADD CONSTRAINT kraj_pkey PRIMARY KEY (postna);
ALTER TABLE ONLY material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id);
ALTER TABLE ONLY materialdn
    ADD CONSTRAINT materialdn_dn_pk PRIMARY KEY (dn, material);
ALTER TABLE ONLY obisk
    ADD CONSTRAINT obisk_pkey PRIMARY KEY (id);
ALTER TABLE ONLY okolis
    ADD CONSTRAINT okolis_pkey PRIMARY KEY (id);
ALTER TABLE ONLY oseba
    ADD CONSTRAINT oseba_pkey PRIMARY KEY (id);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_pkey PRIMARY KEY (id);
ALTER TABLE ONLY pacientdn
    ADD CONSTRAINT pacientdn_pacient_pk PRIMARY KEY (delovninalog, pacient);
ALTER TABLE ONLY razmerje
    ADD CONSTRAINT razmerje_pkey PRIMARY KEY (id);
ALTER TABLE ONLY vloga
    ADD CONSTRAINT seznamvlog_pkey PRIMARY KEY (id);
ALTER TABLE ONLY uporabnik
    ADD CONSTRAINT uporabnik_pkey PRIMARY KEY (id);
ALTER TABLE ONLY vrstaobiska
    ADD CONSTRAINT vrstaobiska_pkey PRIMARY KEY (id);
ALTER TABLE ONLY zaposleni
    ADD CONSTRAINT zaposleni_pkey PRIMARY KEY (id);
ALTER TABLE ONLY zdravilo
    ADD CONSTRAINT zdravilo_pkey PRIMARY KEY (id);
ALTER TABLE ONLY zdravilodn
    ADD CONSTRAINT zdravilodn_dn_pk PRIMARY KEY (zdravilo, dn);
CREATE UNIQUE INDEX "SorodstvenoRazmerje _id_uindex" ON sorodstvenorazmerje USING btree (id);
CREATE UNIQUE INDEX delovninalog_id_uindex ON delovninalog USING btree (id);
CREATE UNIQUE INDEX izvajalec_id_uindex ON izvajalec USING btree (id);
CREATE UNIQUE INDEX kraj_postna_uindex ON kraj USING btree (postna);
CREATE UNIQUE INDEX material_id_uindex ON material USING btree (id);
CREATE UNIQUE INDEX obisk_id_uindex ON obisk USING btree (id);
CREATE UNIQUE INDEX okolis_id_uindex ON okolis USING btree (id);
CREATE UNIQUE INDEX oseba_id_uindex ON oseba USING btree (id);
CREATE UNIQUE INDEX pacient_id_uindex ON pacient USING btree (id);
CREATE UNIQUE INDEX pacient_sifrazavarovanja_uindex ON pacient USING btree (sifrazavarovanja);
CREATE INDEX pacientdn_delovninalog_index ON pacientdn USING btree (delovninalog);
CREATE INDEX pacientdn_pacient_index ON pacientdn USING btree (pacient);
CREATE UNIQUE INDEX razmerje_id_uindex ON razmerje USING btree (id);
CREATE UNIQUE INDEX uporabnik_id_uindex ON uporabnik USING btree (id);
CREATE UNIQUE INDEX vloga_id_uindex ON vloga USING btree (id);
CREATE UNIQUE INDEX vloga_ime_uindex ON vloga USING btree (ime);
CREATE UNIQUE INDEX zaposleni_id_uindex ON zaposleni USING btree (id);
CREATE UNIQUE INDEX zdravilo_id_uindex ON zdravilo USING btree (id);
ALTER TABLE ONLY delovninalog
    ADD CONSTRAINT delovninalog_vrstaobiska_id_fk FOREIGN KEY (vrstaobiska) REFERENCES vrstaobiska(id);
ALTER TABLE ONLY delovninalog
    ADD CONSTRAINT delovninalog_zaposleni_id_fk FOREIGN KEY (izdajatelj) REFERENCES zaposleni(id);
ALTER TABLE ONLY izvajalec
    ADD CONSTRAINT izvajalec_kraj_postna_fk FOREIGN KEY (postna) REFERENCES kraj(postna);
ALTER TABLE ONLY materialdn
    ADD CONSTRAINT materialdn_delovninalog_id_fk FOREIGN KEY (dn) REFERENCES delovninalog(id);
ALTER TABLE ONLY materialdn
    ADD CONSTRAINT materialdn_material_id_fk FOREIGN KEY (material) REFERENCES material(id);
ALTER TABLE ONLY obisk
    ADD CONSTRAINT obisk_delovninalog_id_fk FOREIGN KEY (delovninalog) REFERENCES delovninalog(id);
ALTER TABLE ONLY oseba
    ADD CONSTRAINT oseba_postna__fk FOREIGN KEY (postna) REFERENCES kraj(postna);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_okolis_id_fk FOREIGN KEY (okolis) REFERENCES okolis(id);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_oseba_id_fk FOREIGN KEY (kontaktnaoseba) REFERENCES oseba(id);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_oseba_id_fk2 FOREIGN KEY (id) REFERENCES oseba(id);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_pacient_id_fk FOREIGN KEY (skrbnik) REFERENCES pacient(id);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_razmerje_id_fk FOREIGN KEY (kontaktnaosebarazmerje) REFERENCES razmerje(id);
ALTER TABLE ONLY pacient
    ADD CONSTRAINT pacient_razmerje_id_fk2 FOREIGN KEY (skrbnikrazmerje) REFERENCES razmerje(id);
ALTER TABLE ONLY pacientdn
    ADD CONSTRAINT pacientdn_delovninalog_id_fk FOREIGN KEY (delovninalog) REFERENCES delovninalog(id);
ALTER TABLE ONLY pacientdn
    ADD CONSTRAINT pacientdn_pacient_id_fk FOREIGN KEY (pacient) REFERENCES pacient(id);
ALTER TABLE ONLY uporabnik
    ADD CONSTRAINT uporabnik_oseba_id_fk FOREIGN KEY (id) REFERENCES oseba(id);
ALTER TABLE ONLY uporabnik
    ADD CONSTRAINT uporabnik_vloga_id_fk FOREIGN KEY (vloga) REFERENCES vloga(id);
ALTER TABLE ONLY zaposleni
    ADD CONSTRAINT zaposleni_izvajalec_sifraizvajalca_fk FOREIGN KEY (izvajalec) REFERENCES izvajalec(id);
ALTER TABLE ONLY zaposleni
    ADD CONSTRAINT zaposleni_okolis_sifraokolisa_fk FOREIGN KEY (okolis) REFERENCES okolis(id);
ALTER TABLE ONLY zaposleni
    ADD CONSTRAINT zaposleni_uporabnik_id_fk FOREIGN KEY (id) REFERENCES uporabnik(id);
ALTER TABLE ONLY zdravilodn
    ADD CONSTRAINT zdravilodn_delovninalog_id_fk FOREIGN KEY (dn) REFERENCES delovninalog(id);
ALTER TABLE ONLY zdravilodn
    ADD CONSTRAINT zdravilodn_zdravilo_id_fk FOREIGN KEY (zdravilo) REFERENCES zdravilo(id);
