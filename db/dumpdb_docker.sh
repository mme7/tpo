(docker exec $(docker ps -f "name=strezniksql" -f "status=running" -q) '/usr/local/bin/pg_dump' -U postgres -d tpodb -c -C -E UTF8 --inserts --column-inserts) | grep -v -e '^--.*$' | grep -v -e '^$' | sed -e 's/DROP DATABASE/DROP DATABASE IF EXISTS/' > tpodb.sql
echo Opravljeno.
