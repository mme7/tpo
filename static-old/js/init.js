(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $(".dropdown-button").dropdown();
    $('ul.tabs').tabs();
    $('select').material_select();
    $('.collapsible').collapsible();
    $('.datepicker').pickadate({
        // http://api.jqueryui.com/datepicker/#option-firstDay
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 120, // Creates a dropdown to control year
      max: true,
      firstDay: 1,
      labelMonthNext: 'Naslednji mesec',
      labelMonthPrev: 'Prešnji mesec',
      labelMonthSelect: 'Izberi mesec',
      labelYearSelect: 'Izberi leto',
      monthsFull: [ 'Januar', 'Februar', 'Marec', 'April', 'Maj', 'Junij', 'Julij', 'Avgust', 'September', 'Oktober', 'November', 'December' ],
      monthsShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Avg', 'Sep', 'Okt', 'Nov', 'Dec' ],
      weekdaysFull: [ 'Nedelja', 'Ponedeljek', 'Torek', 'Sreda', 'Četrtek', 'Petek', 'Sobota' ],
      weekdaysShort: [ 'Ned', 'Pon', 'Tor', 'Sre', 'Čet', 'Pet', 'Sob' ],
      weekdaysLetter: [ 'N', 'P', 'T', 'S', 'Č', 'P', 'S' ],
      today: 'Danes',
      clear: 'Počisti',
      close: 'Zapri'
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space