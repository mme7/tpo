'use strict';
import axios from "axios";

class Zgodba3 {
    // prefix je zaenkrat kludge, ker izgleda da imamo dva razlicna vmesnika za
    // isto funkcionalnost
    static PoskusiPrijavo(prefix) {
        axios.post('/api/login', {
            email: document.getElementById(prefix.concat("email")).value,
            geslo: document.getElementById(prefix.concat("geslo")).value
        })
        .then(r=>{
            if(r.data)  // prijava uspesna
                window.location.replace("/");
            else {
                var odziv = $('#odzivStreznika');
                odziv.addClass('chip red lighten-3')
                odziv.html("Vnesli ste napačen e-poštni naslov ali geslo.");
            }
        });
    }
    static ValidateGeslo(element)
    {
        var vneseno = document.getElementById(element).value.length;
        if(vneseno < 8)
        {
            document.getElementById(element).style.borderBottom = "2px solid red";
            var odziv = $('#odzivStreznika');
            odziv.addClass('chip red lighten-3')
            odziv.html("Geslo je prekratko");
        }
        else
        {
            document.getElementById(element).style.borderBottom = "2px solid green";
            var odziv = $('#odzivStreznika');
            odziv.removeClass('chip red lighten-3');
            odziv.empty();
        }
    }    
}



