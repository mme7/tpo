'use strict';
import axios from "axios";

class Zgodba4 {
    
    static PoskusiSprememboGesla() {
        axios.post('/api/configuration', {
            oldPsw: document.getElementById("old_password").value,
            newPsw: document.getElementById("new_password").value,
            confirmPsw: document.getElementById("confirm_password").value
        })
        .then(r=>{
            if(r.data) {
                window.location.replace("/");
            } else {
                var odziv = $('#odzivStreznika');
                odziv.addClass('chip red lighten-3')
                odziv.html("Podatkov niste pravilno izpolnili.");
            }
        });
    }    
    
}



