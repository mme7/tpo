# Na splošno
Spletna stran, ki se razvija v sklopu predmeta Tehnologija Programske Opreme, v letu 2016/17.


---
# Potrebna razvojna orodja
Minimalno je potrebno sledeče:

- jdk8
- maven
- docker
- docker-compose
- npm (vključno z njim node)
- angular-cli
- _pure magic_

Za deployment na AWS je potrebno še nekaj orodij, za natančna navodila glej `./dokumentacija/AWS-ECS-README.md`

---
# Kako prevesti ter izvesti
V nadaljevanju se `./` nanaša na koren projekta.

Projekt podpira več načinov izvajanja, odvisno od konteksta uporabe:
## 1. Backend razvoj
Baza v docker containerju, backend se izvaja direktno, ta servira statični Angular.
Nekako najhitrejši način če spreminjaš samo backend.
### 1.1 Zbuildaj frontend
Pojdi v `./ps-angular/` ter izvedi `npm install` (potrebno le prvič, oz. ob uvedbi novega npm dependencyja) ter `npm run-script build-dev`.

S tem v `./ps-angular/dist/` zgenerirate statično različico fontenda, ki ga lahko servira backend.

### 1.2 Vzpostavi bazo
Izvedi `docker ps -a`, poglej kateri containerji pripadajo `tposql` ter jih izbriši z `docker rm` (oz. če bi radi izbrisali vse containerje, izvedi `docker container prune`). Nato naredi `docker rmi tposql`.

Pojdi v `./db/`, izvedi `docker-compose up`, s tem se vzpostavi + inicializira baza (iz `tpodb.sql`, ki je v istem direktoriju).

Da shranite novo stanje baze v docker containerju, izvedite `dumpdb_docker.sh` (*nix only, TODO powershell različica ali kaj podobnega).

### 1.3 Zbuildaj backend
Pojdi v `./ps-spark/`, izvedi `mvn package`.
Če si šele kloniral repo, ali pa si prišel iz starih verzij, skopiraj example konfiguracijo `./ps-spark/primer-ps-spark.json` v `./ps-spark/ps-spark.json`

Zbuildan backend poženeš z `mvn exec:java`

Če želiš posodobiti jooq-ovo preslikavo baze v POJOte, potem znotraj `./ps-spark/`:

1. kopiraj `primer-settings.xml` v `settings.xml` ter v njem nastavi pravo geslo za dostop do baze (znotraj `jdbcpassword` tag-a)
2. izvedi `mvn --settings ./settings.xml package -P jooq`
---

## 2. Frontend razvoj
Baza v docker containerju, webpack strežnik ki servira frontend + proxy na backend.
Omogoča, da lahko on-the-fly urejaš frontend, hkrati pa uporabljaš API backenda.

### 2.1 Vzpostavi webpack strežnik
Pojdi v `./ps-angular` ter izvedi `npm install` (potrebno le prvič, oz. ob uvedbi novega npm dependencyja) ter `npm start`

S tem se zažene webpack strežnik s proxyjem, katerega konfiguracija je v `proxy.config.json`

Glede na trenutno konfiguracijo se vse zahteve na `/api/` preusmerijo na backend.

### 2.2 Vzpostavi bazo
Glej 1.2

### 2.3 Vzpostavi backend
Glej 1.3

---

## 3. Lokalni deployment
Projekt se v celoti izvaja v Docker containerjih. V takem okolju se naj bi izvajale release verzije spletne strani.

Priporočljivo je, da se prvo preveri na tak način, predno se pošlje na AWS.

Prvo počisti vse prejšnje containerje, ki pripadajo slikam `tposql` ter `tpojava` (poglej `docker ps -a` ter naredi `docker rm` za vse pripadajo tem slikam - ali pa `docker container prune` za izbris _vseh_ containerjev)

Nato pa v korenu (`./`) projekta izvedi `mvn package` (ki zbuilda frontend, backend ter containerje), nastale slike pa lokalno zaženeš z `docker-compose up`

---

## 4. Deployment na AWS

Za podrobnosti glej `./dokumentacija/AWS-ECS-README.md`.

TL;DR gre v osnovi tako:

1. Izvedi korake za lokalni deployment do (ne vključno) samega lokalnega zagona
2. (če še nisi) odpri .csv s svojimi ključi za avtentikacijo za AWS
3. (če še nisi) izvedi `aws configure`, vnesite ključe ter za "_Default region name_" nastavite `eu-central-1`
4. prijavi se v AWS repo

    `$(aws ecr get-login --registry-ids 981739140558)`

5. pošlji gor slike

    `ecs-cli push --registry-id 981739140558 tposql:latest`

    ter

    `ecs-cli push --registry-id 981739140558 tpojava:latest`

6. zahtevaj zagon novih slik z 

    `ecs-cli compose --file docker-compose-ecs.yml up`

In bi moralo delat.