import { PsAngularPage } from './app.po';

describe('ps-angular App', () => {
  let page: PsAngularPage;

  beforeEach(() => {
    page = new PsAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('ps works!');
  });
});
