import { Component } from '@angular/core';
import { Avtentikacija } from './avtentikacija/avtentikacija';
import { Router } from "@angular/router";

@Component({
  selector: 'ps-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {  
  constructor(public avtentikacija:Avtentikacija, private router:Router){}

  odjava(){
    this.avtentikacija.logout();
    this.router.navigate(["/"]);
  }
}