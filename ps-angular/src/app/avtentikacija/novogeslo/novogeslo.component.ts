import { Component, OnInit, } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Avtentikacija } from '../avtentikacija';
import { Dialog } from "app/komponente/dialog/dialog.component";

@Component({
  selector: 'ps-novogeslo',
  templateUrl: './novogeslo.component.html',
  styleUrls: ['./novogeslo.component.css']
})
export class NovogesloComponent implements OnInit {

  loginForm = new FormGroup({
    oldPsw: new FormControl('', []),
    newPsw: new FormControl('', [Validators.required, this.preveriStrukturoGesla.bind(this)]),
    newPsw2: new FormControl('', [Validators.required, this.preveriUjemanje.bind(this)])
  });

  constructor(
    private route: ActivatedRoute,
    private http: Http,
    private router: Router,
    private avtentikacija: Avtentikacija,
    public dialog: Dialog
  ) { }

  ngOnInit() {
    this.loginForm.valueChanges.subscribe(_ => {
      this.loginForm.controls.newPsw.updateValueAndValidity({ emitEvent: false });
      this.loginForm.controls.newPsw2.updateValueAndValidity({ emitEvent: false });
    });
  }

  preveriUjemanje(control: FormControl): { [s: string]: boolean } {
    if (this.loginForm !== undefined && this.loginForm.get('newPsw').value !== this.loginForm.get('newPsw2').value) {
      return { 'nevjemanje': true };
    }
    return null;
  }

  preveriStrukturoGesla(control: FormControl): { [s: string]: boolean } {
    let g = control.value as string;
    if (g === null) return null;

    if (g.length < 8) {
      return { 'premajhnogeslo': true };
    } else if (g.split("").find(c => Number.isInteger(+c)) === undefined) {
      return { 'nistevilke': true };
    }
    return null;
  }

  onSubmit() {
    console.log(this.loginForm.value);
    this.http.post("/api/configuration", JSON.stringify(this.loginForm.value)).subscribe(r => {
      this.dialog.odpri("Geslo uspešno spremenjeno.", this.preusmeri.bind(this), ["V redu", this.preusmeri.bind(this)]);
    }, () => {
      this.dialog.odpri("Napačno staro geslo.", undefined, ["V redu", undefined]);
      ;
    });
  }

  preusmeri() {
    this.router.navigate(["/"]);
  }

}
