import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovogesloComponent } from './novogeslo.component';

describe('NovogesloComponent', () => {
  let component: NovogesloComponent;
  let fixture: ComponentFixture<NovogesloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovogesloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovogesloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
