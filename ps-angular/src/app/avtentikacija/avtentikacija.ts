import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { Http, Headers, Response, RequestOptions } from '@angular/http'
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Uporabnik, Oseba, Vloga, Izvajalec, Vrstaobiska } from '../entitete'


@Injectable()
export class Avtentikacija {

  public vloge = this.http
    .get("/api/seznam-vlog")
    .map(vs => vs.json() as Vloga[])
    .publishLast().refCount();

  public izvajalci = this.http
    .get("/api/seznam-izvajalcev")
    .map(is => is.json() as Izvajalec[])
    .publishLast().refCount();

  public vrsteobiska = this.http
    .get("/api/seznam-vrstobiska")
    .map(is => is.json() as Vrstaobiska[])
    .publishLast().refCount();

  constructor(private http: Http, private router: Router) {
    this.vloge.subscribe();
    this.izvajalci.subscribe();
    this.vrsteobiska.subscribe();
    this.pridobiUporabnika().catch(() => console.log("Neprijavljeni."));
  }

  private _uporabnik: Uporabnik;
  public get uporabnik(): Uporabnik {
    return this._uporabnik;
  };
  public set uporabnik(val: Uporabnik) {
    this._uporabnik = val;
    this.vlogaString(val).subscribe(r => {
      this.vlogaStr = r;
    });
  };

  public oseba: Oseba;
  public vlogaStr = "";

  public logout() {
    return this.http.get("/api/logout").subscribe(() => {
      this.uporabnik = undefined;
      this.oseba = undefined;
      console.log("logout");
      this.router.navigate(["/"]);
    });
  }

  public timestampPrijave : string;
  public pridobiUporabnika(): Promise<Uporabnik> {
    return this.http.get("/api/prijavljeni_uporabnik").map(r => {
      this.uporabnik = r.json() as Uporabnik;
      this.timestampPrijave = this.uporabnik.datumzadnjeprijave ?
      (new Date(this.uporabnik.datumzadnjeprijave)).toLocaleString()
      : undefined;

      this.http.get("/api/prijavljena_oseba").subscribe(r => {
        if (r.status === 200) {
          this.oseba = r.json() as Oseba;
        } else {
          this.oseba = null;
        }
      });

      return this.uporabnik;
    }).toPromise();
  }

  public vlogaString(o: Uporabnik): Observable<string> {
    if (!o) {
      return Observable.of(undefined);
    }
    return this.vloge.map(vs => {
      let f = vs.find(x => x.id === +o.vloga);
      return f !== undefined ? f.ime : "ni vloge"
    });
  }
}
