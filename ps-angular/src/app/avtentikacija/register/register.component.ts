import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Avtentikacija } from '../avtentikacija';
import { Sifranti } from '../../sifranti.service';
import { DatumComponent } from '../../komponente/datum/datum.component';
import { Oseba, UporabnikDB, PacientDB } from '../../entitete';

import { IMyOptions } from 'mydatepicker';

// podrejene forme
import { OsebaFormComponent } from './oseba-form.component'
import { PacientFormComponent } from './pacient-form.component'
import { MdDialog } from "@angular/material";
import { RegisterStatusDialog } from "./register-status-dialog.component";

@Component({
  selector: 'ps-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  // forma same komponente
  registerForm: FormGroup;
  // podrejene komponente
  pacientiSkrbnika: FormArray;

  constructor(
    private fb: FormBuilder,
    private http: Http,
    public dialog: MdDialog,
    private router: Router,
    public avtentikacija: Avtentikacija,
    public sifranti: Sifranti
  ) {}

  ngOnInit() {
    // tvorimo FormArray dodatnih pacientov - na zacetku prazen
    this.pacientiSkrbnika = this.fb.array([]);

    this.registerForm = this.fb.group({
      osebaPacient: PacientFormComponent.vrniFormo(this.fb),
      
      email: ['', [Validators.required, Validators.email]],
      geslo: ['', [Validators.required, this.preveriUjemanje.bind(this), this.preveriStrukturoGesla.bind(this)]],
      geslo2: ['', this.preveriUjemanje.bind(this)],
      osebaKontaktna: OsebaFormComponent.vrniFormoDovoljenaNeizpolnjeno(this.fb),
      kontaktnarazmerje: '',
      pacientiSkrbnika: this.pacientiSkrbnika
    });
  }

  onSubmit() {
    // pridobimo trenutno stanje form model-a
    let model = this.registerForm.value;
    
    let osebaRegistriraniPacient = {
      datumrojstva: model.osebaPacient.datumrojstva,
      spol: model.osebaPacient.spol,
      okolis: model.osebaPacient.okolis,
      sifrazavarovanja: model.osebaPacient.sifrazavarovanja
    }

    let osebaRegistriraniUporabnik = {
      email: model.email,
      geslo: model.geslo
    }
    let oskrbovaniPacienti = model.pacientiSkrbnika.map(tps => {
      return {
        oseba: tps.pacient.osebaForm,
        pacient: {
          datumrojstva: tps.pacient.datumrojstva,
          spol: tps.pacient.spol,
          okolis: tps.pacient.okolis,
          sifrazavarovanja: tps.pacient.sifrazavarovanja,
          skrbnikrazmerje: tps.razmerje
        }
      }
    })

    // tvorimo ogrodje, ter z Object.assign dodamo zdruzimo podatke
    let registracija = {
      registriranaOseba: model.osebaPacient.osebaForm,
      registriranPacient: osebaRegistriraniPacient,
      
      osebaRegistriraniUporabnik: osebaRegistriraniUporabnik,
      oskrbovaniPacienti: oskrbovaniPacienti
    };
    // podatke, vezane na kontaktno osebo, dodamo v objekt le, ce je
    // le ta izpolnjena pravilno (ter ni prazna)
    if(this.registerForm.get('osebaKontaktna').valid &&
       this.registerForm.get('osebaKontaktna.ime').value.length !== 0) {
      Object.assign(osebaRegistriraniPacient, {
        kontaktnarazmerje: model.kontaktnarazmerje
      });
      Object.assign(registracija, {
        osebaRegistriraniKontaktna: model.osebaKontaktna,
      });
    }

    this.http.post('/api/pacient/register', JSON.stringify(registracija))
      .toPromise()
      .then(res => {
        let statusDialog = this.dialog.open(RegisterStatusDialog);
        statusDialog.componentInstance.status = "Registracija uspešna!"
        statusDialog.componentInstance.vsebina = "Glej svoj email za aktivacijo računa.";
        statusDialog.afterClosed().subscribe(result => this.router.navigate(["/login"]))
      }).catch(res => {
        let statusDialog = this.dialog.open(RegisterStatusDialog);
        statusDialog.componentInstance.vsebina = res;
      });
  }
  dodajPacientaSkrbnika() {
    // dodamo kombinacijo forme pacienta ter okolisnih kontrolnikov
    this.pacientiSkrbnika.push(this.fb.group({
      pacient: PacientFormComponent.vrniFormo(this.fb),
      razmerje: ['', Validators.required]
    }));
  }

  //-------validatorji
  preveriUjemanje(control: FormControl): { [s: string]: boolean } {
    if (this.registerForm !== undefined && control.value !== this.registerForm.get('geslo').value) {
      return { 'nevjemanje': true };
    }
    return null;
  }

  preveriStrukturoGesla(control: FormControl): { [s: string]: boolean } {
    let g = control.value as string;
    if (g === null) return null;

    if (g.length < 8) {
      return { 'premajhnogeslo': true };
    } else if (g.split("").find(c => Number.isInteger(+c)) === undefined) {
      return { 'nistevilke': true };
    }
    return null;
  }

}
