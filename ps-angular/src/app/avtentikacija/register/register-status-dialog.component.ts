import { Component, OnInit } from '@angular/core';
import { MdDialog } from "@angular/material";

@Component({
    selector: 'ps-register-status-dialog',
    template: `<md-dialog-content>
                    <p style="text-align: center;">{{this.status}}</p>
                    <p>{{this.vsebina}}</p>
                </md-dialog-content>`,
    styles: [`
        md-dialog-content {
            display: block;
            font-family: Roboto,"Helvetica Neue",sans-serif;;
        }
    `]
})
export class RegisterStatusDialog {
    public status: string;
    public vsebina: string;
}