import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';

import { Sifranti } from '../../sifranti.service';

@Component({
    selector: 'oseba-form',
    templateUrl: './oseba-form.component.html',
    styleUrls: ['./oseba-form.component.css']
})
export class OsebaFormComponent implements OnInit {
    @Input()
    formGroup: FormGroup;

    constructor(public sifranti: Sifranti) {
        // znotraj konstruktorja se @Input bindingi se ne postavijo!
        // (posledicno ne tu ne moremo inicializirati komponente glede na
        // parametre parent-a)
    }

    // direktno podrejenih form ta komponenta nima
    ngOnInit() {
        // tu imamo zagotovljeno, da so postavljeni vsi @Input bindingi
        //
        // pri tem je tezava v tem, da se ngOnInit funkcije podrejenih komponent
        // klicejo za ngOnInit parent komponente, kar pomeni da mora parent
        // inicializirati formo, ta komponenta pa lahko kvecjemu pridobi le
        // njegovo instanco
        //
        // Torej, dogovor naj bo tak, da podrejene komponente implementirajo vse
        // svoje inicializacije form, njihove direktno nadrejene komponente pa jih
        // klicejo.
        // V primeru, da ta komponenta generira tudi druge podrejene komponente, mora
        // inicializacijska rutina TE komponente klicati inicializacijske rutine
        // podrejenih komponent.
        //
        // Validatorji forme pa ze sami po sebi pridobijo referenco na FormGroup objekt,
        // katerega preverjajo, tako da tu ni potrebno skrbeti za hierarhijo.
        //
        // V osnovi za podrejene forme niti ne potrebujemo instance same komponente,
        // moramo pa definirati formGroupName - tega dolocimo v parent komponenti
        // ter ga v template te komponente pridobimo preko input.
    }

    // vrne formo te komponente - potrebuje instanco formBuilderja
    public static vrniFormo(fb: FormBuilder): FormGroup {
        return fb.group({
            ime: ['', [Validators.required, Validators.pattern('[a-zA-Z\xC0-\uFFFF]*')]],
            priimek: ['', [Validators.required, Validators.pattern('[a-zA-Z\xC0-\uFFFF]*')]],
            ulica: ['', [Validators.required, Validators.pattern('[a-zA-Z\xC0-\uFFFF 0-9]*')]],
            hisnastevilka: ['', [Validators.required, Validators.pattern('[a-zA-Z\xC0-\uFFFF0-9]*')]],
            postna: ['', Validators.required],
            tel: ['', ]
        }, {});
    }
    // vrne formo te komponente, pri cemer je veljavno stanje, da se v celoti pusti
    // neizpolnjeno - potrebuje instanco formBuilderja
    public static vrniFormoDovoljenaNeizpolnjeno(fb: FormBuilder): FormGroup {
        // vsi custom validatorji morajo biti definirani znotraj konteksta
        // funkcije

        // objekt za deljeno stanje runtime validatorjev
        let validatorStanje = {
            formaEnaIzpolnjena : false,
            formaPrazna : false
        };
        // tvorimo validator, ki gleda ali je njihova vsebina prazna
        // vendar samo takrat ko ni forma v celoti prazna
        let zahtevanCeNeprazno = (control: AbstractControl): ValidationErrors | null => {
            if(!validatorStanje.formaPrazna && 
                (!control.value || (control.value && control.value.length === 0))
              ) {
                return {prazen : true};
            }
            return null;
        }

        return fb.group({
            ime: ['', [zahtevanCeNeprazno, Validators.pattern('[a-zA-Z\xC0-\uFFFF]*')]],
            priimek: ['', [zahtevanCeNeprazno, Validators.pattern('[a-zA-Z\xC0-\uFFFF]*')]],
            ulica: ['', [zahtevanCeNeprazno, Validators.pattern('[a-zA-Z\xC0-\uFFFF 0-9]*')]],
            hisnastevilka: ['', [zahtevanCeNeprazno, Validators.pattern('[a-zA-Z\xC0-\uFFFF0-9]*')]],
            postna: ['', [zahtevanCeNeprazno]],
            tel: ['', [zahtevanCeNeprazno, Validators.pattern('[0-9 ]*')]]
        }, {
                validator:
                // validira, ali so vsa polja izpolnjena
                // v osnovi jih je lahko vec (po istem principu kot za kontrolnike)
                (formGroup: FormGroup) => {
                    validatorStanje.formaPrazna = Object.keys(formGroup.controls).every(k => !formGroup.get(k).value || formGroup.get(k).value.length === 0);
                    if (!validatorStanje.formaPrazna &&
                        !Object.keys(formGroup.controls).every(k => formGroup.get(k).value && formGroup.get(k).value.length !== 0)) {
                        return { delnoIzpolnjen: true };
                    }
                    else if(validatorStanje.formaPrazna) {
                        Object.keys(formGroup.controls).forEach(k => formGroup.get(k).updateValueAndValidity({onlySelf:true, emitEvent:true}));
                        formGroup.markAsUntouched();
                    }
                    else
                        return null; // pravilno validirano
                }
            });
    }
}