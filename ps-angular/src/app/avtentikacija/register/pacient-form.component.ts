import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { Sifranti } from '../../sifranti.service';

// podrejene forme
import {OsebaFormComponent} from './oseba-form.component'

@Component({
    selector: 'pacient-form',
    templateUrl: './pacient-form.component.html'
})
export class PacientFormComponent implements OnInit {
    // instanca same forme, da jo lahko bindamo v template-u
    @Input()
    formGroup : FormGroup;

    constructor(public sifranti: Sifranti) { }
    ngOnInit() {}
    
    // vrne krovno formo te komponente
    public static vrniFormo(fb: FormBuilder): FormGroup {
        return fb.group({
            osebaForm: OsebaFormComponent.vrniFormo(fb),

            datumrojstva: ['', Validators.required],
            spol: ['', Validators.required],
            okolis: ['', Validators.required],
            sifrazavarovanja: ['', [Validators.required,  Validators.pattern('[0-9]*')]]
        });
    }
}