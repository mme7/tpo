import { Component, OnInit, } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Avtentikacija } from '../avtentikacija';

import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'ps-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required]),
    geslo: new FormControl('', [Validators.required])
  });
  public napaka = "";
  constructor(
    private route: ActivatedRoute,
    private http: Http,
    private router: Router,
    private avtentikacija: Avtentikacija
  ) { }
  // za onemogocanje kontrol ce ban v poteku
  ban : boolean = false;
  preveriPrijavo(odgovor : PrijavaOdgovor) : boolean {
    // ce dobimo false, disablamo formo, pri
    switch(odgovor.status) {
      case PrijavaStatus.BAN:
      this.ban = true;
      this.loginForm.disable();
      this.napaka = `Prijava onemogočena za ${odgovor.banPreostaloMinut} min`;
      return false;

      case PrijavaStatus.NAPACNA_PRIJAVA:
      this.napaka = "Napačna prijava.";
      return false;

      case PrijavaStatus.PRIJAVA_OK:
      this.ban = false;
      this.loginForm.enable();
      this.napaka = "";
      return true;
    }
  }

  ngOnInit() {
    // da se v email formo dopolne email, ki je podan v URLju (sploh uporabno?)
    this.route.params.subscribe(params => {
      if (params['email']) {
        this.loginForm.controls.email.setValue(params['email']);
      }
    });
    // preverimo, ce je login sploh mogoc
    this.http.get("/api/prijava_mogoca").toPromise().then(r=>{
      this.preveriPrijavo(r.json() as PrijavaOdgovor);
    });
  }

  onSubmit() {
    let prijava = {
      email: this.loginForm.get('email').value,
      geslo: this.loginForm.get('geslo').value
    }
    this.http.post("/api/login2", JSON.stringify(prijava)).toPromise().then(r => {
      let odgovor : PrijavaOdgovor = (r.json() as PrijavaOdgovor);

      if (this.preveriPrijavo(odgovor)) {
        localStorage.setItem("login", odgovor.loginCookie);
        this.avtentikacija.pridobiUporabnika().then(() => {
          this.router.navigate(["/"]);
        });
      }
    });
  }
}
// razredi za odgovor od streznika
enum PrijavaStatus {
  PRIJAVA_OK = 0,     // uspesna prijava
  NAPACNA_PRIJAVA,    // v primeru prvih par napacnih poskusov pred banom
  BAN                 // dosegli ban, ni druge kot da pocakamo
}
class PrijavaOdgovor {
  status: PrijavaStatus;

  // v primeru, da je IP bannan, server vrne stevilo minut do konca
  banPreostaloMinut: number;

  // v primeru, da login uspe, srver vrne login piskotek
  loginCookie: string;
}
