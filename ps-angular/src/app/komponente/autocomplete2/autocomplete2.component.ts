import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl, Validator, AbstractControl, Validators } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { Http } from '@angular/http'
import { AutoselectnadrazredComponent } from "app/komponente/autoselectnadrazred.component";


@Component({
  selector: 'ps-autocomplete2',
  templateUrl: './autocomplete2.component.html',
  styleUrls: ['./autocomplete2.component.css'],
  exportAs: 'ps-autocomplete2',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => Autocomplete2Component),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => Autocomplete2Component),
      multi: true,
    }]
})
export class Autocomplete2Component extends AutoselectnadrazredComponent implements OnInit, ControlValueAccessor, Validator {


  public vsebujeString = false;
  public objekt = undefined;

  filtriranoPolje: Observable<any> = null;

  //ne sme biti rest storitev, ker ni nujno da so vse možnosti vedno v _polju
  setValueFn = () => {
    if (this._polje !== undefined && typeof this._polje !== 'string') {
      this.inputControl.setValue(this._polje.find(e => this._vrednost(e) === this.writeValueObject));
    }
  }


  //-----Validator impls
  validate(c: AbstractControl): { [key: string]: any; } {
    if (this.vsebujeString) {
      return { "vsebujestring": true }
    }
    return null;
  }
  registerOnValidatorChange(fn: () => void): void { }
  //-----


  //-----ControlValueAccessor impls
  writeValue(obj: any): void {
    this.writeValueObject = obj;
    if (this._rest_url_polja === undefined) {
      this.writeValueObject = obj;
      this.setValueFn();
    } else {
      if (obj === undefined || obj === null || obj === Number.NaN) {
        this.inputControl.setValue(undefined);
      } else {
        if (this.pridobi1Url === null) {
          this.http.post(this._rest_url_polja, obj)
            .map(x => x.json())
            .subscribe(r => {
              this.inputControl.setValue(r[0]);
            })
        } else {
          this.http.post(this.pridobi1Url, obj)
            .map(x => x.json())
            .subscribe(r => {
              this.inputControl.setValue(r);
            })
        }
      }
    }

  }
  registerOnChange(fn: any): void {
    this.changeFn = (x) => {
      this.trenutno = x;
      fn(x);
    };
  }
  registerOnTouched(fn: any): void { }
  setDisabledState(isDisabled: boolean): void { }
  //------


  constructor(http: Http) {
    super(http)
  }

  ngOnInit() {
    this.filtriranoPolje = this.inputControl
      .valueChanges
      .do(v => { 
        this.vsebujeString = (typeof v === 'string');
        if(!this.vsebujeString){
          this.objekt = v;
        }
       })
      .debounce(() => Observable.interval(250))
      .distinctUntilChanged()
      .switchMap((p) => {
        if (p === null || p === undefined) {
          this.changeFn(undefined);
          return Observable.of([]);
        } else if (typeof p === 'object') {
          this.changeFn(this._vrednost(p))
          return Observable.of([p]);
        } else if (typeof p === 'string') {
          this.changeFn(undefined);
          if ((p as string).length > 0) {
            if (this._rest_url_polja === undefined) {
              return Observable.of(this._polje.filter(o => this._filter(o, p)));
            } else {
              return this.http.post(this._rest_url_polja, p).map(x => x.json());
            }
          } else {
            return Observable.of([]);
          }
        } else {
          this.changeFn(undefined);
          console.error("Neznan objekt");
          console.log(typeof p);
        }
      });
  }

  izvediPrikaznoFunkcijo(o: any): string {
    if (typeof o === 'object' && o !== null && o !== undefined) {
      return this._prikaz(o)
    } else if (typeof o === 'string') {
      return o as string;
    } else {
      return "";
    }
  }
}
