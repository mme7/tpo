import { Component, OnInit, Input, Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Http } from "@angular/http";
import { FormControl } from "@angular/forms";

@Injectable()
export class AutoselectnadrazredComponent implements OnInit {
  @Input() placeholder;
  public trenutno;

  protected changeFn = (x: any) => { this.trenutno = x };

  writeValueObject = undefined;
  inputControl = new FormControl(null);
  setValueFn = () => { };

  constructor(protected http: Http) { }



  ngOnInit() {
  }


  //---- formatiranje prikaza (ce ni nastavljeno bo izpisal vsa polja) 
  public _prikaz = (obj: any) => {
    let c = "";
    for (let i in obj) {
      c = c.concat(obj[i]) + " ";
    }
    return c;
  };
  @Input() set prikaz(val: any) {
    if (typeof val === "string") {
      this._prikaz = eval(val);
    } else if (val instanceof Array) {
      this._prikaz = x => {
        let c = "";
        val.forEach(e => {
          c = c.concat(x[e]) + " ";
        });
        return c;
      }
    } else {
      this._prikaz = val;
    }
  }
  get prikaz() {
    return this._prikaz;
  }

  //---- polje možnih vrednosti (lahko je array, observable ali pa url servisa, ki vrača rezultate), edini obvezni parameter
  protected _rest_url_polja = undefined;
  public _polje = undefined;
  @Input()
  set polje(val: any) {
    if (val instanceof Array) {
      this._polje = val;
      this._rest_url_polja = undefined;
    } else if (val instanceof Observable) {
      val.subscribe(v => {
        this._polje = v;
        this._rest_url_polja = undefined;
        this.setValueFn();
      });
    } else if (typeof val === "string") {
      this._rest_url_polja = val;
    } else if (val === null || val === undefined) {
      this._polje = undefined;
    } else {
      console.error("Nepoznan tip objekta v polju.")
      console.log(val)
    }
    if (this._polje !== undefined) {
      this.setValueFn();
    }
  }
  get polje() {
    return this._polje;
  }


  //---- url za pridobitev enega samega rezultata, kadar polje pridobimo preko urlja in 
  //potebujemo nastaviti predhodno vrednost autocompleta (ker vrenost ni nujno enaka iskalnemu izrazu oz lahko ne vrne samo enega zadetka)
  //če ni nastavljeno se izbere prvi zadetek iz urlja polja 
  @Input()
  protected pridobi1Url = null;

  //----filter: ce je polje array ali observale bo filter funkcija za filtriranje zadetkov, ce ni nastavljena bo funkcija iskala po vsek poljih
  public _filter = (obj: any, term: string) => {
    if (this._prikaz(obj).toLocaleLowerCase().search(term.toLowerCase()) !== -1) {
      return true
    } else {
      return false;
    }
  };
  @Input()
  set filter(val: any) {
    if (typeof val === "string") {
      this._filter = eval(val);
    } else {
      this._filter = val;
    }
  }
  get filter() {
    return this._filter;
  }

  //----vrednost: vrednost, ki se bo nahajala iz v ngModelu (če ni določeno se bo nahajal objekt sam)
  public _vrednost = (obj: any) => obj;
  @Input()
  set vrednost(val: any) {
    if (typeof val === "string") {
      this._vrednost = eval(val);
    } else if (val instanceof Array) {
      if (val.length === 1) {
        this._vrednost = x => {
          return x[val[0]];
        }
      } else {
        this._vrednost = x => {
          let c = new Object();
          val.forEach(e => {
            c[e] = x[e];
          });
          return c;
        }
      }
    } else {
      this._vrednost = val;
    }
  }
  get vrednost() {
    return this._vrednost;
  }

}
