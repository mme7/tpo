import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl, Validator, AbstractControl, Validators } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { Http } from '@angular/http'
import { AutoselectnadrazredComponent } from "app/komponente/autoselectnadrazred.component";

@Component({
  selector: 'ps-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
  exportAs: 'ps-select',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    }]
})
export class SelectComponent extends AutoselectnadrazredComponent implements OnInit, ControlValueAccessor, Validator {

  public objekt = undefined;

  setValueFn = () => {
    if (this._polje !== undefined) {
      this.inputControl.setValue(this._polje.find(e => this._vrednost(e) === this.writeValueObject));
    }
  }


  //-----Validator impls
  validate(c: AbstractControl): { [key: string]: any; } { return null }
  registerOnValidatorChange(fn: () => void): void { }
  //-----


  //-----ControlValueAccessor impls
  writeValue(obj: any): void {
    this.writeValueObject = obj;
    this.setValueFn();
  }
  registerOnChange(fn: any): void {
    this.changeFn = (x) => {
      this.trenutno = x;
      fn(x);
    };
  }
  registerOnTouched(fn: any): void { }
  setDisabledState(isDisabled: boolean): void { }
  //------

  constructor(http: Http) {
    super(http)
  }

  ngOnInit() {
    this.inputControl.valueChanges
      .do((v) => {
        this.objekt = v;
      })
      .subscribe(v => {
        if (v !== null && v !== undefined) {
          this.changeFn(this._vrednost(v));
        } else {
          this.changeFn(v);
        }
      })
  }

}
