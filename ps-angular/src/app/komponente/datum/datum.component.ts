import { Component, OnInit, Input, ViewEncapsulation, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from "@angular/forms";
import { MdDialog, MdDialogConfig } from "@angular/material";
import { DatumDialogComponent } from "./datum-dialog/datum-dialog.component";

@Component({
  selector: 'ps-datum',
  templateUrl: './datum.component.html',
  styleUrls: ['./datum.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatumComponent),
      multi: true,
    }
  ]
})
export class DatumComponent implements OnInit {

  @Input() placeholder;
  inputControl = new FormControl(null);

  izbran: Date;
  danes: Date;


   //---- formatiranje prikaza (ce ni nastavljeno bo izpisal vsa polja) 
  public _dovoljen = (obj: Date) => {
      return true;
  };
  @Input() set dovoljen(val: any) {
    if (typeof val === "string") {
      this._dovoljen = eval(val);     
    } else {
      this._dovoljen = val;
    }
  }
  get dovoljen() {
    return this._dovoljen;
  }


  protected changeFn = (x: any) => { };
  constructor(public dialog: MdDialog) {
    this.danes = new Date();
    this.danes.setHours(12, 0, 0, 0);
  }

  //-----ControlValueAccessor impls
  writeValue(obj: any): void {
    if (typeof obj === 'string') {
      let dn: number = Date.parse(obj);
      if (!Number.isNaN(dn)) {
        obj = new Date(dn);
      }
    }
    if (obj !== undefined && obj !== null && typeof obj !== 'string') {
      if (typeof obj === 'string') throw new Error();
      this.izbran = obj;
      this.izbran.setHours(12, 0, 0, 0);
      this.inputControl.setValue(`${this.izbran.getDate()}.${this.izbran.getMonth() + 1}.${this.izbran.getFullYear()}`);
    }
  }
  registerOnChange(fn: any): void {
    this.changeFn = (x) => {
      if (typeof x === 'string') {
        console.error("napaka string mečem!");
      }
      return fn(x);
    };
  }
  registerOnTouched(fn: any): void { }
  setDisabledState(isDisabled: boolean): void { }
  //------


  ngOnInit() {
    this.izbran = new Date(this.danes);
    this.inputControl.valueChanges.subscribe(v => {
      if (v !== null && v !== undefined) {
        let p = v.split(".");
        if (p.length !== 3) {
          this.changeFn(undefined);
          return;
        }
        let y = +(p[2].trim());
        let m = +(p[1].trim());
        let d = +(p[0].trim());
        if (y > 1900 && y < 2100 && m >= 1 && m <= 12 && d >= 1 && d <= 31) {
          this.izbran = new Date(y, m - 1, d);
          this.changeFn(this.izbran);
          return;
        } else {
          this.changeFn(undefined);
        }
      }
    })
  }


  odpriVnosnoOkno() {
    let config = new MdDialogConfig();
    config.data = {
      'izbran': this.izbran,
      'danes': this.danes,
      'dovoljen': this.dovoljen
    }
    let dialogRef = this.dialog.open(DatumDialogComponent, config);
    dialogRef.updateSize('300px', '350px');
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.writeValue(result);
        this.changeFn(result);
      }
    });

  }

}
