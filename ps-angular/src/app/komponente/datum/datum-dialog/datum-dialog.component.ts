import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from "@angular/material";

@Component({
  selector: 'ps-datum-dialog',
  templateUrl: './datum-dialog.component.html',
  styleUrls: ['./datum-dialog.component.css']
})
export class DatumDialogComponent implements OnInit {

  izbran: Date;
  datumiZaMesec: Date[] = [];
  mesec: Date;
  danes: Date;
  dovoljen: (x: Date) => boolean;
  hoverDate: Date;

  meseciStr = ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"]

  constructor(
    @Optional() @Inject(MD_DIALOG_DATA) private dialogData: any,
    public dialogRef: MdDialogRef<DatumDialogComponent>) {
    this.izbran = dialogData.izbran;
    this.izbran.setHours(12, 0, 0, 0);
    this.danes = dialogData.danes;
    this.dovoljen = dialogData.dovoljen;
    this.izracunajDatume();
  }

  ngOnInit() {
  }

  izracunajDatume() {
    let datum = new Date(this.izbran);
    datum.setDate(1);
    this.mesec = new Date(datum);
    datum.setDate(datum.getDate() - (datum.getDay() + 6) % 7);
    for (let i = 0; i < 42; i++) {
      let d = new Date(datum);
      d.setDate(datum.getDate() + i);
      if (d.getMonth() > this.mesec.getMonth() && d.getDay() == 1) {
        this.datumiZaMesec = this.datumiZaMesec.slice(0, i);
        break;
      }
      this.datumiZaMesec[i] = d;
    }
    this.datumiZaMesec = this.datumiZaMesec.slice();
  }

  plusMesec() {
    this.izbran.setMonth(this.izbran.getMonth() + 1);
    this.izracunajDatume();
  }
  minusMesec() {
    this.izbran.setMonth(this.izbran.getMonth() - 1);
    this.izracunajDatume();
  }
  plusLeto() {
    this.izbran.setFullYear(this.izbran.getFullYear() + 1);
    this.izracunajDatume();
  }
  minusLeto() {
    this.izbran.setFullYear(this.izbran.getFullYear() - 1);
    this.izracunajDatume();
  }

  mouseenter(d) {
    this.hoverDate = d;
  }
  mouseleave(d) {
    this.hoverDate = undefined;
  }

  izbranDatum(d) {
    if (this.dovoljen(d)) {
      this.izbran = d;
      this.dialogRef.close(d);
    }
  }

}

