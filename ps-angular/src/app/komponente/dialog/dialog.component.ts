import { Component, OnInit, Optional, Inject, Injectable } from '@angular/core';
import { MdDialogConfig, MdDialogRef, MD_DIALOG_DATA, MdDialog } from "@angular/material";


@Injectable()
export class Dialog {
  constructor(public dialog: MdDialog, ) { }

  public odpri(sporocilo: string, izven?: () => any, ...gumbi: [string, undefined | (() => any)][]) {

    let config = new MdDialogConfig();
    config.data = {
      'sporocilo': sporocilo,
      'izven': izven,
      'gumbi': gumbi
    }
    let dialogRef = this.dialog.open(DialogWindowComponent, config);
    return dialogRef.afterClosed().subscribe(r => {
      if (r === undefined && izven !== undefined) {
        izven();
      }
    });
  }
}


@Component({
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogWindowComponent implements OnInit {

  sporocilo: string;
  izven: () => any;
  gumbi: [string, undefined | (() => any)][];


  constructor(
    @Optional() @Inject(MD_DIALOG_DATA) private dialogData: any,
    public dialogRef: MdDialogRef<DialogWindowComponent>) {
    this.sporocilo = dialogData.sporocilo;
    this.izven = dialogData.izven;
    this.gumbi = dialogData.gumbi;
  }

  gumb(akcija?: () => any) {
    if (akcija !== undefined) {
      akcija();
    }
    this.dialogRef.close(true);
  }

  ngOnInit() {
  }
}


