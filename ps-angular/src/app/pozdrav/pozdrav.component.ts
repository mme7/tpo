import { Component, OnInit } from '@angular/core';
import { Avtentikacija} from '../avtentikacija/avtentikacija'

import {Uporabnik} from '../entitete'

@Component({
  selector: 'ps-pozdrav',
  templateUrl: './pozdrav.component.html',
  styleUrls: ['./pozdrav.component.css']
})
export class PozdravComponent implements OnInit {

  vlogax = this.avtentikacija.vlogaString(this.avtentikacija.uporabnik);
  constructor(public avtentikacija:Avtentikacija) { 
    //avtentikacija.vloge.subscribe(vs=>console.log(vs));

  }

  ngOnInit() { }

}
