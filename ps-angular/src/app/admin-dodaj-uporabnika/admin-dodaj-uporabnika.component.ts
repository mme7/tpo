import { Component, OnInit } from '@angular/core';
import { Vloga, Izvajalec, Kraj, Okolis } from "../entitete";

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Observable } from 'rxjs';

import { Http } from '@angular/http'
import { Avtentikacija } from '../avtentikacija/avtentikacija';
import { Skupno } from "app/skupno/skupno";

import { Dialog } from "app/komponente/dialog/dialog.component";
import { Router } from "@angular/router";

//import 'rxjs/add/operator/startWith';

@Component({
  selector: 'ps-admin-dodaj-uporabnika',
  templateUrl: './admin-dodaj-uporabnika.component.html',
  styleUrls: ['./admin-dodaj-uporabnika.component.css']
})
export class AdminDodajUporabnikaComponent implements OnInit {
  vloge: Vloga[];
  izvajalci: Izvajalec[];

  okolisControl = new FormControl(null, [Validators.required, this.preveriNastavljenostObjekta]);
  krajControl = new FormControl(null, [Validators.required, this.preveriNastavljenostObjekta]);


  public loginForm = new FormGroup({
    vloga: new FormControl(null, Validators.required),
    izvajalec: new FormControl(null, Validators.required),
    ime: new FormControl(null, Validators.required),
    priimek: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email], this.preveriZasedenostEmaila.bind(this)),
    telefon: new FormControl(null, Validators.required),
    geslo: new FormControl(null, [Validators.required, this.preveriUjemanje.bind(this), this.preveriStrukturoGesla.bind(this)]),
    geslo2: new FormControl(null, [Validators.required, this.preveriUjemanje.bind(this)])
  });



  public izbranaVloga: number = -1;


  constructor(
    private http: Http,
    private avtentikacija: Avtentikacija,
    public skupno:Skupno,
    public dialog: Dialog,
    private router: Router) {    
  }

  ngOnInit() {
    this.avtentikacija.vloge.subscribe(vs => {
      this.vloge = vs.filter(v => v.id !== 0);
    })
    this.avtentikacija.izvajalci.subscribe(is => {
      this.izvajalci = is;
    });

    this.loginForm.get('vloga').valueChanges.subscribe(v => {
      this.izbranaVloga = v;
    });
  }

  public dodaj() {
    console.log(this.okolisControl);

    this.http.get("/api/pridobi_sifro").subscribe((sifra) => {
      console.log(sifra.text);
      let registracija = {
        ime: this.loginForm.get('ime').value,
        priimek: this.loginForm.get('priimek').value,
        geslo: this.loginForm.get('geslo').value,
        email: this.loginForm.get('email').value,
        tel: this.loginForm.get('telefon').value,
        sifraIzvajalec: this.loginForm.get('izvajalec').value,
        sifraZaposleni: sifra.text(),
        vloga: this.loginForm.get('vloga').value,

        okolis: (this.okolisControl.value as Okolis) !== null ? (this.okolisControl.value as Okolis).id: null,
      }
      this.http.post("/api/kreiraj_racun", JSON.stringify(registracija)).subscribe(r => {
      if (r.status === 200) {
        this.dialog.odpri("Uspešno ustvaril uporabnika", undefined,
          ["V redu", () => {
            this.router.navigate(["/"]);
          }]
        );
      } else {
        this.dialog.odpri("Zgodila se je napaka", undefined, ["V redu", undefined]);
      }

    });
    })
  }

  preveriUjemanje(control: FormControl): { [s: string]: boolean } {
    if (this.loginForm !== undefined && control.value !== this.loginForm.get('geslo').value) {
      return { 'nevjemanje': true };
    }
    return null;
  }

  preveriStrukturoGesla(control: FormControl): { [s: string]: boolean } {
    let g = control.value as string;
    if (g === null) return null;

    if (g.length < 8) {
      return { 'premajhnogeslo': true };
    } else if (g.split("").find(c => Number.isInteger(+c)) === undefined) {
      return { 'nistevilke': true };
    }
    return null;
  }

  preveriZasedenostEmaila(control: FormControl): Promise<any> | Observable<any> {
    return this.http.post("/api/email_prost", control.value)
      .map(x =>
        x.text() as string
      )
      .do(console.log)
      .map(x => {
        if (x === "true") {
          return null;
        } else {
          return { 'zaseden': true };
        }
      });
  }

  preveriNastavljenostObjekta(control: FormControl): { [s: string]: boolean } {
    let p = control.value
    if (typeof p === 'object' && p !== null && p !== undefined) {
      return null;
    } else {
      return { 'niobjekta': true }
    }
  }


}
