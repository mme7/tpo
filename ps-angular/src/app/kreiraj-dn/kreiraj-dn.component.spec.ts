import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KreirajDnComponent } from './kreiraj-dn.component';

describe('KreirajDnComponent', () => {
  let component: KreirajDnComponent;
  let fixture: ComponentFixture<KreirajDnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KreirajDnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KreirajDnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
