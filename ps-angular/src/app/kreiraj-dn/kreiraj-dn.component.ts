import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { Avtentikacija } from '../avtentikacija/avtentikacija';
import { Http } from '@angular/http';
import { NgForm, FormControl, NgControl } from '@angular/forms';
import { Vrstaobiska, Pacient, Akter, Materialdn } from '../entitete';

import { Observable } from 'rxjs';
import { IMyOptions } from "mydatepicker";
import { Skupno } from "app/skupno/skupno";
import { Autocomplete2Component } from "app/komponente/autocomplete2/autocomplete2.component";
import { Dialog } from "app/komponente/dialog/dialog.component";
import { Router } from "@angular/router";

@Component({
  selector: 'ps-kreiraj-dn',
  templateUrl: './kreiraj-dn.component.html',
  styleUrls: ['./kreiraj-dn.component.scss']
})
export class KreirajDnComponent implements OnInit {
  @ViewChild('f') f: NgForm;

  @ViewChild('pacientControl')
  public pacientControl: NgForm;

  @ViewChild('sestraControl')
  public sestraControl: NgControl;

  @ViewChild('pacientAC')
  public pacientAC: Autocomplete2Component;
  public imekraja;

  public vrsteobiska: Observable<Vrstaobiska[]>;

  public sis;
  public dn = {
    vrstaobiska: undefined as number,
    pacient: undefined,
    otroci: [] as number[],
    zdravila: [] as number[],
    material: [] as Materialdn[],

    prviObisk: undefined as Date,
    obveznostPrvega: undefined as boolean,
    steviloObiskov: undefined as number,
    nacinPorazdelitve: 0,
    interval: undefined as number,
    zadnjiObisk: undefined as Date,
    izdajatelj: 0,
    sestra: undefined as number
  };


  otroci: Akter[] = []
  sestre: any[];


  constructor(public http: Http, public skupno: Skupno, public dialog: Dialog, private router: Router) {
    skupno.avtentikacija.pridobiUporabnika().then(p => {
      this.dn.izdajatelj = p.id;
      if (p.vloga === 3) {
        this.vrsteobiska = skupno.sifranti.vrsteobiska.map(vs => vs.filter(v => !v.kurativa));
      } else {
        this.vrsteobiska = skupno.sifranti.vrsteobiska;
      }
      console.log(this.dn);
    })
  }


  ngOnInit() {
    console.log(this.f);
    this.pacientControl.valueChanges.subscribe(p => {
      if (p !== undefined) {
        this.http.get(`/api/uporabnik/${p}/otroci`).subscribe(o => {
          this.otroci = o.json() as Akter[];
        });
        //ce bi naravnost iz templata to klical bi se zacelo ciklati, ke bi funkcija vrnila vedno nov Observable
        this.imekraja = this.skupno.sifranti.pridobiKraj(this.pacientAC.objekt.oseba.postna);
        this.http.get(`/api/uporabnik/${this.pacientAC.objekt.oseba.id}/sestre`).subscribe(s => {
          this.sestre = s.json();
          if (this.sestre.length >= 1) {
            this.dn.sestra = this.sestre[0].id;
            this.sestraControl.control.setValue(this.dn.sestra); //why?? 

          } else {
            this.dn.sestra = undefined;
            this.sestraControl.control.setValue(undefined);
          }
        })
      }
    });
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  dodajmaterial() {
    this.dn.material.push(new Materialdn());
  }

  dodaj() {
    this.http.post("/api/kreiraj_dn", JSON.stringify(this.dn)).subscribe(r => {
      if (r.status === 200) {
        this.dialog.odpri("Dodal DN", undefined,
          ["V redu", () => {
            this.router.navigate(["/"]);
          }]
        );
      } else {
        this.dialog.odpri("Zgodila se je napaka", undefined, ["V redu", undefined]);
      }

    });
  }




  steviloObiskovValidator() {
    if (this.dn.steviloObiskov < 1 || this.dn.steviloObiskov > 10) {
      return { "steviloobiskov": true };
    }
    return null;
  }

  prviDatum(d: Date) {
    let n = new Date();
    n.setHours(12, 0, 0, 0);
    d.setHours(12, 0, 0, 0);
    return d.getTime() >= n.getTime();
  }

  drugiDatum() {
    let n = new Date();
    n.setHours(12, 0, 0, 0);
    if (this.dn.zadnjiObisk === undefined) {
      return "prazno"
    }
    if (this.dn.zadnjiObisk.getTime() <= n.getTime()) {
      return "pred danes"
    }

    if (this.dn.zadnjiObisk.getTime() <= this.dn.prviObisk.getTime()) {
      return "prvi po zadnji"
    }
  }

}


/*
 Zdravnik ali vodja PS lahko kreira delovni nalog. Zdravnik lahko kreira delovni nalog za vse vrste obiskov, vodja patronažne
  službe pa samo za preventivne obiske. Izmed preventivnih obiskov obravnavamo samo obisk nosečnice, obisk otročnice in novorojenčka
  ter preventivo starostnika. Izmed kurativnih obiskov obravnavamo aplikacijo injekcij, odvzem krvi in kontrolo zdravstvenega
  stanja. Pri obisku otročnice in novorojenčka je treba vnesti imena vseh pacientov (mame in otrok), tako da je na isti delovni
  nalog (v splošnem) lahko vezanih več pacientov. Pri aplik
  iji injekcij je treba iz šifranta zdravil izbrati še ustrezna
  zdravila, ki ga jih treba injicirati. Pri odvzemu krvi pa je potrebno določiti barvo (rdeča, modra, rumena, zelena) in
  število epruvet. Vsak delovni nalog se nanaša samo na eno vrsto obiskov. Za vsak delovni nalog je treba navesti datum prvega
  obiska, število obiskov in (časovni interval med dvema zaporednima obiskoma ali časovno obdobje, v katerem morajo biti
  ti obiski opravljeni). Pri datumu prvega obiska naj bo moč izbrati, ali je datum obvezen ali samo okviren. Ko zdravnik/vodja
  PS potrdi pravilnost vnesenih podatkov, se v PB avtomatsko kreirajo zapisi za vse obiske in vzpostavijo povezave z ustrezno
  patronažno sestro. Dodelitev delovnega naloga MS poteka na podlagi šifre okoliša, ki je eden izmed atributov v pacientovem
  profilu. Vsak okoliš pokriva ena patronažna sestra. Preveri regularen postopek za zdravnika (zdravnik lahko kreira delovni
  nalog za vse vrste obiskov). Preveri regularen postopek za vodjo patronažne službe (vodja PS lahko kreira delovne naloge
  samo za preventivne obiske). Preveri kreiranje delovnega naloga za aplikacijo injekcij (vnesti je treba zdravila, ki jih
  je treba injicirati). Preveri kreiranje delovnega naloga za odvzem krvi (vnesti je treba barvo in število epruvet). Preveri
  s praznim datumom prvega obiska (datum prvega obiska je obvezen podatek). Preveri z datumom prvega obiska, ki je manjši
  od trenutnega (datum mora biti večji ali enak trenutnemu). Preveri, da je pri datumu prvega obiska izbrana ena izmed možnosti
  (obvezen, okviren). Preveri s praznim podatkom o številu obiskov (število obiskov je obvezen podatek). Preveri s previsoko
  vrednostjo za število obiskov (maksimalno 10). Preveri kombinacijo podatkov »časovni interval med dvema obiskoma« in »časovno
  obdobje, v katerem morajo biti obiski opravljeni« (Izpolnjen mora biti samo eden od obeh podatkov, drugi se potem izračuna).
  Preveri, da se v podatkovni bazi kreirajo zapisi za posmezne obiske s pravilno izračunanimi predvidenimi datumi1 in indikatorji
  obvezen/okvirni. Upoštevati je treba sobote in nedelje. Preveri za eno medicinsko sestro v okolišu (delovni nalog se avtomatsko
  dodeli tej MS). 1 Razlikovati je treba 3 datume: predvideni datum obiska (izračuna se ob kreiranju delovnega naloga), planirani
  datum obiska (vzpostavi se ob izdelavi plana obiskov za določen dan; lahko se spremeni, če MS ne uspe realizirati vseh
  planiranih obiskov) in dejanski datum obiska (je praviloma enak planiranemu, nastavi se ob vnosu poročila o obisku).
*/
