import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Http } from "@angular/http";
import { Vrstaobiska, Pacient, Uporabnik, Akter, Delovninalog, Materialdn } from '../../entitete';

@Component({
  selector: 'ps-zgodba11-izpis-podatko-obiska',
  templateUrl: './zgodba11-izpis-podatko-obiska.component.html',
  styleUrls: ['./zgodba11-izpis-podatko-obiska.component.css']
})
export class Zgodba11IzpisPodatkoObiskaComponent implements OnInit {
  public oid;
  public dn;
  public podatki;
  public podatkiPrvi;
  public stv;
  public stevilkaObiska;

  public predvideni;
  public planirani;
  public dejanski;

  public sestra
  public nadomestnasestra;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: Http
  ) {

  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      // inicializacija forme (dopolnitev input fieldov s spolsnimi podatki obiska ter
      // podatki prejsnjih obiskov)
      this.oid = params['id'];
      this.http.get(`/api/obisk/${this.oid}`).subscribe(x => {
        let o = x.json();
        if (o.podatki) {
          this.podatki = JSON.parse(o.podatki);
        } else {
          this.podatki = undefined;
        }
        if (o.podatkiPrvi) {
          this.podatkiPrvi = JSON.parse(o.podatkiPrvi);
        } else {
          this.podatkiPrvi = undefined;
        }
        this.dn = o.dn;
        this.stevilkaObiska = o.stevilkaObiska;
        this.predvideni = o.predvideni;
        this.planirani = o.planirani;
        this.sestra = o.sestra;
        this.nadomestnasestra = o.nadomestnasestra;


      })
    });
  }

}
