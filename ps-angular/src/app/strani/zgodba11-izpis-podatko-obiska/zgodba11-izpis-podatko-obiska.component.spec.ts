import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Zgodba11IzpisPodatkoObiskaComponent } from './zgodba11-izpis-podatko-obiska.component';

describe('Zgodba11IzpisPodatkoObiskaComponent', () => {
  let component: Zgodba11IzpisPodatkoObiskaComponent;
  let fixture: ComponentFixture<Zgodba11IzpisPodatkoObiskaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Zgodba11IzpisPodatkoObiskaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Zgodba11IzpisPodatkoObiskaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
