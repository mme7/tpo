import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Http } from "@angular/http";

@Component({
  selector: 'ps-zgodba7-izpis-dn',
  templateUrl: './zgodba7-izpis-dn.component.html',
  styleUrls: ['./zgodba7-izpis-dn.component.css']
})
export class Zgodba7IzpisDNComponent implements OnInit {
  public dn;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: Http,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let id: number = params['id'];
      this.http.get(`/api/dn/${id}`).subscribe(x => {
        let dnData = x.json();

        dnData.datumizdaje = new Date(Date.parse(dnData.datumizdaje)).toLocaleDateString("sl");

        // premapiramo vse datume ter booleane
        console.log(dnData);
        (dnData.obiski as Array<Prikaz>).forEach(d => {
          d.predvideni = new Date(Date.parse(d.predvideni)).toLocaleDateString("sl");
          d.planirani = d.planirani ? new Date(Date.parse(d.planirani)).toLocaleDateString("sl") : "ni še planiran";
          d.dejanski = d.dejanski ? new Date(Date.parse(d.dejanski)).toLocaleDateString("sl") : "ni še opravljen";
          d.nadomestnasestra = d.nadomestnasestra ? d.nadomestnasestra : "/";
          d.obvezen = d.obvezen ? "da" : "ne";
        });

        this.dn = dnData;
      })
    });
  }

  obiskIzbran(id) {
    this.router.navigate(["/", "obisk", id]);
  }

}

class Prikaz {
  predvideni: string;
  planirani: string
  dejanski: string;
  sestra: string;
  nadomestnasestra: string;
  obvezen: string;
}
