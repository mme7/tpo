import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Zgodba7IzpisDNComponent } from './zgodba7-izpis-dn.component';

describe('Zgodba7IzpisDNComponent', () => {
  let component: Zgodba7IzpisDNComponent;
  let fixture: ComponentFixture<Zgodba7IzpisDNComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Zgodba7IzpisDNComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Zgodba7IzpisDNComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
