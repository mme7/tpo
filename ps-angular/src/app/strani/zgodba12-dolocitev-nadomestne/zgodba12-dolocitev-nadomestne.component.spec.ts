import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Zgodba12DolocitevNadomestneComponent } from './zgodba12-dolocitev-nadomestne.component';

describe('Zgodba12DolocitevNadomestneComponent', () => {
  let component: Zgodba12DolocitevNadomestneComponent;
  let fixture: ComponentFixture<Zgodba12DolocitevNadomestneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Zgodba12DolocitevNadomestneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Zgodba12DolocitevNadomestneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
