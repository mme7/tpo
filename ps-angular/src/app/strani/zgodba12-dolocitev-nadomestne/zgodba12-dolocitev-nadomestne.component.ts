import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http";
import { Dialog } from "app/komponente/dialog/dialog.component";

@Component({
  selector: 'ps-zgodba12-dolocitev-nadomestne',
  templateUrl: './zgodba12-dolocitev-nadomestne.component.html',
  styleUrls: ['./zgodba12-dolocitev-nadomestne.component.css']
})

export class Zgodba12DolocitevNadomestneComponent implements OnInit {
  // seznami
  public sestre;
  public nadomestne;
  public napaka = "";
  public obiski: Array<ObiskiVrstica> = [];


  // spremenljivke za stanje
  public obiskiOk = false;
  public formaOk = false;

  constructor(
    public http: Http,
    public dialog: Dialog
  ) { }

  public nadomescanje = {
    zacetekCasovnega: undefined as Date,
    konecCasovnega: undefined as Date,
    medicinskaSestra: undefined as number,
    nadomestnaSestra: undefined as number
  };

  ngOnInit() {
    // 1. Pridobi seznam vseh sester
    this.http.get('/api/vrni_vse_sestre').subscribe(x => {
      // 2. Pridobljen seznam vseh sester postavi v en select
      this.sestre = x.json();
      //this.nadomestne = x.json();
    })
  }

  // 3. Izberi eno sestro iz levega in refreshaj desnega, brez sestre izbrane na levi
  zapolniDrugiSelect() {
    let nadomestneSestre = [];
    for (let i = 0; i < this.sestre.length; i++) {
      if (this.sestre[i].id !== this.nadomescanje.medicinskaSestra) {
        nadomestneSestre.push(this.sestre[i]);
      }
    }
    this.nadomestne = nadomestneSestre;
  }

  // 4. Pridobi izbrano sestro na desni in shrani
  preveriDatum() {
    var danes = new Date();
    danes.setDate(danes.getDate() - 1);
    // 4.1. Prvo pogledamo ce je zacetni datum vsaj danasnji
    if (this.nadomescanje.zacetekCasovnega > danes) {
      // zgodila se je napaka
      if (this.nadomescanje.zacetekCasovnega > this.nadomescanje.konecCasovnega) {
        this.napaka = "Končni datum je pred začetnim";
      }
      else {
        // nalozimo seznam obiskov
        this.http.post("/api/vrni_obiske_sestre_obdobja", this.nadomescanje).subscribe(x => {
          if (x.status === 200) {
            let obiski = x.json();
            (obiski as Array<ObiskiVrstica>).forEach(d => {
              d.predvideni = new Date(Date.parse(d.predvideni)).toLocaleDateString("sl");
              d.planirani = d.planirani ? new Date(Date.parse(d.planirani)).toLocaleDateString("sl") : "ni še planiran";
              d.nadomestnasestra = d.nadomestnasestra ? d.nadomestnasestra : "/";
              d.obvezen = d.obvezen ? "da" : "ne";
            });
            this.obiski = obiski;

            if (this.obiski.length > 0) {
              // preverimo, ce je kaksen obisk zaseden
              if (this.obiski.some(x => x.zaseden)) {
                this.napaka = "V danem območju je že zaseden obisk";
              }
              else {
                // koncno vsi pogoji zadosceni
                this.napaka = "";
                this.obiskiOk = true;
                return;
              }
            }
            else {
              this.napaka = "Mora obstajati vsaj en obisk";
            }
          }
        });
      }
      this.obiskiOk = false;
    }
    else {
      this.napaka = "Izberi začetni datum, ki bo vsaj današnji ali poznejši";
    }
  }

  poslji() {
    this.http.post('/api/doloci_nadomescanje', this.nadomescanje).toPromise().then(x => {
      if (x.status === 200) {
        this.dialog.odpri("Uspešno zabeležil nadomeščanje", undefined,
          ["V redu", () => {
            // najbrz posodobi spet sezname
          }]
        );
        //this.nadomestne = x.json();
      }
    }).catch(x => {
        this.dialog.odpri("Napaka pri beleženju nadomeščanja", undefined,
          ["V redu", () => {
            // najbrz posodobi spet sezname
          }]
        );
    });
  }
}
// pomozni razred
class ObiskiVrstica {
  predvideni: string;
  planirani: string;
  zaseden: boolean;
  nadomestnasestra: string;
  obvezen: string;
}