import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MyDatePickerModule } from 'mydatepicker';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AppComponent } from './app.component';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminDodajUporabnikaComponent } from './admin-dodaj-uporabnika/admin-dodaj-uporabnika.component';
import { RouterModule, Routes } from '@angular/router';
import { PozdravComponent } from './pozdrav/pozdrav.component';
import { LoginComponent } from './avtentikacija/login/login.component';
import { OsebaFormComponent } from './avtentikacija/register/oseba-form.component';
import { PacientFormComponent } from './avtentikacija/register/pacient-form.component';
import { RegisterComponent } from './avtentikacija/register/register.component';
import { LogoutComponent } from './avtentikacija/logout/logout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { Avtentikacija } from './avtentikacija/avtentikacija';
import { SeznamUporabnikovComponent } from './seznam-uporabnikov/seznam-uporabnikov.component';
import { KreirajDnComponent } from './kreiraj-dn/kreiraj-dn.component';
import { ZacetnaStran } from './zacetna-stran/zacetna-stran.component';
import { IzpisSeznamDN } from './izpisi/IzpisSeznamaDN/IzpisSeznamaDN.component';
import { Autocomplete2Component } from './komponente/autocomplete2/autocomplete2.component';
import { SelectComponent } from './komponente/select/select.component';
import { AutoselectnadrazredComponent } from './komponente/autoselectnadrazred.component';
import { Sifranti } from './sifranti.service';
import { Skupno } from './skupno/skupno';
import { ValidacijaDirective } from './skupno/validacija.directive';
import { DatumComponent } from './komponente/datum/datum.component';
import { DatumDialogComponent } from './komponente/datum/datum-dialog/datum-dialog.component';
import { RegisterStatusDialog } from './avtentikacija/register/register-status-dialog.component';
import { CustomEditorComponent } from './izpisi/custom-editor.component';
import { CustomRenderComponent } from './izpisi/custom-render.component';
import { ButtonViewComponent } from './izpisi/IzpisSeznamaDN/IzpisSeznamaDN.component'
import { PlaniranjeObiskov } from './obiski/planiranjeObiskov/PlaniranjeObiskov.component';
import { SeznamObiskov } from './obiski/seznamObiskov/SeznamObiskov.component';
import { ButtonViewComponent2 } from './obiski/seznamObiskov/SeznamObiskov.component';
import { Meritve } from './obiski/meritve/meritve.component';
import { Dialog, DialogWindowComponent } from './komponente/dialog/dialog.component';
import { Zgodba7IzpisDNComponent } from './strani/zgodba7-izpis-dn/zgodba7-izpis-dn.component';
import { Zgodba11IzpisPodatkoObiskaComponent } from './strani/zgodba11-izpis-podatko-obiska/zgodba11-izpis-podatko-obiska.component';
import { Zgodba12DolocitevNadomestneComponent } from './strani/zgodba12-dolocitev-nadomestne/zgodba12-dolocitev-nadomestne.component';
import { NovogesloComponent } from './avtentikacija/novogeslo/novogeslo.component';

const routes: Routes = [
  { path: '', redirectTo: '/pozdrav', pathMatch: 'full' },
  /*{ path: 'dashboard', component: DashboardComponent, children:dashboard_routes }, */
  { path: 'pozdrav', component: ZacetnaStran },
  { path: 'admin/seznam-uporabnikov', component: SeznamUporabnikovComponent },
  { path: 'admin/dodaj-uporabnika', component: AdminDodajUporabnikaComponent },
  { path: 'kreiraj-dn', component: KreirajDnComponent },
  { path: 'login/:email', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'izpisSeznamDN', component: IzpisSeznamDN },
  { path: 'planiranjeObiskov', component: PlaniranjeObiskov },
  { path: 'seznamObiskov', component: SeznamObiskov },
  { path: 'meritve/:id', component: Meritve },
  { path: 'dn/:id', component: Zgodba7IzpisDNComponent },
  { path: 'obisk/:id', component: Zgodba11IzpisPodatkoObiskaComponent },
  { path: 'nadomestneSestre', component: Zgodba12DolocitevNadomestneComponent },
  { path: 'novogeslo', component: NovogesloComponent },
]


@NgModule({
  declarations: [
    AppComponent,
    AdminDodajUporabnikaComponent,
    PozdravComponent,
    LoginComponent,
    PacientFormComponent,
    OsebaFormComponent,
    RegisterComponent,
    RegisterStatusDialog,
    LogoutComponent,
    SeznamUporabnikovComponent,
    IzpisSeznamDN,
    KreirajDnComponent,
    ZacetnaStran,
    Autocomplete2Component,
    SelectComponent,
    ValidacijaDirective,
    DatumComponent,
    DatumDialogComponent,
    ButtonViewComponent,
    CustomEditorComponent,
    CustomRenderComponent,
    PlaniranjeObiskov,
    SeznamObiskov,
    ButtonViewComponent2,
    Meritve,
    DialogWindowComponent,
    Zgodba7IzpisDNComponent,
    Zgodba11IzpisPodatkoObiskaComponent,
    Zgodba12DolocitevNadomestneComponent,
    NovogesloComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MyDatePickerModule,
    MaterialModule, BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    Ng2SmartTableModule
  ],
  providers: [
    Avtentikacija,
    Sifranti,
    Skupno,
    Dialog
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DatumDialogComponent,
    DialogWindowComponent,
    CustomEditorComponent,
    CustomRenderComponent,
    ButtonViewComponent,
    ButtonViewComponent2,
    RegisterStatusDialog,
  ]
})
export class AppModule { }
