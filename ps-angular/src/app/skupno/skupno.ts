import { Injectable } from "@angular/core";
import { Avtentikacija } from "app/avtentikacija/avtentikacija";
import { Sifranti } from "app/sifranti.service";

@Injectable()
export class Skupno {
    constructor(
        public sifranti: Sifranti,
        public avtentikacija: Avtentikacija
    ) { }

    stejNeprazneElemente(polje: any[]){
        let c = 0;
        polje.forEach(x=>{
            if(c !== null && c !== undefined){
                c++;
            }
        })
        return c;
    }
}