import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, Validators } from '@angular/forms';

@Directive({
  selector: '[validacija][ngControl],[validacija][ngFormControl],[validacija][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidacijaDirective, multi: true }
  ]
})
export class ValidacijaDirective implements Validator {
  private validator = null;
  _validatorji: any;
  @Input('validacija') set validatorji(val) {
    this._validatorji = val;
    if (val instanceof Array) {
      this.validator = Validators.compose(val.map(v => ValidacijaDirective.adapter(v)));
    } else {
      this.validator = ValidacijaDirective.adapter(val);
    }
  }
  get validatorji() {
    return this._validatorji;
  }

  //------ Validator impl
  validate(c: AbstractControl): { [key: string]: any; } {
    return this.validator(c);
  }
  //-----

  constructor() { }

  //če dodamo string ga prevede, če validator vrača string ali undefined prevede v smiselno obliko
  static adapter(validator: any) {
    let val = validator;
    if (typeof validator === 'string') {
      val = eval(validator);
    }
    let ovitValidadotor = (x) => {
      let r = val(x);     
      if (r === null || r === undefined) {
        return null;
      } else if (typeof r === 'string') {
        let nr = new Object();
        nr[r] = true;
        return nr;
      } else if (r === false) {
        let nr = new Object();
        nr["napaka v validaciji"] = true;
        return nr;
      } if (r === true) {       
        return null;
      } else {
        return r;
      }
    }
    return ovitValidadotor;
  }
}
