import { Component, OnInit } from '@angular/core';
import { Uporabnik } from '../entitete';


import { Http, Headers, Response, RequestOptions } from '@angular/http'
import { Avtentikacija } from '../avtentikacija/avtentikacija';
import { Router } from "@angular/router";


@Component({
  selector: 'ps-seznam-uporabnikov',
  templateUrl: './seznam-uporabnikov.component.html',
  styleUrls: ['./seznam-uporabnikov.component.css']
})
export class SeznamUporabnikovComponent implements OnInit {

  public uporabniki: Uporabnik[];

  constructor(
    private http: Http,
    public avtentikacija: Avtentikacija,
    public router: Router) { 
      http.get("/api/admin/uporabniki").toPromise().then(us => {
        this.uporabniki = (us.json() as Uporabnik[]).sort((a,b)=>a.id - b.id);  
      });
  }

  ngOnInit() {
  }

  loginfrm(email){
     this.router.navigate(["/login", email]);
  }

}
