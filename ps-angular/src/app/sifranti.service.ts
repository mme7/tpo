import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { Okolis, Kraj, Vloga, Izvajalec, Vrstaobiska } from './entitete';

export class SorodstvenoRazmerje {
    public idx: number;
    public opis: string;
}

@Injectable()
export class Sifranti {

    constructor(private http: Http) { }

    public sorodstvenaRazmerja = this.http.get("/api/sorodstvenorazmerje")
        .map(response => <SorodstvenoRazmerje[]>response.json())
        .publishLast().refCount();

    public okolisi = this.http.get("/api/okolis")
        .map(response => <Okolis[]>response.json())
        .publishLast().refCount();

    public kraji = this.http.get("/api/kraj")
        .map(response => <Kraj[]>response.json())
        .publishLast().refCount();

    public vloge = this.http
        .get("/api/seznam-vlog")
        .map(vs => vs.json() as Vloga[])
        .publishLast().refCount();

    public izvajalci = this.http
        .get("/api/seznam-izvajalcev")
        .map(is => is.json() as Izvajalec[])
        .publishLast().refCount();

    public vrsteobiska = this.http
        .get("/api/seznam-vrstobiska")
        .map(is => is.json() as Vrstaobiska[])
        .publishLast().refCount();

    public material = this.http
        .get("/api/seznam-materiala")
        .map(is => is.json() as Vrstaobiska[])
        .publishLast().refCount();

    public zdravila = this.http
        .get("/api/seznam-zdravil")
        .map(is => is.json() as Vrstaobiska[])
        .publishLast().refCount();

    public pridobiKraj(postna: number) {       
        return this.kraji.map(ks => ks.find(k => k.postna === postna).imekraja);
    }
}