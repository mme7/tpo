import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Uporabnik, Akter } from '../../entitete';
import { Ng2SmartTableModule, LocalDataSource, ViewCell } from 'ng2-smart-table';
import { ButtonViewComponent } from '../../izpisi/IzpisSeznamaDN/IzpisSeznamaDN.component';
import { ActivatedRoute, Router } from "@angular/router";
import { Avtentikacija } from '../../avtentikacija/avtentikacija';

    // kliknilGumb ~ spremeljivka, ki si zapomni, ce si/nisi kliknil na gumb za izpis
var kliknilGumb = false;

@Component({
  selector: 'seznam-obiskov',
  templateUrl: './SeznamObiskov.component.html',
  styleUrls: ['./SeznamObiskov.component.css']
})

export class SeznamObiskov implements OnInit {
        //  ClickedRowDNID spremenljivka, ki hrani zadnjo vrstico na katero smo kliknili (kjerkoli)
    clickedRowDNID: number;
    gumbzadeva = new ButtonViewComponent();
        // source ~ hranimo DATA za tabelo
    source: LocalDataSource; 
      // filtriraniPodatki spremeljivka, ki hrani filtrirane podatke, ki jih prejmemo iz streznika
    public filtriraniPodatki: PodatkiVrstice[];
      /*========= PODATKI ZA TABELO =========*/
        // settings ~ podatki za stolpce
    settings = {
      actions: false, // odstranimo new/edit/add
      columns: {
        TabIdObiska: { title: 'ID obiska'},
        TabVrstaObiska: { title: 'Vrsta obiska' },
        TabPredvideniDatum: { title: 'Predvideni datum' },
        TabDejanskiDatum: { title: 'Dejanski datum' },
        TabIzdajatelj: { title: 'Izdajatelj' },
        TabZadolzenaSestra: { title: 'Zadolžena sestra' },
        TabNadomestnaSestra: { title: 'Nadomestna sestra' },
        TabKategorija: { title: 'Kategorija obiska' },
        TabPacient: { title: 'Pacient' },
        TabPodrobnosti: {
          title: 'Podrobnosti obiska',
          type: 'custom', 
          renderComponent: ButtonViewComponent2,
        }
      }
    };
    
    data = [ ];
      /*========= KONEC PODATKOV ZA TABELO =========*/
       // podatki za filtre
    public filtri = {
        ObiskPredvideniOD : undefined as Date,
        ObiskPredvideniDO : undefined as Date,
        ObiskDejanskiOD : undefined as Date,
        ObiskDejanskiDO : undefined as Date
    };

    constructor(
        public avtentikacija:Avtentikacija,
        private route: ActivatedRoute,
        private router: Router,
        private http:Http
    ) { 
        this.source = new LocalDataSource(this.data); // create the source
    }
    
    onSubmit(){       
        this.source.empty();
        this.source.refresh();
            // ko imam vse podatke posljem
        this.http.post("/api/filtrirani_obiski", JSON.stringify(this.filtri)).toPromise().then(us => {           
            this.filtriraniPodatki = (us.json() as PodatkiVrstice[]);
            console.log(this.filtriraniPodatki);
            // prejete podatke dodam v tabelo
            this.dodajVTabelo();
        });
    }

    ngOnInit(){}

    private dodajVTabelo(){
        for(let i = 0; i < this.filtriraniPodatki.length; i++)
        {
           //console.log(this.filtriraniPodatki[i].ime);
           this.source.append(this.getNewRow(i));
            // posodobim tabelo (da se prikaze)
           this.source.update;
        }
    }

    // vrne novo vrstico ~ samo za lepso kodo
    getNewRow(n : number){
        return {
            TabIdObiska: this.filtriraniPodatki[n].TabIdObiska,
            TabVrstaObiska: this.filtriraniPodatki[n].TabVrstaObiska,
            TabPredvideniDatum : new Date(Date.parse(this.filtriraniPodatki[n].TabPredvideniDatum)).toLocaleDateString("sl"),
            TabDejanskiDatum : this.filtriraniPodatki[n].TabDejanskiDatum === undefined ? "obisk ni opravljen" : new Date(Date.parse(this.filtriraniPodatki[n].TabDejanskiDatum)).toLocaleDateString("sl"),
            TabIzdajatelj : this.filtriraniPodatki[n].TabIzdajatelj,
            TabZadolzenaSestra : this.filtriraniPodatki[n].TabZadolzenaSestra,
            TabNadomestnaSestra : this.filtriraniPodatki[n].TabNadomestnaSestra,
            TabKategorija : this.filtriraniPodatki[n].TabKategorija,
            TabPacient : this.filtriraniPodatki[n].TabPacient,
            TabPodrobnosti: 'Odpri obisk #'+this.filtriraniPodatki[n].TabIdObiska,
        }
    }
    // Ko kliknimo nekam na vrstico, pogledamo, ce smo kliknili gumb.. ce smo, napolnimo podatke od izpisu
    onUserRowSelect(event) : void{
        this.clickedRowDNID = event.data.TabIdObiska;
        console.log("NEKI "+this.avtentikacija.uporabnik.vloga);
            // ce smo kliknili na gumb, zapolnimo izpis
        if(kliknilGumb == true)
        {
            kliknilGumb = false;
            this.zapolniIzpisObiska(this.clickedRowDNID);
        }
    }
    zapolniIzpisObiska(id){
        console.log("Poskusam it do: "+id);
        this.router.navigate(["/", "obisk", id]);
    } 
}

class PodatkiVrstice {
    TabIdObiska: number;
    TabVrstaObiska: number;
    TabPredvideniDatum : string;
    TabDejanskiDatum : string;
    TabIzdajatelj : string;
    TabZadolzenaSestra : string;
    TabNadomestnaSestra : string;
    TabKategorija : string;
    TabPacient : string;
}

    /* KOMPONENTA ZA GUMB --- ne odstrani!!!!*/
@Component({
  selector: 'button-view',
  template: `
    <button (click)="onClick()">{{ renderValue }}</button>
  `,
})

export class ButtonViewComponent2 implements ViewCell, OnInit {
  renderValue: string;
  kliknil: boolean;
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  onClick() {
    this.save.emit(this.rowData);
    console.log(this.value);
        // shrani da sem kliknil na gumb
    kliknilGumb = true;
    this.kliknil = kliknilGumb;
  }
}
