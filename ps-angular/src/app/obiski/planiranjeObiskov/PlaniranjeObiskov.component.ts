import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Ng2SmartTableModule, LocalDataSource, ViewCell } from 'ng2-smart-table';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Uporabnik, Akter, Delovninalog } from '../../entitete';
import { ActivatedRoute, Router } from "@angular/router";
import { Skupno } from "app/skupno/skupno";

@Component({
  selector: 'planiranje-obiskov',
  templateUrl: './PlaniranjeObiskov.component.html',
  styleUrls: ['./PlaniranjeObiskov.component.css']
})

export class PlaniranjeObiskov implements OnInit {


  public datum = new Date();

  //obvezni = [
  //  new Obisk(1, 'obisk novorojenčka', new Date(2017, 1, 4), new Date(2017, 1, 4), 'Dr Dre', 'Ana', 'Marko', 'Davor'),
  //];

  // neopravljeni = [
  //   new Obisk(13, 'obisk novorojenčka', new Date(2017, 1, 4), new Date(2017, 1, 4), 'Dr Dre', 'Ana', 'Marko', 'Davor'),
  //   new Obisk(14, 'obisk novorojenčka', new Date(2017, 1, 4), new Date(2017, 1, 4), 'Dr Dre', 'Ana', 'Marko', 'Davor'),
  // ];

  // clickedRowDNID: number;

  // source ~ hranimo DATA za tabelo
  // source: LocalDataSource; 

  // settings = {
  //   actions: false, // odstranimo new/edit/add
  //   columns: {
  //     idObiska: { title: 'ID obiska'},
  //     vrstaObiska: { title: 'Vrsta obiska' },
  //     predvideniDatum: { title: 'Predvideni datum' },
  //     dejanskiDatum: { title: 'Dejanski datum' },
  //     izdajatelj: { title: 'Izdajatelj' },
  //     zadolzenaSestra: { title: 'Zadolžena sestra' },
  //     nadomestnaSestra: { title: 'Nadomestna sestra' },
  //     pacient: { title: 'Pacient' },
  //     podrobnosti: { title: 'Podrobnosti DN' },
  //   }
  // };

  // data = [
  //   {
  //     idObiska: 1,
  //     vrstaObiska: "Obisk nosečnice",
  //     predvideniDatum: "12.5.2017",
  //     dejanskiDatum: "17.5.2017",
  //     izdajatelj: "Dr Dre",
  //     zadolzenaSestra: "Nina",
  //     nadomestnaSestra: "Zala",
  //     pacient: "Davor",
  //     podrobnosti: 'Prikaži podrobnosti',
  //   }
  // ];

  // //data = [];

  // public filtri = {
  //     IzbraniDatum : undefined as Date
  // };

  public neuvrsceniObiski: any[];
  public planiraniObiskiDneva: any[];
  public planiraniObiskiVsi: any[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: Http,
    private skupno: Skupno
  ) {

  }

  // posodobi vse panele za obiske
  posodobiPodatke() {
    this.prikaziNeuvrsceneObiske();
    this.prikaziPlaniraneObiske();
    this.prikaziVsePlaniraneObiske();
  }

  ngOnInit() {
    this.posodobiPodatke();
  }

  // to je za prikaz planiranih obiskov posameznega dneva
  prikaziPlaniraneObiske() {
    let plan = new Date(this.datum).setHours(0, 0, 0, 0);
    let danes = new Date().setHours(0, 0, 0, 0);

    this.http.get(`/api/obisk/planirani_obiski/${this.datum.getFullYear()}-${this.datum.getMonth() + 1}-${this.datum.getDate()}`).subscribe(x => {
      this.planiraniObiskiDneva = x.json() as ObiskZg8[];
    });
  }

  prikaziNeuvrsceneObiske() {
    this.http.get('/api/obisk/neuvrsceni_obiski').subscribe(x => {
      this.neuvrsceniObiski = x.json() as ObiskZg8[];
    });
  }

  prikaziVsePlaniraneObiske() {
    this.http.get('/api/obisk/planirani_obiski').subscribe(x => {
      this.planiraniObiskiVsi = x.json() as ObiskZg8[];
    });
  }

  dodajVPlan(o: ObiskZg8) {
    let danes = new Date().setHours(0, 0, 0, 0);
    let datum = new Date(this.datum.getTime()).setHours(0, 0, 0, 0);
    if (datum < danes) {
      return;
    }

    // api klic za dodajanje v plan
    this.http.get(`/api/obisk/dodajvplan/${this.datum.getFullYear()}-${this.datum.getMonth() + 1}-${this.datum.getDate()}/${o.id}`)
      .subscribe(x => {
        this.posodobiPodatke();
      });
  }

  odstrani(o: ObiskZg8) {
    let danes = new Date().setHours(0, 0, 0, 0);
    let datum = new Date(this.datum.getTime()).setHours(0, 0, 0, 0);
    if (datum < danes) {
      return;
    }
    
    this.http.get(`/api/obisk/odstraniizplana/${o.id}`)
      .subscribe(x => {
        this.posodobiPodatke();
      });
  }

  // prikaze formo za pregled meritev
  obisci(o: ObiskZg8) {
    this.router.navigate(["/", "obisk", o.id]);
  }

  // prikaze formo za vnos podatkov o obisku
  meritve(o: ObiskZg8) {
    this.router.navigate(["/", "meritve", o.id]);
  }

  // prikaze podrobnosti delovnega naloga
  dnx(o: ObiskZg8) {
    this.router.navigate(["/", "dn", o.did]);
  }
}


class ObiskZg8 {
  public id;
  public predvideni;
  public planirani;
  public dejanski;
  public sestra;
  public nadomestnasestra;
  public obvezen;
  public pacienti;
  public did;
}