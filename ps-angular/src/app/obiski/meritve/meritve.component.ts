import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule, FormBuilder, NgForm } from '@angular/forms';
import { Vrstaobiska, Pacient, Uporabnik, Akter, Delovninalog, Materialdn } from '../../entitete';
import { ActivatedRoute, Router } from "@angular/router";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Dialog } from "app/komponente/dialog/dialog.component";

@Component({
  selector: 'meritve',
  templateUrl: './meritve.component.html',
  styleUrls: ['./meritve.component.css']
})
export class Meritve implements OnInit {

  private readonly _MS_PER_DAY = 1000 * 60 * 60 * 24;

  _f: NgForm;
  @ViewChild('f') set f(val: NgForm) {
    this._f = val;
    if (this._f) {
      this._f.valueChanges.subscribe(x => {
        this.razlike();
      })
    }
  };

  get f(): NgForm {
    return this._f;
  }

  public oid;
  public dn;
  public podatki;
  public podatkiPrvi;
  public stv;
  public stevilkaObiska;

  public predvideni;
  public planirani;
  public dejanski;

  public sestra
  public nadomestnasestra;

  // kontrolne spremenljivke, ki dovoljujejo urejanje glede na dan
  public mozenVnos: boolean;
  // v primeru, da je danasnji datum za predvidenim, je prvo potrebno potrditi, predno se lahko
  // formo editira
  public opozoriloDatumGumb: boolean;
  public opozoriloDatumOpis: string = "nekaj";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: Http,
    public dialog: Dialog
  ) {

  }

  razlike() {
    if (this.dn && this.dn.otroci) {
      let otroci: any[] = this.dn.otroci;

      otroci.forEach(otrok => {
        if (!this.f || !this.f.value) {
          return;
        }
        let dv: number;
        let dt: number;
        if (this.stevilkaObiska === 1) {
          dt = this.f.value["o" + otrok.id + "_30_60"] - this.f.value["o" + otrok.id + "_20_10_porodnaTeza"]
          dv = this.f.value["o" + otrok.id + "_30_70"] - this.f.value["o" + otrok.id + "_20_10_porodnaVisina"]

        } else {
          dt = this.f.value["o" + otrok.id + "_30_60"] - this.podatkiPrvi["o" + otrok.id + "_20_10_porodnaTeza"]
          dv = this.f.value["o" + otrok.id + "_30_70"] - this.podatkiPrvi["o" + otrok.id + "_20_10_porodnaVisina"]
        }
        if (dv > 0) {
          otrok.dv = dv;
        } else {
          otrok.dv = "/";
        }

        if (dt > 0) {
          otrok.dt = dt;
        } else {
          otrok.dt = "/";
        }
      })
    }
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      // inicializacija forme (dopolnitev input fieldov s spolsnimi podatki obiska ter
      // podatki prejsnjih obiskov)
      this.oid = params['id'];
      this.http.get(`/api/obisk/${this.oid}`).subscribe(x => {
        let o = x.json();
        if (o.podatki) {
          this.podatki = JSON.parse(o.podatki);
        } else {
          this.podatki = undefined;
        }
        if (o.podatkiPrvi) {
          this.podatkiPrvi = JSON.parse(o.podatkiPrvi);
        } else {
          this.podatkiPrvi = undefined;
        }
        this.dn = o.dn;
        this.stevilkaObiska = o.stevilkaObiska;
        this.predvideni = o.predvideni;
        this.planirani = o.planirani;
        this.sestra = o.sestra;
        this.nadomestnasestra = o.nadomestnasestra;

        Observable.timer(250).subscribe(_ => {
          this.f.form.reset();
          if (this.podatki) {
            this.f.form.patchValue(this.podatki);
          }
          // ce ta se ni definiran, damo na planirani datum
          if (o.dejanski === undefined)
            this.dejanski = o.planirani;
          else
            this.dejanski = o.dejanski;
        })
        // pregledamo datum
        this.preglejDatum();
      })
    });
  }


  // funkcija za pregled datuma
  preglejDatum() {
    // pregled, ce je formo mogoce sploh se urejati glede na datum
    let de = new Date(Date.now());
    let pl = new Date(this.dejanski);
    let utc1 = Date.UTC(de.getFullYear(), de.getMonth(), de.getDate());
    let utc2 = Date.UTC(pl.getFullYear(), pl.getMonth(), pl.getDate());
    // console.log(new Date(utc1) + " " + de)
    // console.log(new Date(utc2) + " " + pl)

    let dni = Math.floor((utc2 - utc1) / this._MS_PER_DAY);
    // console.log(dni);
    switch (dni) {
      case 0: // all is good, lajf je lep, MS opravlja delo kot mora in v istem dnevu izpolne obrazec
        this.mozenVnos = true;
        this.opozoriloDatumGumb = false;
        this.opozoriloDatumOpis = "";
        break;

      case -1: // MS je lena in zamuja za en dan, pa recimo da ji to povemo v obraz, ona to pohlevno potrdi in se pobotamo
        this.mozenVnos = false;
        this.opozoriloDatumGumb = true;
        this.opozoriloDatumOpis = "Pozen vnos podatkov obiska"
        break;


      default: // MS se kratko malo ne da vpisovati takrat ko bi morala, posledicno jo sistem kaznuje s tem da ji ne dovoli vpisa
        if (dni > 0) {
          this.mozenVnos = false;
          this.opozoriloDatumGumb = false;
          this.opozoriloDatumOpis = "Vnašanje podatkov za pridonost ni dovoljeno."
        } else {
          this.mozenVnos = false;
          this.opozoriloDatumGumb = false;
          this.opozoriloDatumOpis = "Čas za vnos podatkov obiska je potekel."
        }
    }
  }

  potrdiDatum() {
    this.mozenVnos = true;
    this.opozoriloDatumGumb = false;
  }


  // izvede transakcijo
  shrani2() {
    this.http.post(`/api/obisk/${this.oid}/${Date.parse(this.f.value.dejanski)}`, this.f.value).subscribe(x => { });
    this.router.navigate(["/"]);
  }


}
