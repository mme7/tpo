import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {IMyOptions} from 'mydatepicker';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import { Uporabnik, Akter, Delovninalog } from '../../entitete';
import { Skupno } from "app/skupno/skupno";
import { Ng2SmartTableModule, LocalDataSource, ViewCell } from 'ng2-smart-table';
import { Router } from "@angular/router";

    // kliknilGumb ~ spremeljivka, ki si zapomni, ce si/nisi kliknil na gumb za izpis
var kliknilGumb = false;

@Component({
  selector: 'izpis-seznam-dn',
  templateUrl: './IzpisSeznamaDN.component.html',
  styleUrls: ['./IzpisSeznamaDN.component.css']
})

export class IzpisSeznamDN implements OnInit {
        //  ClickedRowDNID spremenljivka, ki hrani zadnjo vrstico na katero smo kliknili (kjerkoli)
    clickedRowDNID: number;
        
        // filtriraniPodatki spremeljivka, ki hrani filtrirane podatke, ki jih prejmemo iz streznika
    public filtriraniPodatki: PodatkiVrstice[];
        
        // source ~ hranimo DATA za tabelo
    source: LocalDataSource; 

        /*========= PODATKI ZA TABELO =========*/
        // settings ~ podatki za stolpce
    settings = {
        actions: false, // odstranimo new/edit/add
        columns: { 
            TabDelovniNalog: { title: 'ID delovnega naloga' },
            TabImePacient: { title: 'Ime in priimek pacienta' },
            TabCasIzdaje: { title: 'Časovno obdobje izdaje' },
            TabVrstaObiska: { title: 'Vrsta obiska' },
            TabIzdajatelj: { title: 'Izdajatelja' },
            TabZadolzenaPS: { title: 'Zadolžena patronažna sestra' },
            TabNadomestnaPS: { title: 'Nadomestne patronažna sestra' },
            TabOdpriDN: { title: 'Izpis delovnega naloga', type: 'custom', renderComponent: ButtonViewComponent, }
        }
    };
        // PODATKI ZA VRSTICE
    data = [];
        /*========= KONEC PODATKOV ZA TABELO =========*/
        // podatki za filtre
    public filtri = {
        DNObdobjeIzdajeOD : undefined as Date,
        DNObdobjeIzdajeDO : undefined as Date
    };

    constructor(
        private http:Http,
        public router:Router
    ) { 
        this.source = new LocalDataSource(this.data); // create the source
    }
    
    onSubmit(){       
        this.source.empty();
        this.source.refresh();
            // ko imam vse podatke posljem
        this.http.post("/api/filtrirani_delovni_nalog", JSON.stringify(this.filtri)).toPromise().then(us => {           
            this.filtriraniPodatki = (us.json() as PodatkiVrstice[]);
            console.log(this.filtriraniPodatki);
                // prejete podatke dodam v tabelo
            this.dodajVTabelo();
        });
    }
    ngOnInit(){}

    private dodajVTabelo(){
        for(let i = 0; i < this.filtriraniPodatki.length; i++)
        {
           //console.log(this.filtriraniPodatki[i].ime);
           this.source.append(this.getNewRow(i));
            // posodobim tabelo (da se prikaze)
           this.source.update;
        }
    }

    // vrne novo vrstico ~ samo za lepso kodo
    getNewRow(n : number){
        return {
            TabDelovniNalog: this.filtriraniPodatki[n].TabDelovniNalogId,
            TabImePacient: this.filtriraniPodatki[n].TabImePacient,
            TabCasIzdaje: new Date(Date.parse(this.filtriraniPodatki[n].TabCasIzdaje)).toLocaleDateString("sl"),
            TabVrstaObiska: this.filtriraniPodatki[n].TabVrstaObiska,
            TabIzdajatelj: this.filtriraniPodatki[n].TabIzdajatelj,
            TabZadolzenaPS: this.filtriraniPodatki[n].TabZadolzenaPS,
            TabNadomestnaPS: this.filtriraniPodatki[n].TabNadomestnaPS,
            TabOdpriDN: 'Odpri DN #'+this.filtriraniPodatki[n].TabDelovniNalogId,
        }
    }

    // Ko kliknimo nekam na vrstico, pogledamo, ce smo kliknili gumb.. ce smo, napolnimo podatke od izpisu
    onUserRowSelect(event) : void{
        this.clickedRowDNID = event.data.TabDelovniNalog;
            // ce smo kliknili na gumb, zapolnimo izpis
        if(kliknilGumb == true)
        {
            this.zapolniIzpisDN(this.clickedRowDNID);
            kliknilGumb = false;
        }
    }
        
    // SPREMELJIVKE ZA IZPIS DN -- NISO DOKONCNE ~ cakam na zakljucek 5. Zgodbe
    /*
    delovniNalog = [];
    IzpisVrstaObiska: string;
    IzpisImeinPriimekPacienta: string;
    IzpisNaslovPacienta: string;
    IzpisDatumPrvegaObiska: Date;
    IzpisDatumZadnjegaObiska: Date;
    IzpisSteviloObiskov: number;
    IzpisPlaniranje: string;

    specificniPodatki = [
        {
            vrstaPodatka: 'Zdravilo 1',
            opisPodatka: 'Gravibinom'
        },
        {
            vrstaPodatka: 'Zdravilo 2',
            opisPodatka: '2 brci v tazadnjo'
        }
    ];*/

    // to je za izpis posamezne podrobnosti delovnega naloga
    zapolniIzpisDN(id: number) : void {
         this.router.navigate(["/", "dn", id]);


        // // ko pridobis id, poklici delovni nalog in pridobi podatke
        // this.http.post("/api/pridobi_delovni_nalog", JSON.stringify(id)).toPromise().then(us => {           
        //     this.delovniNalog = (us.json() as Akter[]);
        // });
        
        // this.IzpisVrstaObiska = "nekaj";
        // this.IzpisImeinPriimekPacienta = "Janez Novak";
        // this.IzpisNaslovPacienta = "Ulica 12, 1000 Ljubljana";
        //     // date LETO, MESEC+1, DAN
        // this.IzpisDatumPrvegaObiska =  new Date(2017,5,1);
        // this.IzpisDatumZadnjegaObiska = new Date(2017,5,20);
        // this.IzpisSteviloObiskov = 5;
        // this.IzpisPlaniranje = "Interval";
    }

}

class PodatkiVrstice {
    TabDelovniNalogId : number;
    TabImePacient : string;
    // castanje ne preoblikuje niza v objekt Date!
    TabCasIzdaje : string;
    TabVrstaObiska : string;
    TabIzdajatelj : string;
    TabZadolzenaPS : string;
    TabNadomestnaPS : string;
}

    /* KOMPONENTA ZA GUMB --- ne odstrani!!!!*/
@Component({
  selector: 'button-view',
  template: `
    <button (click)="onClick()">{{ renderValue }}</button>
  `,
})
export class ButtonViewComponent implements ViewCell, OnInit {
  renderValue: string;
  kliknil: boolean;
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  onClick() {
    this.save.emit(this.rowData);
    console.log(this.value);
        // shrani da sem kliknil na gumb
    kliknilGumb = true;
    this.kliknil = kliknilGumb;
  }
}