import { Injectable } from '@angular/core';

import { Avtentikacija } from './avtentikacija/avtentikacija';

export class Akter {
	public oseba: Oseba;
    public uporabnik: UporabnikDB;
    public pacient: PacientDB;
    public zaposleni: Zaposleni;
}


export class Storitev {
	public id: number;
	public tipstoritve: number;
	public opis: string;
}

export class Oseba {
	public id: number;
	public ime: string;
	public priimek: string;
	public ulica: string;
	public hisnastevilka: string;
	public postna: number;
	public tel: string;
}

export class Vloga {
	public id: number;
	public ime: string;
}

export class Izvajalec {
	public id: number;
	public naziv: string;
	public ulica: string;
	public hisna: string;
	public postna: number;
	//--------
	public imekraja: string;
}

export class Zdravilo {
	public id: number;
	public naziv: string;
}

export class Delovninalog {
	public id: number;
}

export class Okolis {
	public id: number;
	public ime: string;
}

export class Pacient extends Oseba {
	public id: number;
	public sifrazavarovanja: number;
	public okolis: number;
	public datumrojstva: Date;
	public spol: number;
	public kontaktnaoseba: number;
	public kontaktnarazmerje: number;
	public skrbnik: number;
	public skrbnikrazmerje: number;
}

export class PacientDB {
	public id: number;
	public sifrazavarovanja: number;
	public okolis: number;
	public datumrojstva: Date;
	public spol: number;
	public kontaktnaoseba: number;
	public kontaktnarazmerje: number;
	public skrbnik: number;
	public skrbnikrazmerje: number;
}

export class Postavkadn {
	public id: number;
	public delovninalog: number;
	public storitev: number;
	//public parametristoritve: jsonb;
	public dodatenopis: string;
	public pacient: number;
}

export class Uporabnik extends Oseba {
	public id: number;
	public email: string;
	public geslo: string;
	public sol: string;
	public stanje: number;
	public aktivacija: string;
	public datumregistracije: string; // bi moral biti date, sm castanje razreda ne pretvori atributov objekta
	public datumzadnjeprijave: string;
	public vloga: SifraVloge;
}

export class UporabnikDB {
	public id: number;
	public email: string;
	public geslo: string;
	public sol: string;
	public stanje: number;
	public aktivacija: string;
	public datumregistracije: Date;
	public datumzadnjeprijave: Date;
}

export class Vrstaobiska {
	public id: number;
	public ime: string;
	public kurativa: boolean;
}

export class Material {
	public id: number;
	public ime: string;
}

export class Materialdn {
	public material: number;
	public dn: number;
	public kolicina: number;
}

export class Zdravilodn {
	public zdravilo: number;
	public dn: number;
	public kolicina?: number;
}

export class Zaposleni extends Uporabnik {
	public id: number;
	public sifrazaposlenega: number;
	public izvajalec: number;
	public okolis: number;
}

export enum SifraVloge{
	Pacient = 0,
	Administrator = 1,
	Zdravnik = 2,
	Vodja_patronazne_sluzbe = 3,
	Medicinska_sestra = 4,
	Usluzbenec = 5,
}

export class Kraj {
	public postna: number;
	public imekraja: string;
}

export class Obisk {
  constructor(
    public id: number,
    public vrstaObiska: string, 
    public predvideniDatum: Date,
    public dejanskiDatum: Date,
    public izdajatelj:string,
    public zadolzenaSestra: string,
    public nadomestnaSestra: string,
    public pacient: string) { }
}